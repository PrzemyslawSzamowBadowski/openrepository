package pl.fitpossible.fitpossibleproject.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.test.espresso.action.ViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.Food;
import pl.fitpossible.fitpossibleproject.repository.FoodRepository;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AddNewFoodProductFragmentTest {

    FoodRepository foodRepository;
    Food testFood;
SharedPreferences.Editor editor;

    @Rule
    public ActivityTestRule mActivityTestRule = new ActivityTestRule(MainActivity.class,true, false);

    Activity mActivity = mActivityTestRule.getActivity();

    @Before
    public void setUp() {
        mActivity = mActivityTestRule.getActivity();
        SharedPreferences sharedPreferences = getInstrumentation().getTargetContext().getSharedPreferences("ID", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putLong("LoggedAppUserID", 1);
        editor.commit();
        mActivityTestRule.launchActivity(new Intent());

        onView(withId(R.id.bn_go_to_fragment_food_product))
                .perform(click());
        onView(withId(R.id.bn_go_to_add_new_food_product))
                .perform(click());
    }

    @Test
    public void fragment_add_new_fod_product_is_instantiated() {
        onView(withId(R.id.fragment_add_new_food_product))
                .check(matches(isDisplayed()));
    }

    @Test
    public void txt_title_add_new_food_product_is_instantiated() {
        onView(withId(R.id.txt_title_add_new_food_product))
                .check(matches(isDisplayed()));
    }

    @Test
    public void edTxt_add_new_food_product_name_is_instantiated() {
        onView(withId(R.id.edTxt_add_new_food_product_name))
                .check(matches(isDisplayed()));
    }

    @Test
    public void edTxt_add_new_food_product_name_can_be_edited() {
        onView(withId(R.id.edTxt_add_new_food_product_name))
                .perform(ViewActions.typeText("Food"))
                .check(matches(withText("Food")));
    }

    @Test
    public void edTxt_add_new_food_product_calories_per_unit() {
        onView(withId(R.id.edTxt_add_new_food_product_calories_per_unit))
                .check(matches(isDisplayed()));
    }

    @Test
    public void edTxt_add_new_food_product_calories_per_unit_can_be_edited() {
        onView(withId(R.id.edTxt_add_new_food_product_calories_per_unit))
                .perform(ViewActions.typeText("100"))
                .check(matches(withText("100")));
    }

    @Test
    public void edTxt_add_new_food_product_calories_per_unit_can_not_recive_letters() {
        onView(withId(R.id.edTxt_add_new_food_product_calories_per_unit))
                .perform(ViewActions.typeText("abc"))
                .check(matches(not(withText("abc"))));
    }

    @Test
    public void edTxt_add_new_food_product_unit_is_instantiated() {
        onView(withId(R.id.edTxt_add_new_food_product_unit))
                .check(matches(isDisplayed()));
    }

    @Test
    public void edTxt_add_new_food_product_unit_can_be_edited() {
        onView(withId(R.id.edTxt_add_new_food_product_unit))
                .perform(ViewActions.typeText("kilogram"))
                .check(matches(withText("kilogram")));
    }

    @Test
    public void bn_submit_add_new_food_product_is_instantiated() {
        onView(withId(R.id.bn_submit_add_new_food_product))
                .check(matches(isCompletelyDisplayed()));
    }

    @Test
    public void bn_submit_add_new_food_product_is_clickable() {
        onView(withId(R.id.bn_submit_add_new_food_product))
                .check(matches(isClickable()));
    }

    @Test
    public void bn_submit_add_new_food_product_submits_new_food_product() {
        String testName = "TestName";
        String testNumber = "100";
        String testUnit = "TestUnit";

        onView(withId(R.id.edTxt_add_new_food_product_name))
                .perform(typeText(testName));
        onView(withId(R.id.edTxt_add_new_food_product_calories_per_unit))
                .perform(typeText(testNumber));
        onView(withId(R.id.edTxt_add_new_food_product_unit))
                .perform(typeText(testUnit))
                .perform(closeSoftKeyboard());

        onView(withId(R.id.bn_submit_add_new_food_product))
                .perform(click());


        FoodRepository foodRepository = new FoodRepository();
        Food testFood = foodRepository.showFoodProductByName(testName, mActivity);

        Assert.assertEquals(testName, testFood.getName());
    }

    @After
    public void wipeOut() {
        if (foodRepository != null) {
            foodRepository.deleteFoodProduct(testFood, mActivity);
        }
        editor.clear().commit();
        mActivity = null;
    }
}
