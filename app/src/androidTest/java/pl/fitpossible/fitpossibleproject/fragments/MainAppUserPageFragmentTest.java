package pl.fitpossible.fitpossibleproject.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.TestHelpClass;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressBack;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainAppUserPageFragmentTest {

    Intent intent;
    SharedPreferences.Editor editor;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule(MainActivity.class, false, false);

    private MainActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
        SharedPreferences sharedPreferences = getInstrumentation().getTargetContext().getSharedPreferences("ID", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putLong("LoggedAppUserID", 1);
        editor.commit();
        mActivityTestRule.launchActivity(new Intent());
    }

    @Test
    public void txt_show_login_can_be_instantiated() {
        onView(withId(R.id.txt_show_login)).check(matches(isDisplayed()));
    }

    @Test
    public void txt_show_login_shows_valid_login() {
        onView(withId(R.id.txt_show_login)).check(matches(withText(ApplicationGlobals.loggedAppUserName)));
    }

    @Test
    public void txt_show_login_shows_login() {
        onView(withId(R.id.txt_show_login)).perform(TestHelpClass.setTextInTextView("Login")).check(matches(withText("Login")));
    }

    @Test
    public void avatar_can_be_instantiated() {
        onView(withId(R.id.avatar)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_go_to_update_appUser_can_be_instantiated() {
        onView(withId(R.id.bn_go_to_update_appUser)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_go_to_update_appUser_replaces_fragment_with_update_app_user_fragment() {
        onView(withId(R.id.bn_go_to_update_appUser)).perform(click());
        onView(withId(R.id.fragment_update_app_user)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_log_out_can_be_instantiated() {
        onView(withId(R.id.bn_log_out)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_log_out_logs_out() {
        onView(withId(R.id.bn_log_out)).perform(click());
        onView(withId(R.id.fragment_logging)).check((matches(isDisplayed())));
        onView(withId(R.id.fragment_logging)).perform(pressBack()).check(matches(isDisplayed()));
    }

    @Test
    public void bn_food_diary_can_be_instantiated() {
        onView(withId(R.id.bn_food_diary)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_food_diary_switches_to_food_diary_fragment() {
        onView(withId(R.id.bn_food_diary)).perform(click());
        onView(withId(R.id.fragment_food_diary)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_trainings_can_be_instantiated() {
        onView(withId(R.id.bn_trainings)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_trainings_switches_to_fragment_training_diary() {
        onView(withId(R.id.bn_trainings)).perform(click());
        onView(withId(R.id.fragment_training_diary)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_balance_can_be_instantiated() {
        onView(withId(R.id.bn_balance)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_balance_is_clickable() {
        onView(withId(R.id.bn_balance)).check(matches(isClickable()));
    }

    @Test
    public void bn_balance_switches_to_fragment_user_charts() {
        onView(withId(R.id.bn_balance)).perform(click());
        onView(withId(R.id.fragment_user_charts)).check(matches(isDisplayed()));
    }

    @Test
    public void quick_balance_test() {
        onView(withId(R.id.bn_balance)).perform(click());
    }

    @Test
    public void bn_add_meal_now_can_be_instantiated() {
        onView(withId(R.id.bn_go_to_fragment_food_product)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_add_meal_now_switches_to_fragment_add_new_meal() {
        onView(withId(R.id.bn_go_to_fragment_food_product)).perform(click());
        onView(withId(R.id.fragment_food_product)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_go_to_steps_fragment_can_be_instantiated() {
        onView(withId(R.id.bn_go_to_steps_fragment)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_go_to_steps_fragment_switches_to_fragment_steps() {
        onView(withId(R.id.bn_go_to_steps_fragment)).perform(click());
        onView(withId(R.id.fragment_steps)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_rep_capture_can_be_instantiated() {
        onView(withId(R.id.bn_rep_capture)).check(matches(isDisplayed()));
    }

    @Test
    public void bn_rep_capture_switches_to_fragment_rep_capture() {
        onView(withId(R.id.bn_rep_capture)).perform(click());
        onView(withId(R.id.activity_capture_rep)).check(matches(isDisplayed()));
    }

    @Test
    public void emptySharedPreferencesSwitchesToLoggingActivity() {
        editor.clear().commit();
        mActivityTestRule.finishActivity();
        mActivityTestRule.launchActivity(new Intent());
        onView(withId(R.id.fragment_logging)).check(matches(isDisplayed()));
    }

    @After
    public void wipeOut() {
        editor.clear().commit();
        mActivity = null;
    }

}
