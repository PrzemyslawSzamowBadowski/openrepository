package pl.fitpossible.fitpossibleproject.activities;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import pl.fitpossible.fitpossibleproject.LoggingActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.fragments.LoggingFragment;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertNotNull;


@RunWith(AndroidJUnit4.class)
public class LoggingActivityTest {

    @Rule
    public ActivityTestRule<LoggingActivity> mActivityTestRule = new ActivityTestRule<LoggingActivity>(LoggingActivity.class);

    private LoggingActivity mActivity = null;
    private LoggingFragment mLoggingFragment = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testLunch() {
        View view = mActivity.findViewById(R.id.activity_logging);
        assertNotNull(view);
    }

    @Test
    public void edtxt_enter_login_can_be_instantiated() {
        onView(withId(R.id.edtxt_enter_login))
                .check(matches(isDisplayed()));

    }

    @Test
    public void edtxt_password_can_be_instantiated() {
        onView(withId(R.id.edtxt_password))
                .check(matches(isDisplayed()));

    }

    @Test
    public void bn_submit_can_be_instantiated() {
        onView(withId(R.id.bn_submit))
                .check(matches(isClickable()));
    }

    @Test
    public void bn_add_appUser_can_be_instantiated() {
        onView(withId(R.id.bn_add_appUser))
                .check(matches(isClickable()));
    }

    @Test
    public void bn_view_appUsers_can_be_instantiated() {
        onView(withId(R.id.bn_view_appUsers))
                .check(matches(isClickable()));
    }

    @Test
    public void bn_view_appUsers_takes_to_ReadAppUsersFragment() {
        onView(withId(R.id.bn_view_appUsers))
                .perform(click());
        onView(withId(R.id.fragment_read_app_users))
                .check(matches(isDisplayed()));
    }

    @Test
    public void bn_add_appUser_takes_to_ReadAppUsersFragment() {
        onView(withId(R.id.bn_add_appUser))
                .perform(click());
        onView(withId(R.id.fragment_add_user))
                .check(matches(ViewMatchers.isDisplayed()));
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }
}
