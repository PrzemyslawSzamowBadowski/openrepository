package pl.fitpossible.fitpossibleproject.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertNotNull;

public class MainActivityTest {

    SharedPreferences.Editor editor;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    private MainActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
        SharedPreferences sharedPreferences = getInstrumentation().getTargetContext().getSharedPreferences("ID", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putLong("LoggedAppUserID", 1);
        editor.commit();
        mActivityTestRule.launchActivity(new Intent());
    }

    @Test
    public void testLunch() {
        View view = mActivity.findViewById(R.id.activity_main);
        assertNotNull(view);
    }

    @Test
    public void mainActivityStartsMainUserPage() {
        onView(withId(R.id.fragment_main_user_page))
                .check(matches(isDisplayed()));
    }

    @After
    public void wipeOut() {
        editor.clear().commit();
        mActivity = null;
    }


}