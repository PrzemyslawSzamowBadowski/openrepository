package pl.fitpossible.fitpossibleproject.databases;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import pl.fitpossible.fitpossibleproject.dao.FoodDAO;
import pl.fitpossible.fitpossibleproject.entity.Food;

@Database(entities = {Food.class}, version = 1, exportSchema = false)
public abstract class FoodDatabase extends RoomDatabase {
    private static FoodDatabase foodDatabaseInstance;

    public abstract FoodDAO foodDAO();

    public static synchronized FoodDatabase getInstance(Context context) {
        if (foodDatabaseInstance == null) {
            foodDatabaseInstance = Room.databaseBuilder(context.getApplicationContext(),
                    FoodDatabase.class, "fooddb").
                    fallbackToDestructiveMigration().
                    build();
        }
        return foodDatabaseInstance;
    }
}


