package pl.fitpossible.fitpossibleproject.databases;


import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import pl.fitpossible.fitpossibleproject.dao.MovementActivityDAO;
import pl.fitpossible.fitpossibleproject.entity.MovementActivity;

@Database(entities = {MovementActivity.class}, version = 1, exportSchema = false)
public abstract class MovementActivityDatabase extends RoomDatabase {
    private static MovementActivityDatabase foodDatabaseInstance;

    public abstract MovementActivityDAO movementActivityDAO();

    public static synchronized MovementActivityDatabase getFoodDatabaseInstance(Context context) {
        if (foodDatabaseInstance == null) {
            foodDatabaseInstance = Room.databaseBuilder(context.getApplicationContext(),
                    MovementActivityDatabase.class, "movementactivitydb" ).
                    fallbackToDestructiveMigration().
                    build();
        }

        return foodDatabaseInstance;
    }
}