package pl.fitpossible.fitpossibleproject.databases;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import pl.fitpossible.fitpossibleproject.dao.StepsHistoryDAO;
import pl.fitpossible.fitpossibleproject.entity.StepsHistory;

@Database(entities = StepsHistory.class, version = 1,exportSchema = false)
public abstract class StepsHistoryDatabase extends RoomDatabase {
    public abstract StepsHistoryDAO stepsHistoryDAO();
}
