package pl.fitpossible.fitpossibleproject.databases;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

import pl.fitpossible.fitpossibleproject.converter.AppUserConverters;
import pl.fitpossible.fitpossibleproject.dao.ActivityHistoryDAO;
import pl.fitpossible.fitpossibleproject.dao.AppUserDAO;
import pl.fitpossible.fitpossibleproject.dao.AvatarImageDAO;
import pl.fitpossible.fitpossibleproject.dao.NutritionHistoryDAO;
import pl.fitpossible.fitpossibleproject.dao.WeightDAO;
import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.entity.AvatarImage;
import pl.fitpossible.fitpossibleproject.entity.NutritionHistory;
import pl.fitpossible.fitpossibleproject.entity.Weight;

@Database(entities = {AppUser.class, Weight.class, ActivityHistory.class, NutritionHistory.class, AvatarImage.class}, version = 1, exportSchema = false)
@TypeConverters({AppUserConverters.class})
public abstract class AppUserDatabase extends RoomDatabase {

    private static AppUserDatabase appUserDatabaseInstance;

    public abstract AppUserDAO appUserDAO();

    public abstract WeightDAO weightDAO();

    public abstract ActivityHistoryDAO activityHistoryDAO();

    public abstract NutritionHistoryDAO nutritionHistoryDAO();

    public abstract AvatarImageDAO avatarImageDAO();

    public static synchronized AppUserDatabase getInstance(Context context) {
        if (appUserDatabaseInstance == null) {
            appUserDatabaseInstance = Room.databaseBuilder(context.getApplicationContext(),
                    AppUserDatabase.class, "userdb")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return appUserDatabaseInstance;
    }

//    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            // Since we didn't alter the table, there's nothing else to do here.
//        }
//    };


}
