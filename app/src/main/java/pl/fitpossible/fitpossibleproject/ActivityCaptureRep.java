package pl.fitpossible.fitpossibleproject;

import android.app.Application;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Instant;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;

import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;
import pl.fitpossible.fitpossibleproject.repository.ActivityHistoryRepository;

import static android.graphics.ImageFormat.YV12;

public class ActivityCaptureRep extends AppCompatActivity {

    Button bnStart;
    Button bnStop;
    Button bnPause;
    Button bnReset;
    SurfaceView surfaceView;
    TextView showRepCount;
    //TextView textViewStopwatch;
    ImageView imageViewLowColorImage;
    ImageView checkColorBox;
    RelativeLayout relativeLayoutWithPreview;

    Chronometer chronometer;

    LinearLayout emptyLinearLayout;
    LinearLayout linearLayoutWithCheckColorBox;

    Camera camera;
    SurfaceHolder surfaceHolder;
    Camera.PreviewCallback previewCallback;

    Bitmap bmRotatedAndScaled;
    int fixationColor;
    int colorOfPixel;
    Integer counter = 0;
    boolean count = true;
    boolean chronometerRunning = false;
    long pauseOffset;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_rep);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        relativeLayoutWithPreview = findViewById(R.id.relativeLayout_withPreview);
        checkColorBox = findViewById(R.id.checkColorBox);

        showRepCount = findViewById(R.id.txtView_showRepCount);
        imageViewLowColorImage = findViewById(R.id.imageView_LowColorImage);

        emptyLinearLayout = findViewById(R.id.empty_linearLayout);
        linearLayoutWithCheckColorBox = findViewById(R.id.layout_with_checkColorBox);

        chronometer = findViewById(R.id.chronometer);


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


        SetBlackSquarePlace(height);

        bnStart = findViewById(R.id.bn_start_chronometer_button);
        startTimersWithStratButton();

        bnPause = findViewById(R.id.bn_pause_chronometer);
        bnPause.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {
                                           pauseChronometer();
                                       }
                                   }
        );


        bnStop = findViewById(R.id.bn_stop_chronometer_button);
        bnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopChronometer();

            }
        });

        bnReset = findViewById(R.id.bn_reset_chronometer_button);
        bnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetChronometer();
            }
        });

        surfaceView = findViewById(R.id.sv_preview);

        surfaceHolder = surfaceView.getHolder();

        addCallbackToSurfaceHolder();

        makePreviewCallback();
    }

    private void SetBlackSquarePlace(int height) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) emptyLinearLayout.getLayoutParams();
        params.height = height / 9;
        emptyLinearLayout.setLayoutParams(params);

        RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) linearLayoutWithCheckColorBox.getLayoutParams();
        params2.height = height / 9;
        linearLayoutWithCheckColorBox.setLayoutParams(params2);
    }

    private void startTimersWithStratButton() {
        bnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CountDownTimer(5000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        showRepCount.setText(String.valueOf(millisUntilFinished / 1000));
                    }

                    public void onFinish() {
                        fixationColor = colorOfPixel;
                        startChronometer();
                    }
                }.start();

            }
        });
    }

    private void addCallbackToSurfaceHolder() {
        surfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                startCamera();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                try {
                    stopCamera();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }


    private void makePreviewCallback() {
        previewCallback = new PreviewCallback() {
            @Override
            public void onPreviewFrame(byte[] data, Camera camera) {
                Camera.Parameters parameters = camera.getParameters();
                Camera.Size size = parameters.getPreviewSize();
                YuvImage image = new YuvImage(data, parameters.getPreviewFormat(),
                        size.width, size.height, null);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                image.compressToJpeg(new Rect(0, 0, size.width, size.height), 0, baos);
                byte[] imageByte = baos.toByteArray();

                Matrix matrix = new Matrix();
                matrix.setScale(-1, 1);
                matrix.postRotate(90);

                BitmapFactory.Options opt = new BitmapFactory.Options();
                opt.inMutable = true;
                Bitmap bm = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length, opt);

                Bitmap bmRotated = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
                Bitmap bmRotatedAndScaled = Bitmap.createScaledBitmap(bmRotated, 5, 10, false);

                for (int i = 0; i < bmRotatedAndScaled.getWidth(); i++) {
                    for (int j = 0; j < bmRotatedAndScaled.getHeight(); j++) {
                        int color = bmRotatedAndScaled.getPixel(i, j);
                        color = color & 0xFFC08080;
                        bmRotatedAndScaled.setPixel(i, j, color);
                    }
                }

                colorOfPixel = bmRotatedAndScaled.getPixel(2, 2);
                imageViewLowColorImage.setImageBitmap(bmRotatedAndScaled);

                if (count == true && fixationColor == colorOfPixel && chronometerRunning == true) {


                    counter = counter + 1;
                    showRepCount.setText(counter.toString());
                    count = false;

                }
                if (fixationColor != colorOfPixel) {

                    count = true;

                }
            }


        };
    }

    private void startCamera() {
        if (camera == null) {
            try {
                camera = Camera.open(1);
                Camera.Parameters param = camera.getParameters();
                param.setSceneMode(Camera.Parameters.SCENE_MODE_ACTION);
                param.setPreviewFormat(YV12);
                camera.setDisplayOrientation(90);
                camera.setPreviewCallback(previewCallback);
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return;
        }
    }

    private void stopCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;


        }


    }

    private void startChronometer() {
        if (!chronometerRunning) {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            chronometerRunning = true;
        }
    }

    private void stopChronometer() {
        if (chronometerRunning) {
            chronometer.stop();
            chronometerRunning = false;
            showMassage();
            AlertDialog alertDialog = showMassage().create();
            alertDialog.show();
        }
    }

    private void pauseChronometer() {
        if (!chronometerRunning) {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            chronometerRunning = true;
        } else {
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            chronometerRunning = false;
        }

    }

    private void resetChronometer() {
        chronometer.setBase(SystemClock.elapsedRealtime());
        pauseOffset = 0;
        counter = 0;
        showRepCount.setText("0");
    }

    private void stopCameraAndChronometer() {
        stopCamera();
        stopChronometer();
    }

    @Override
    protected void onDestroy() {
        stopCamera();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        stopCamera();
        super.onPause();
    }

    protected AlertDialog.Builder showMassage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Do you want to save current training?");
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                try {
                    Integer reps = Integer.parseInt(showRepCount.getText().toString());
                    Date date = new Date();
                    Date date1 = new Date(date.getYear(), date.getMonth(), date.getDate());
                    Date time = new Date(0, 0, 0, date.getHours(), date.getMinutes());
                    ActivityHistory activityHistory = new ActivityHistory();

                    activityHistory.setActivityType("jakiś trening");
                    activityHistory.setCaloriesPerUnit(100);
                    activityHistory.setUnitType("jakaś jednostka");
                    activityHistory.setAppUserId(ApplicationGlobals.loggedAppUserId);
                    activityHistory.setDate(date1);
                    activityHistory.setTime(time);
                    activityHistory.setReps(reps);
                    ActivityHistoryRepository activityHistoryRepository = new ActivityHistoryRepository();
                    activityHistoryRepository.addActivityHistory(activityHistory, getApplication());



                    Toast.makeText(getApplication(),"Activity Saved",Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        });
        return builder;
    }
}

