package pl.fitpossible.fitpossibleproject.repository;

import android.content.Context;
import android.os.AsyncTask;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.dao.ActivityHistoryDAO;
import pl.fitpossible.fitpossibleproject.databases.AppUserDatabase;
import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;

@Data

public class ActivityHistoryRepository {
    private ActivityHistoryDAO activityHistoryDAO;
    private List<ActivityHistory> allActivityHistory;
    private List<ActivityHistory> getActivityHistoryById;
    private List<ActivityHistory> getActivityHistoryByIdAndDate;

//    public ActivityHistoryRepository(Application application) {
//        AppUserDatabase appUserDatabase = AppUserDatabase.getInstance(application);
//        activityHistoryDAO = appUserDatabase.activityHistoryDAO();
//        allActivityHistory = activityHistoryDAO.getActivityHistories();
//    }
//
//    public ActivityHistoryRepository(Application application, Long id) {
//        AppUserDatabase appUserDatabase = AppUserDatabase.getInstance(application);
//        activityHistoryDAO = appUserDatabase.activityHistoryDAO();
//        getActivityHistoryById = activityHistoryDAO.getActivityHistoryById(id);
//    }
//
//    public ActivityHistoryRepository(Application application, Long id, Date date) {
//        AppUserDatabase appUserDatabase = AppUserDatabase.getInstance(application);
//        activityHistoryDAO = appUserDatabase.activityHistoryDAO();
//        getActivityHistoryByIdAndDate = activityHistoryDAO.getActivityHistoryByIdAndDate(id, date);
//    }


    public void addActivityHistory(ActivityHistory activityHistory, Context context) {
        new AddActivityHistoryAsyncTask(activityHistory, context, activityHistoryDAO).execute();
    }

    public List<ActivityHistory> getActivityHistories() {
        return allActivityHistory;
    }


    public List<ActivityHistory> getActivityHistoryById(Long id, Context context) {
        List activityHistoryById = null;
        try {
            activityHistoryById = new GetActivityHistoryByIdAsyncTask(id, context, activityHistoryDAO).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return activityHistoryById;
    }


    public List<ActivityHistory> getActivityHistoryByIdAndDate(Long appUserId, Date date, Context context) {
        List activityHistoryByIdAndDate = null;
        try {
            activityHistoryByIdAndDate = new GetAllActivityHistoryByIdAndDateAsyncTask(activityHistoryDAO, appUserId, date, context).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return activityHistoryByIdAndDate;
    }


    public void deleteActivityHistory(ActivityHistory activityHistory) {
        new DeleteActivityHistoryAsyncTask(activityHistoryDAO).execute(activityHistory);
    }

    public void updateActivityHistory(ActivityHistory activityHistory) {
        new UpdateActivityHistoryAsyncTask(activityHistoryDAO).execute(activityHistory);
    }

    private static class GetActivityHistoryByIdAsyncTask extends AsyncTask<Void, Void, List<ActivityHistory>> {
        private ActivityHistoryDAO activityHistoryDAO;
        private Long id;
        private Context context;

        private GetActivityHistoryByIdAsyncTask(Long id, Context context, ActivityHistoryDAO activityHistoryDAO) {
            this.id = id;
            this.context = context;
            this.activityHistoryDAO = activityHistoryDAO;
        }

        @Override
        protected List<ActivityHistory> doInBackground(Void... voids) {

            return AppUserDatabase.getInstance(context).activityHistoryDAO().getActivityHistoryById(id);
        }
    }

    private static class GetAllActivityHistoryByIdAndDateAsyncTask extends AsyncTask<ActivityHistory, Void, List<ActivityHistory>> {
        private ActivityHistoryDAO activityHistoryDAO;
        private Long id;
        private Date date;
        private Context context;


        private GetAllActivityHistoryByIdAndDateAsyncTask(ActivityHistoryDAO activityHistoryDAO, Long id, Date date, Context context) {
            this.activityHistoryDAO = activityHistoryDAO;
            this.id = id;
            this.date = date;
            this.context = context;
        }

        @Override
        protected List<ActivityHistory> doInBackground(ActivityHistory... activityHistories) {
            return AppUserDatabase.getInstance(context).activityHistoryDAO().getActivityHistoryByIdAndDate(id, date);
        }
    }


    private static class GetAllActivityHistoryAsyncTask extends AsyncTask<ActivityHistory, Void, List<ActivityHistory>> {
        private ActivityHistoryDAO activityHistoryDAO;

        private GetAllActivityHistoryAsyncTask(ActivityHistoryDAO activityHistoryDAO) {
            this.activityHistoryDAO = activityHistoryDAO;
        }

        @Override
        protected List<ActivityHistory> doInBackground(ActivityHistory... activityHistories) {
            ;
            return activityHistoryDAO.getActivityHistories();
        }
    }

    private static class AddActivityHistoryAsyncTask extends AsyncTask<ActivityHistory, Void, Void> {
        private ActivityHistory activityHistory;
        private Context context;
        private ActivityHistoryDAO activityHistoryDAO;

        private AddActivityHistoryAsyncTask(ActivityHistory activityHistory, Context context, ActivityHistoryDAO activityHistoryDAO) {
            this.activityHistory = activityHistory;
            this.context = context;
            this.activityHistoryDAO = activityHistoryDAO;
        }

        @Override
        protected Void doInBackground(ActivityHistory... activityHistories) {
            AppUserDatabase.getInstance(context).activityHistoryDAO().addActivityHistory(activityHistory);
            return null;
        }
    }

    private static class DeleteActivityHistoryAsyncTask extends AsyncTask<ActivityHistory, Void, Void> {
        private ActivityHistoryDAO activityHistoryDAO;

        private DeleteActivityHistoryAsyncTask(ActivityHistoryDAO activityHistoryDAO) {
            this.activityHistoryDAO = activityHistoryDAO;
        }

        @Override
        protected Void doInBackground(ActivityHistory... activityHistories) {
            activityHistoryDAO.deleteActivityHistory(activityHistories[0]);
            return null;
        }
    }

    private static class UpdateActivityHistoryAsyncTask extends AsyncTask<ActivityHistory, Void, Void> {
        private ActivityHistoryDAO activityHistoryDAO;

        private UpdateActivityHistoryAsyncTask(ActivityHistoryDAO activityHistoryDAO) {
            this.activityHistoryDAO = activityHistoryDAO;
        }

        @Override
        protected Void doInBackground(ActivityHistory... activityHistories) {
            activityHistoryDAO.updateActivityHistory(activityHistories[0]);
            return null;
        }
    }
}
