package pl.fitpossible.fitpossibleproject.repository;

import android.content.Context;
import android.os.AsyncTask;

import java.util.List;
import java.util.concurrent.ExecutionException;

import pl.fitpossible.fitpossibleproject.databases.MovementActivityDatabase;
import pl.fitpossible.fitpossibleproject.entity.MovementActivity;

public class MovementActivityRepository {

    public void addMovementActivity(MovementActivity movementActivity, Context context) {
        try {
            new AddMovementActivityAsyncTask(movementActivity, context).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateMovementActivity(MovementActivity movementActivity, Context context) {
        new UpdateMovementActivityAsyncTask(movementActivity, context).execute();
    }

    public void deleteMovementActivity(MovementActivity movementActivity, Context context) {
        new DeleteMovementActivityAsyncTask(movementActivity, context).execute();
    }

    public MovementActivity getMovementActivityByName(String name, Context context) {
        MovementActivity movementActivity = null;
        try {
            movementActivity = new GetMovementActivityByNameAsyncTask(name, context).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return movementActivity;
    }

    public List<MovementActivity> getAllMovementActivity(Context context) {
        List<MovementActivity> getAllMovementActivity = null;
        try {
            getAllMovementActivity = new GetAllMovementActivityAsyncTask(context).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return getAllMovementActivity;
    }


    private static class AddMovementActivityAsyncTask extends AsyncTask<Void, Void, Void> {
        MovementActivity movementActivity;
        Context context;


        private AddMovementActivityAsyncTask(MovementActivity movementActivity, Context context) {
            this.movementActivity = movementActivity;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                MovementActivityDatabase.getFoodDatabaseInstance(context).movementActivityDAO().addActivity(movementActivity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    private static class UpdateMovementActivityAsyncTask extends AsyncTask<Void, Void, Void> {
        MovementActivity movementActivity;
        Context context;

        private UpdateMovementActivityAsyncTask(MovementActivity movementActivity, Context context) {
            this.movementActivity = movementActivity;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            MovementActivityDatabase.getFoodDatabaseInstance(context).movementActivityDAO().updateActivity(movementActivity);
            return null;
        }
    }

    private static class DeleteMovementActivityAsyncTask extends AsyncTask<Void, Void, Void> {
        MovementActivity movementActivity;
        Context context;

        private DeleteMovementActivityAsyncTask(MovementActivity movementActivity, Context context) {
            this.movementActivity = movementActivity;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            MovementActivityDatabase.getFoodDatabaseInstance(context).movementActivityDAO().deleteActivity(movementActivity);
            return null;
        }
    }

    private static class GetMovementActivityByNameAsyncTask extends AsyncTask<Void, Void, MovementActivity> {
        String name;
        Context context;

        private GetMovementActivityByNameAsyncTask(String name, Context context) {
            this.name = name;
            this.context = context;
        }

        @Override
        protected MovementActivity doInBackground(Void... voids) {
            MovementActivity movementActivity = MovementActivityDatabase.getFoodDatabaseInstance(context).movementActivityDAO().getMovementActivity(name);
            return movementActivity;
        }
    }

    private static class GetAllMovementActivityAsyncTask extends AsyncTask<Void, Void, List<MovementActivity>> {
        Context context;

        public GetAllMovementActivityAsyncTask(Context context) {
            this.context = context;
        }


        @Override
        protected List<MovementActivity> doInBackground(Void... voids) {
            List<MovementActivity> allMovementActivity = MovementActivityDatabase.getFoodDatabaseInstance(context).movementActivityDAO().getMovementActivity();
            return allMovementActivity;
        }
    }

}
