package pl.fitpossible.fitpossibleproject.repository;

import android.content.Context;
import android.os.AsyncTask;

import java.util.concurrent.ExecutionException;

import pl.fitpossible.fitpossibleproject.dao.AppUserDAO;
import pl.fitpossible.fitpossibleproject.dao.WeightDAO;
import pl.fitpossible.fitpossibleproject.databases.AppUserDatabase;
import pl.fitpossible.fitpossibleproject.entity.Weight;

public class WeightRepository {
    private AppUserDAO appUserDAO;
    private Context context;
    private WeightDAO weightDAO;
    private Weight getLastAppUsersWeight;

    public void addWeight(Weight weight, Context context){
       new AddWeightAsyncTask(weight, context, weightDAO).execute();
    }

    public Weight getLastAppUsersWeight(Long appUsersId, Context context){
        Weight lastAppUsersWeight = null;
        try {
            lastAppUsersWeight = new GetLastAppUsersWeightAsyncTask(weightDAO, appUsersId, context).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return lastAppUsersWeight;
    }

    static class GetLastAppUsersWeightAsyncTask extends AsyncTask<Void, Void, Weight> {
        private WeightDAO weightDAO;
        private Context context;
        private Long appUsersId;

        private GetLastAppUsersWeightAsyncTask(WeightDAO weightDAO, Long appUsersId, Context context) {
            this.weightDAO = weightDAO;
            this.appUsersId = appUsersId;
            this.context = context;
        }

        @Override
        protected Weight doInBackground(Void... voids) {
            Weight lastAppUsersWeight = AppUserDatabase.getInstance(context).weightDAO().getLastWeightByAppUserId(appUsersId);
            return lastAppUsersWeight;
        }
    }

    private static class AddWeightAsyncTask extends AsyncTask<Weight,Void,Void>{

        private Weight weight;
        private Context context;
        private WeightDAO weightDAO;

        private AddWeightAsyncTask (Weight weight, Context context, WeightDAO weightDAO){
            this.weight = weight;
            this.context = context;
            this.weightDAO = weightDAO;

        }
        @Override
        protected Void doInBackground(Weight... weights) {
           AppUserDatabase.getInstance(context).weightDAO().addWeight(weight);
            return null;
        }
    }
}
