package pl.fitpossible.fitpossibleproject.repository;

import android.content.Context;
import android.os.AsyncTask;

import java.util.concurrent.ExecutionException;

import pl.fitpossible.fitpossibleproject.dao.AvatarImageDAO;
import pl.fitpossible.fitpossibleproject.databases.AppUserDatabase;
import pl.fitpossible.fitpossibleproject.entity.AvatarImage;

public class AvatarImageRepository {

    private AvatarImageDAO avatarImageDAO;
    private String oneStringAppUserAvatarImage;

    public void addAvatarImage(AvatarImage avatarImage, Context context) {
        new AddAvatarImageAsyncTask(avatarImageDAO, avatarImage, context).execute();
    }

    public void updateAvatarImage(AvatarImage avatarImage, Context context) {
        new UpdateAvatarImageAsyncTask(avatarImageDAO, avatarImage, context).execute();
    }

    public String getOneStringAppUserAvatarImage(Long appUserId, Context context) {
        String oneStringAvatarImageByAppUserId = null;
        try {
            oneStringAvatarImageByAppUserId = new GetOneStringAvatarImageByAppUserIdAsyncTask(avatarImageDAO, context, appUserId).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return oneStringAvatarImageByAppUserId;
    }

    public AvatarImage getOneAvatarImageById(Long appUserId, Context context) {
        AvatarImage getOneAvatarImageById = null;
        try {
            getOneAvatarImageById = new GetOneAvatarImageByIdAsyncTask(avatarImageDAO, appUserId, context).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return getOneAvatarImageById;
    }

    static class GetOneStringAvatarImageByAppUserIdAsyncTask extends AsyncTask<Void, Void, String> {
        private AvatarImageDAO avatarImageDAO;
        private Context context;
        private Long appUserId;

        private GetOneStringAvatarImageByAppUserIdAsyncTask(AvatarImageDAO avatarImageDAO, Context context, Long appUserId) {
            this.avatarImageDAO = avatarImageDAO;
            this.context = context;
            this.appUserId = appUserId;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String oneStringAvatarImageByAppUserId = AppUserDatabase.getInstance(context).avatarImageDAO().getOneStringAvatarImageByAppUserId(appUserId);
            return oneStringAvatarImageByAppUserId;
        }
    }

    static class GetOneAvatarImageByIdAsyncTask extends AsyncTask<Void, Void, AvatarImage> {
        private AvatarImageDAO avatarImageDAO;
        private Long appUserId;
        private Context context;

        private GetOneAvatarImageByIdAsyncTask(AvatarImageDAO avatarImageDAO, Long appUserId, Context context) {
            this.avatarImageDAO = avatarImageDAO;
            this.appUserId = appUserId;
            this.context = context;
        }

        @Override
        protected AvatarImage doInBackground(Void... voids) {
            AvatarImage avatarImage = AppUserDatabase.getInstance(context).avatarImageDAO().getOneAvatarImageById(appUserId);
            return avatarImage;
        }
    }

    static class AddAvatarImageAsyncTask extends AsyncTask<AvatarImage, Void, Void> {
        private AvatarImageDAO avatarImageDAO;
        private AvatarImage avatarImage;
        private Context context;

        private AddAvatarImageAsyncTask(AvatarImageDAO avatarImageDAO, AvatarImage avatarImage, Context context) {
            this.avatarImageDAO = avatarImageDAO;
            this.avatarImage = avatarImage;
            this.context = context;
        }

        @Override
        protected Void doInBackground(AvatarImage... avatarImages) {
            AppUserDatabase.getInstance(context).avatarImageDAO().addAvatarImage(avatarImage);

            return null;
        }
    }

    static class UpdateAvatarImageAsyncTask extends AsyncTask<AvatarImage, Void, Void> {
        private AvatarImageDAO avatarImageDAO;
        private AvatarImage avatarImage;
        private Context context;

        private UpdateAvatarImageAsyncTask(AvatarImageDAO avatarImageDAO, AvatarImage avatarImage, Context context) {
            this.avatarImageDAO = avatarImageDAO;
            this.avatarImage = avatarImage;
            this.context = context;
        }

        @Override
        protected Void doInBackground(AvatarImage... avatarImages) {
            AppUserDatabase.getInstance(context).avatarImageDAO().updateAvatarImage(avatarImages[0]);
            return null;
        }
    }

}
