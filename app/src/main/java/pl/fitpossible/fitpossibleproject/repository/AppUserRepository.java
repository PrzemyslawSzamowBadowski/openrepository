package pl.fitpossible.fitpossibleproject.repository;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.ExecutionException;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.dao.AppUserDAO;
import pl.fitpossible.fitpossibleproject.databases.AppUserDatabase;
import pl.fitpossible.fitpossibleproject.entity.AppUser;

@Data

public class AppUserRepository {
    private AppUserDAO appUserDAO;
    private Context context;
    private Activity activity;
    private List<AppUser> allAppUsers;
    private AppUser appUserById;
    private AppUser appUserByLogin;
    private List<String> appUserLoginList;
    private String appUserAvatarList;


    public Integer addAppUser(AppUser appUser, Context context) {
        try {
            return new AddAppUserAsyncTask(appUserDAO, context).execute(appUser).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return -1;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public List<AppUser> getAppUsers(Context context) {
        List<AppUser> appUsers = null;

        try {
            appUsers = new GetAllAppUsersAsyncTask(appUserDAO, context, activity).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return appUsers;
    }


    public AppUser getAppUserById(Long appUserId, Context context) {
        AppUser appUserById = null;
        try {
            appUserById = new GetAppUserByIdAsyncTask(appUserDAO, appUserId, context).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return appUserById;
    }


    public AppUser getAppUserByLogin(String login, Context context) {
        AppUser appUserByLogin = null;
        try {
            appUserByLogin = new GetAppUserByLoginAsyncTask(appUserDAO, context, login).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return appUserByLogin;
    }


    public List<String> getAppUserLoginList() {
        return appUserLoginList;
    }


    public void deleteAppUser(AppUser appUser) {
        new DeleteAppUserAsyncTask(appUser,context,appUserDAO).execute(appUser);
    }

    public void updateAppUser(AppUser appUser) {
        new UpdateAppUserAsyncTask(appUserDAO, context).execute(appUser);
    }

    private static class GetAppUserByIdAsyncTask extends AsyncTask<AppUser, Void, AppUser> {
        private AppUserDAO appUserDAO;
        private Long id;
        private Context context;

        private GetAppUserByIdAsyncTask(AppUserDAO appUserDAO, Long id, Context context) {
            this.appUserDAO = appUserDAO;
            this.id = id;
            this.context = context;
        }

        @Override
        protected AppUser doInBackground(AppUser... appUsers) {
            AppUser appUserById = AppUserDatabase.getInstance(context).appUserDAO().getAppUserById(id);
            return appUserById;
        }
    }

    public static class GetAppUserByLoginAsyncTask extends AsyncTask<Void, Void, AppUser> {
        private AppUserDAO appUserDAO;
        private String login;
        private Context context;


        private GetAppUserByLoginAsyncTask(AppUserDAO appUserDAO, Context context, String login) {
            this.appUserDAO = appUserDAO;
            this.login = login;
            this.context = context;
        }

        @Override
        protected AppUser doInBackground(Void... voids) {
            AppUser appUsersByLogin = AppUserDatabase.getInstance(context).appUserDAO().getAppUserByLogin(login);
            return appUsersByLogin;
        }


    }


    private static class GetAllAppUsersAsyncTask extends AsyncTask<Void, Void, List<AppUser>> {
        private AppUserDAO appUserDAO;
        private Context context;
        private final WeakReference<Activity> weakReference;

        private GetAllAppUsersAsyncTask(AppUserDAO appUserDAO, Context context, Activity activity) {
            this.appUserDAO = appUserDAO;
            this.context = context;
            this.weakReference = new WeakReference<>(activity);
        }

        @Override
        protected List<AppUser> doInBackground(Void... voids) {
            return AppUserDatabase.getInstance(context).appUserDAO().getAppUsers();
        }
    }

    private static class AddAppUserAsyncTask extends AsyncTask<AppUser, Void, Integer> {
        private AppUserDAO appUserDAO;
        private Context context;

        private AddAppUserAsyncTask(AppUserDAO appUserDAO, Context context) {
            this.appUserDAO = appUserDAO;
            this.context = context;
        }

        @Override
        protected Integer doInBackground(AppUser... appUsers) {
            try {
                AppUserDatabase.getInstance(context).appUserDAO().addAppUser(appUsers[0]);
                return 1;
            } catch (android.database.sqlite.SQLiteConstraintException e) {
                return 0;
            }
        }
    }

    private static class DeleteAppUserAsyncTask extends AsyncTask<AppUser, Void, Void> {
        private AppUser appUser;
        private Context context;
        private AppUserDAO appUserDAO;

        private DeleteAppUserAsyncTask(AppUser appUser, Context context,AppUserDAO appUserDAO) {
            this.appUser = appUser;
            this.appUserDAO = appUserDAO;
            this.context = context;
        }

        @Override
        protected Void doInBackground(AppUser... appUsers) {
            AppUserDatabase.getInstance(context).appUserDAO().deleteAppUser(appUser);//appUsers[0]);
            return null;
        }
    }

    private static class UpdateAppUserAsyncTask extends AsyncTask<AppUser, Void, Void> {
        private AppUserDAO appUserDAO;
        private Context context;

        private UpdateAppUserAsyncTask(AppUserDAO appUserDAO, Context context) {

            this.appUserDAO = appUserDAO;
            this.context = context;
        }

        @Override
        protected Void doInBackground(AppUser... appUsers) {
            AppUserDatabase.getInstance(context).appUserDAO().updateAppUser(appUsers[0]);
            return null;
        }
    }

    private static class GetAppUserLoginListAsyncTask extends AsyncTask<Void, Void, List<String>> {
        private AppUserDAO appUserDAO;
        private Context context;

        private GetAppUserLoginListAsyncTask(AppUserDAO appUserDAO) {
            this.appUserDAO = appUserDAO;
            this.context = context;

        }

        @Override
        protected List<String> doInBackground(Void... voids) {
            List<String> appUserLoginList = AppUserDatabase.getInstance(context).appUserDAO().getAppUsersLogin();
            return appUserLoginList;
        }
    }

}
