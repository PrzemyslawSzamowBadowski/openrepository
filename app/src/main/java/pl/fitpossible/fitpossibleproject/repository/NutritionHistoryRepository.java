package pl.fitpossible.fitpossibleproject.repository;

import android.content.Context;
import android.os.AsyncTask;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.dao.NutritionHistoryDAO;
import pl.fitpossible.fitpossibleproject.databases.AppUserDatabase;
import pl.fitpossible.fitpossibleproject.entity.NutritionHistory;

@Data
public class NutritionHistoryRepository {
    private NutritionHistoryDAO nutritionHistoryDAO;
    private Context context;
    private Long id;


    public void addNutritionHistory(NutritionHistory nutritionHistory, Context context) {
        try {
            new AddNutritionHistoryAsyncTask(nutritionHistoryDAO, context).execute(nutritionHistory).get();
        } catch (ExecutionException e) {
            e.printStackTrace();

        } catch (InterruptedException e) {
            e.printStackTrace();

        }
    }

    public List getNutritionHistoryById(Long id, Context context) {
        List nutritionHistoryById = null;
        try {
            nutritionHistoryById = new GetNutritionHistoryByIdAsyncTask(id, context, nutritionHistoryDAO).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return nutritionHistoryById;
    }

    public List<NutritionHistory> getNutritionHistoryByIdAndDate(Long id, Date date, Context context) {
        List nutritionHistoryByIdAndDate = null;
        try {
            nutritionHistoryByIdAndDate = new GetNutritionHistoryByIdAndDateAsyncTask(id, date, context, nutritionHistoryDAO).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return nutritionHistoryByIdAndDate;
    }

    private static class AddNutritionHistoryAsyncTask extends AsyncTask<NutritionHistory, Void, Void> {
        private NutritionHistoryDAO nutritionHistoryDAO;
        private Context context;

        private AddNutritionHistoryAsyncTask(NutritionHistoryDAO nutritionHistoryDAO, Context context) {
            this.nutritionHistoryDAO = nutritionHistoryDAO;
            this.context = context;
        }


        @Override
        protected Void doInBackground(NutritionHistory... nutritionHistories) {

            try {
                AppUserDatabase.getInstance(context).nutritionHistoryDAO().addNutritionHistory(nutritionHistories[0]);

            } catch (android.database.sqlite.SQLiteConstraintException e) {

            }

            return null;
        }
    }

    private static class GetNutritionHistoryByIdAsyncTask extends AsyncTask<Long, Void, List<NutritionHistory>> {
        private NutritionHistoryDAO nutritionHistoryDAO;
        private Context context;
        private Long id;

        private GetNutritionHistoryByIdAsyncTask(Long id, Context context, NutritionHistoryDAO nutritionHistoryDAO) {
            this.id = id;
            this.context = context;
            this.nutritionHistoryDAO = nutritionHistoryDAO;
        }

        @Override
        protected List<NutritionHistory> doInBackground(Long... longs) {
            return AppUserDatabase.getInstance(context).nutritionHistoryDAO().getNutritionHistoryById(id);
        }
    }

    private static class GetNutritionHistoryByIdAndDateAsyncTask extends AsyncTask<Void, Void, List<NutritionHistory>> {
        Long id;
        Date date;
        Context context;
        NutritionHistoryDAO nutritionHistoryDAO;

        private GetNutritionHistoryByIdAndDateAsyncTask(Long id, Date date, Context context, NutritionHistoryDAO nutritionHistoryDAO) {
            this.id = id;
            this.date = date;
            this.context = context;
            this.nutritionHistoryDAO = nutritionHistoryDAO;
        }

        @Override
        protected List<NutritionHistory> doInBackground(Void... voids) {
            return AppUserDatabase.getInstance(context).nutritionHistoryDAO().getNutritionHistoryByIdAndDate(id, date);
        }
    }



}
