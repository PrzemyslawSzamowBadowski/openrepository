package pl.fitpossible.fitpossibleproject.repository;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;

import java.util.List;
import java.util.concurrent.ExecutionException;

import pl.fitpossible.fitpossibleproject.databases.FoodDatabase;
import pl.fitpossible.fitpossibleproject.entity.Food;

public class FoodRepository {
    public int addNewFoodProduct(Food food, Context context) {
        int isOk = 0;
        try {
            isOk = new AddNewFoodProductAsyncTask(food, context).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            return 0;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return 0;
        }
        return isOk;
    }

    public void updateNewFoodProduct(Food food, Context context) {
        new UpdateFoodProductAsyncTask(food, context).execute();

    }

    public void deleteFoodProduct(Food food, Context context) {
        new DeleteFoodProductAsyncTask(food, context).execute();

    }

    public List showAllFoodProducts(Context context) {
        List foodProducts = null;
        try {
            foodProducts = new ShowAllFoodProductsAsyncTask(context).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return foodProducts;
    }

    public Food showFoodProductByName(String name, Context context) {
        Food foodProduct = null;
        try {
            foodProduct = new ShowFoodProductByNameAsyncTask(name, context).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return foodProduct;
    }


    private class AddNewFoodProductAsyncTask extends AsyncTask<Void, Void, Integer> {
        private Food food;
        private Context context;

        private AddNewFoodProductAsyncTask(Food food, Context context) {
            this.food = food;
            this.context = context;
        }


        @Override
        protected Integer doInBackground(Void... voids) {
            try {
                FoodDatabase.getInstance(context).foodDAO().addFood(food);
            } catch (SQLiteConstraintException e) {
                e.printStackTrace();
                return -1;
            }
            return 1;

        }
    }

    private class UpdateFoodProductAsyncTask extends AsyncTask<Void, Void, Void> {
        Food food;
        Context context;

        private UpdateFoodProductAsyncTask(Food food, Context context) {
            this.food = food;
            this.context = context;
        }


        @Override
        protected Void doInBackground(Void... voids) {
            FoodDatabase.getInstance(context).foodDAO().updateFood(food);
            return null;
        }
    }

    private class DeleteFoodProductAsyncTask extends AsyncTask<Void, Void, Void> {
        Food food;
        Context context;

        private DeleteFoodProductAsyncTask(Food food, Context context) {
            this.food = food;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            FoodDatabase.getInstance(context).foodDAO().deleteFood(food);
            return null;
        }


    }


    private class ShowAllFoodProductsAsyncTask extends AsyncTask<Void, Void, List<Food>> {
        Context context;

        private ShowAllFoodProductsAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected List doInBackground(Void... voids) {
            List allFoodProducts = FoodDatabase.getInstance(context).foodDAO().getFood();
            return allFoodProducts;
        }
    }

    private class ShowFoodProductByNameAsyncTask extends AsyncTask<Void, Void, Food> {
        private String name;
        private Context context;

        private ShowFoodProductByNameAsyncTask(String name, Context context) {
            this.name = name;
            this.context = context;
        }

        @Override
        protected Food doInBackground(Void... voids) {
            Food food = FoodDatabase.getInstance(context).foodDAO().getOneFood(name);
            return food;
        }
    }
}
