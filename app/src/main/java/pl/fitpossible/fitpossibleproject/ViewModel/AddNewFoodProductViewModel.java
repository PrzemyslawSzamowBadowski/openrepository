package pl.fitpossible.fitpossibleproject.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import lombok.Data;

@Data
public class AddNewFoodProductViewModel extends AndroidViewModel {
    public AddNewFoodProductViewModel(@NonNull Application application) {
        super(application);
    }

    public String unit;

    public AddNewFoodProductViewModel(@NonNull Application application, String unit) {
        super(application);
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
