package pl.fitpossible.fitpossibleproject;

import android.content.Context;
import android.widget.Toast;

import org.threeten.bp.Duration;

import java.util.Calendar;

import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;

import static pl.fitpossible.fitpossibleproject.ApplicationGlobals.appUser;
import static pl.fitpossible.fitpossibleproject.ApplicationGlobals.loggedAppUserWeight;

public class Formulas {


    public static double appUserWeight = loggedAppUserWeight;//(MainActivity.appUserDatabase.weightDAO().getLastWeightByAppUserId(ApplicationGlobals.loggedAppUserId)).getWeight();
    // public static int appUserAge = MainActivity.appUserDatabase.appUserDAO().getAppUserById(ApplicationGlobals);

    static Long long1 = Calendar.getInstance().getTime().getTime() - appUser.getDateOfBirth().getTime();
    static Duration duration1 = Duration.ofMillis(long1);
    static int age = (int) duration1.toDays() / 365;

    public static int BMRbyHarrisa_BenedictaMethod(AppUser appUser, Context context) {
        int BMRbyHarrisa_BenedictaMethod = 0;
        if (appUser.getHeight() != null && loggedAppUserWeight != null) {
            // try {
            BMRbyHarrisa_BenedictaMethod = (int) (66 + (13.7 * appUserWeight) + (5 * appUser.getHeight()) - (6.76 * age));

//            } catch (NullPointerException e) {
//                Toast.makeText(context, "No weight set", Toast.LENGTH_SHORT).show();
//            }
        } else if (appUser.getHeight() == null) {
            Toast.makeText(context, "No height set", Toast.LENGTH_SHORT).show();
        } else if (loggedAppUserWeight == null) {
            Toast.makeText(context, "No weight set", Toast.LENGTH_SHORT).show();
        }
        return BMRbyHarrisa_BenedictaMethod;

    }
}
