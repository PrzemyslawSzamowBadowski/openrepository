package pl.fitpossible.fitpossibleproject;

import android.content.Context;
import android.util.Log;

import org.apache.commons.lang3.time.DateUtils;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.entity.Food;
import pl.fitpossible.fitpossibleproject.entity.MovementActivity;
import pl.fitpossible.fitpossibleproject.entity.NutritionHistory;
import pl.fitpossible.fitpossibleproject.entity.Weight;
import pl.fitpossible.fitpossibleproject.repository.ActivityHistoryRepository;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;
import pl.fitpossible.fitpossibleproject.repository.MovementActivityRepository;
import pl.fitpossible.fitpossibleproject.repository.NutritionHistoryRepository;
import pl.fitpossible.fitpossibleproject.repository.WeightRepository;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.getInstance;
import static pl.fitpossible.fitpossibleproject.MainActivity.appUserDatabase;

public class SampleDatabases {
    public AppUserRepository appUserRepository = new AppUserRepository();
    public MovementActivityRepository movementActivityRepository = new MovementActivityRepository();

    public static void pupulateFoodDatabase(Context context) {
        Food food1 = new Food();
        food1.setName("ananas");
        food1.setCaloriesPerUnit(20);
        food1.setUnit("item");

        Food food2 = new Food();
        food2.setName("arbuz");
        food2.setCaloriesPerUnit(25);
        food2.setUnit("item");

        Food food3 = new Food();
        food3.setName("awokado");
        food3.setCaloriesPerUnit(30);
        food3.setUnit("item");

        Food food4 = new Food();
        food4.setName("banan");
        food4.setCaloriesPerUnit(35);
        food4.setUnit("item");

        Food food5 = new Food();
        food5.setName("baklazan");
        food5.setCaloriesPerUnit(15);
        food5.setUnit("item");

        Food food6 = new Food();
        food6.setName("brukselka");
        food6.setCaloriesPerUnit(20);
        food6.setUnit("item");

        Food food7 = new Food();
        food7.setName("batat");
        food7.setCaloriesPerUnit(25);
        food7.setUnit("item");
        try {
            MainActivity.foodDatabase.foodDAO().addFood(food1);
            MainActivity.foodDatabase.foodDAO().addFood(food2);
            MainActivity.foodDatabase.foodDAO().addFood(food3);
            MainActivity.foodDatabase.foodDAO().addFood(food4);
            MainActivity.foodDatabase.foodDAO().addFood(food5);
            MainActivity.foodDatabase.foodDAO().addFood(food6);
            MainActivity.foodDatabase.foodDAO().addFood(food7);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            MovementActivity movementActivity1 = new MovementActivity();
            movementActivity1.setActivityName("Pushup");
            movementActivity1.setCaloriesPerUnit(400);
            movementActivity1.setUnitType("repetition");

            MovementActivity movementActivity2 = new MovementActivity();
            movementActivity2.setActivityName("Pullup");
            movementActivity2.setCaloriesPerUnit(500);
            movementActivity2.setUnitType("Repetition");

            MovementActivity movementActivity3 = new MovementActivity();
            movementActivity3.setActivityName("PowerSnatch");
            movementActivity3.setCaloriesPerUnit(350);
            movementActivity3.setUnitType("Repetition");

            MovementActivity movementActivity4 = new MovementActivity();
            movementActivity4.setActivityName("PowerClean");
            movementActivity4.setCaloriesPerUnit(400);
            movementActivity4.setUnitType("Repetition");

            MovementActivity movementActivity5 = new MovementActivity();
            movementActivity5.setActivityName("ProneBenchPress");
            movementActivity5.setCaloriesPerUnit(300);
            movementActivity5.setUnitType("Repetition");


            MovementActivityRepository movementActivityRepository = new MovementActivityRepository();
            movementActivityRepository.addMovementActivity(movementActivity1, context);
            movementActivityRepository.addMovementActivity(movementActivity2, context);
            movementActivityRepository.addMovementActivity(movementActivity3, context);
            movementActivityRepository.addMovementActivity(movementActivity4, context);
            movementActivityRepository.addMovementActivity(movementActivity5, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void populateUsers(Context context) {
        AppUserRepository appUserRepository = new AppUserRepository();
        ActivityHistoryRepository activityHistoryRepository = new ActivityHistoryRepository();
        NutritionHistoryRepository nutritionHistoryRepository = new NutritionHistoryRepository();

        AppUser appUser1 = new AppUser();
        appUser1.setLogin("login1");
        appUser1.setPassword("password1");

        AppUser appUser2 = new AppUser();
        appUser2.setLogin("login2");
        appUser2.setPassword("password2");

        AppUser a = new AppUser();
        a.setLogin("a");
        a.setPassword("a");
        a.setDateOfBirth(new Date(503190000000L));
        a.setHeight(175);

        try {
            appUserRepository.addAppUser(appUser1, context);
            appUserRepository.addAppUser(appUser2, context);
            appUserRepository.addAppUser(a, context);

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Date date = new Date(1545777522415L);
            Weight weight1 = new Weight();
            weight1.setAppUserId(1L);
            weight1.setDate(date);
            weight1.setWeight(89.5);

            Weight weight2 = new Weight();
            weight2.setAppUserId(2L);
            weight2.setDate(date);
            weight2.setWeight(99.5);

            Weight weight3 = new Weight();
            weight3.setAppUserId(3L);
            weight3.setDate(date);
            weight3.setWeight(79.5);

            appUserDatabase.weightDAO().addWeight(weight1);
            appUserDatabase.weightDAO().addWeight(weight2);
            appUserDatabase.weightDAO().addWeight(weight3);

            WeightRepository weightRepository = new WeightRepository();
            weightRepository.addWeight(weight1, context);
            weightRepository.addWeight(weight2, context);
            weightRepository.addWeight(weight3, context);


            ActivityHistory activityHistory1 = new ActivityHistory();
            activityHistory1.setAppUserId(1L);
            activityHistory1.setActivityType("bieganie");

            ActivityHistory activityHistory2 = new ActivityHistory();
            activityHistory2.setAppUserId(2L);
            activityHistory2.setActivityType("tyranie");


            activityHistoryRepository.addActivityHistory(activityHistory1, context);
            activityHistoryRepository.addActivityHistory(activityHistory2, context);
            Calendar c = getInstance();
            Log.d("calendar date", c.toString());


            Calendar truncatedToDay = DateUtils.truncate(c, DAY_OF_MONTH);
            Log.d("calendar date trun.", truncatedToDay.toString());
            Log.d("trunDay getTime", truncatedToDay.getTime().toString());
            Date truncatedToDayDate = truncatedToDay.getTime();
            Time timeOfMeal = new Time(0);
            timeOfMeal.setHours(c.getTime().getHours());
            timeOfMeal.setMinutes(c.getTime().getMinutes());
            NutritionHistory nutritionHistory = new NutritionHistory();
            nutritionHistory.setAppUserId(3L);
            nutritionHistory.setName("wodorosty");
            nutritionHistory.setCaloriesPerUnit(120);
            nutritionHistory.setDate(truncatedToDayDate);
            nutritionHistory.setUnit("kg");
            nutritionHistory.setNumberOfUnits(5.0F);
            nutritionHistory.setTime(timeOfMeal);

            nutritionHistoryRepository.addNutritionHistory(nutritionHistory, context);

            ActivityHistory activityHistory = new ActivityHistory();
            activityHistory.setAppUserId(3L);
            activityHistory.setActivityType("Lezenie plackiem");
            activityHistory.setCaloriesPerUnit(12);
            activityHistory.setReps(12);
            activityHistory.setDate(truncatedToDayDate);
            activityHistory.setTime(timeOfMeal);

            activityHistoryRepository.addActivityHistory(activityHistory, context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

