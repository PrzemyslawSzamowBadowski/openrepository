package pl.fitpossible.fitpossibleproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import pl.fitpossible.fitpossibleproject.databases.AppUserDatabase;
import pl.fitpossible.fitpossibleproject.databases.FoodDatabase;
import pl.fitpossible.fitpossibleproject.databases.StepsHistoryDatabase;
import pl.fitpossible.fitpossibleproject.fragments.MainUserPageFragment;

import static android.arch.persistence.room.Room.databaseBuilder;

public class MainActivity extends AppCompatActivity {
    public static FragmentManager fragmentManager;
    public static AppUserDatabase appUserDatabase;
    public static FoodDatabase foodDatabase;
    public static StepsHistoryDatabase stepsHistoryDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();

        prepareDatabase();

        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            goToHomeFragment();
        }
    }

    private void prepareDatabase() {
        stepsHistoryDatabase = databaseBuilder(getApplicationContext(), StepsHistoryDatabase.class, "stepshistorydb")
                .allowMainThreadQueries()
                .build();

        SampleDatabases.pupulateFoodDatabase(getApplicationContext());
        SampleDatabases.populateUsers(getApplicationContext());
    }

    private void goToHomeFragment() {
        Long id = getSharedPreferencesOfID();
        if (id == -1) {
            startActivity(new Intent(MainActivity.this, LoggingActivity.class));
        } else {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, new MainUserPageFragment())
                    .commit();
        }
    }

    private Long getSharedPreferencesOfID() {
        SharedPreferences sharedPreferences = getSharedPreferences("ID", MODE_PRIVATE);
        return sharedPreferences.getLong("LoggedAppUserID", -1);
    }
}
