package pl.fitpossible.fitpossibleproject.converter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import pl.fitpossible.fitpossibleproject.entity.StepsHistory;


public class StepsHistoryConverter {

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }



//    @TypeConverter // note this annotation
//    public String StepsHistory (List<StepsHistory> stepsHistoryList) {
//        if (stepsHistoryList == null) {
//            return (null);
//        }
//        Gson gson = new Gson();
//        Type type = new TypeToken<List<StepsHistory>>() {
//        }.getType();
//        String json = gson.toJson(stepsHistoryList, type);
//        return json;
//    }
//
//    @TypeConverter // note this annotation
//    public List<StepsHistory> toOptionValuesList(String stepsHistoryString) {
//        if (stepsHistoryString == null) {
//            return (null);
//        }
//        Gson gson = new Gson();
//        Type type = new TypeToken<List<StepsHistory>>() {
//        }.getType();
//        List<OptionValues> productCategoriesList = gson.fromJson(stepsHistoryString, type);
//        return productCategoriesList;
//    }
}
