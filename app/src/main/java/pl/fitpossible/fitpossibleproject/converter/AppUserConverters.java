package pl.fitpossible.fitpossibleproject.converter;

import android.arch.persistence.room.TypeConverter;

import java.sql.Time;
import java.util.Date;

import pl.fitpossible.fitpossibleproject.entity.Gender;
import pl.fitpossible.fitpossibleproject.entity.LifestyleType;

import static pl.fitpossible.fitpossibleproject.entity.Gender.FEMALE;
import static pl.fitpossible.fitpossibleproject.entity.Gender.MALE;
import static pl.fitpossible.fitpossibleproject.entity.LifestyleType.ACTIVE;
import static pl.fitpossible.fitpossibleproject.entity.LifestyleType.INACTIVE;
import static pl.fitpossible.fitpossibleproject.entity.LifestyleType.LOW_ACTIVE;
import static pl.fitpossible.fitpossibleproject.entity.LifestyleType.MODERATELY_ACTIVE;
import static pl.fitpossible.fitpossibleproject.entity.LifestyleType.VERY_ACTIVE;


public class AppUserConverters {

    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static Time fromTime(Long value) {
        return value == null ? null : new Time(value);
    }

    @TypeConverter
    public static Long dateToTime(Time time) {
        return time == null ? null : time.getTime();
    }
/////////////////////////////////////////////////////////////////////////////
    @TypeConverter
    public static Gender toGender(Integer numeral){
        if (numeral != null) {
            for(Gender s : Gender.values()){
                if(s.ordinal() == numeral){
                    return s;
                }
            }
        }
        return null;
    }
//
//    @TypeConverter
//    public static Integer getGenderInt(Gender gender){
//        return gender.numeral;
//    }

//    @TypeConverter
//    public static Gender toGender(Integer gender) {
//        if (gender != null) {
//            if (FEMALE.ordinal() == gender) {
//                return FEMALE;
//            } else if (MALE.ordinal() == gender) {
//                return MALE;
//            }
//        }
//
//        return null;
//    }

    @TypeConverter
    public static Integer toInteger(Gender gender) {
        if (gender != null) {
            return gender.ordinal();
        } else {
            return null;
        }
    }

    /////////////////////////////////////////////////////////////////////////
    @TypeConverter
    public static LifestyleType toLifestyleType(Integer lifestyleType) {
        if (lifestyleType != null) {
            if (INACTIVE.ordinal() == lifestyleType) {
                return INACTIVE;
            } else if (LOW_ACTIVE.ordinal() == lifestyleType) {
                return LOW_ACTIVE;
            } else if (MODERATELY_ACTIVE.ordinal() == lifestyleType) {
                return MODERATELY_ACTIVE;
            } else if (ACTIVE.ordinal() == lifestyleType) {
                return ACTIVE;
            } else if (VERY_ACTIVE.ordinal() == lifestyleType) {
                return VERY_ACTIVE;
            }
        }

        return null;
    }

    @TypeConverter
    public static Integer toInteger(LifestyleType lifestyleType) {
        if (lifestyleType != null) {
            return lifestyleType.ordinal();
        } else {
            return null;
        }
    }

}
