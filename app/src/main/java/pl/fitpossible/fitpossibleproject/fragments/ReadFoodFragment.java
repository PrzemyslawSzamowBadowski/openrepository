package pl.fitpossible.fitpossibleproject.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.databases.FoodDatabase;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.entity.Food;
import pl.fitpossible.fitpossibleproject.repository.FoodRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReadFoodFragment extends Fragment {
    private TextView ViewAllFood;

    public ReadFoodFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_read_food, container, false);

        ViewAllFood = view.findViewById(R.id.txt_view_all_Food);

        String info = getFoodInfo();

        ViewAllFood.setText(info);

        return view;
    }

    @NonNull
    private String getFoodInfo() {

        String info = "";
        try {
            FoodRepository foodRepository = new FoodRepository();
            List<Food> foodList = foodRepository.showAllFoodProducts(getContext());//MainActivity.foodDatabase.foodDAO().getFood();


            for (Food food : foodList) {
                long id = food.getId();
                String name = food.getName();
                Integer caloriesPerUnit = food.getCaloriesPerUnit();
                String unit = food.getUnit();

                info = info + "\n\n" + "Id: " + id + "\n" + "Name: " + name + "\n" + "Calories per unit: " + caloriesPerUnit + "\n " + "Unit: " + unit;
            }
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return info;
    }

}
