package pl.fitpossible.fitpossibleproject.fragments;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import java.util.List;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.repository.ActivityHistoryRepository;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;



public class LoggingFragmentViewModel extends AndroidViewModel {

    private AppUserRepository appUserRepository;
    private List<AppUser> allAppUsers;
    private AppUser appUserById;
    private AppUser appUserByLogin;

    public LoggingFragmentViewModel(@NonNull Application application) {
        super(application);
        appUserRepository = new AppUserRepository();
        allAppUsers = appUserRepository.getAppUsers(application.getApplicationContext());
        appUserById = appUserRepository.getAppUserById();
        appUserByLogin = appUserRepository.getAppUserByLogin();

        //activityHistoryById = activityHistoryRepository.getActivityHistoryById();
    }

    public void insert(AppUser appUser, Context context) {
        appUserRepository.addAppUser(appUser,context);
    }

    public void update(AppUser appUser) {
        appUserRepository.updateAppUser(appUser);
    }

    public void delete(AppUser appUser) {
        appUserRepository.deleteAppUser(appUser);
    }

    public List<AppUser> getAllAppUsers() {
        return allAppUsers;
    }

    public AppUser getAppUserById(Long id) {
        return appUserById;
    }

    public AppUser getAppUserByLogin(String login) {
        return appUserByLogin;
    }
}
