package pl.fitpossible.fitpossibleproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;


public class ChangePasswordFragment extends Fragment implements View.OnClickListener {

    EditText EdTxtOldPassword;
    EditText EdTxtNewPassword;
    EditText EdTxtConfirmPassword;
    Button BnChangePassword;

    AppUserRepository appUserRepository = new AppUserRepository();


    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_change_password, container, false);

        EdTxtOldPassword = view.findViewById(R.id.edTxt_old_password);
        EdTxtNewPassword = view.findViewById(R.id.edTxt_new_password);
        EdTxtConfirmPassword = view.findViewById(R.id.ed_txt_confirm_password);

        BnChangePassword = view.findViewById(R.id.bn_submit_change_password);
        BnChangePassword.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {


//
        String passwordFromEdTxtOldPassword = EdTxtOldPassword.getText().toString();
        String passwordFromEdTxtNewPassword = EdTxtNewPassword.getText().toString();
        String passwordFromEdTxtConfirmPassword = EdTxtConfirmPassword.getText().toString();

        String passwordOfAppUserFromApplicationGlobalsString = getStringOfOldPassword();
        //ToDo:Change this code to boolean
        compareOldPasswordWithLoggedAppUserPassword(passwordFromEdTxtOldPassword, passwordFromEdTxtNewPassword, passwordFromEdTxtConfirmPassword, passwordOfAppUserFromApplicationGlobalsString);

    }

    private void compareOldPasswordWithLoggedAppUserPassword(String passwordFromEdTxtOldPassword, String passwordFromEdTxtNewPassword, String passwordFromEdTxtConfirmPassword, String passwordOfAppUserFromApplicationGlobalsString) {
        if (passwordFromEdTxtOldPassword.equals(passwordOfAppUserFromApplicationGlobalsString)) {

            compareNewPasswordWithConfirmation(passwordFromEdTxtNewPassword, passwordFromEdTxtConfirmPassword);
        } else {
            Toast.makeText(getContext(), "Old password doesn't match AppUser's password", Toast.LENGTH_SHORT).show();
        }
    }

    private void compareNewPasswordWithConfirmation(String passwordFromEdTxtNewPassword, String passwordFromEdTxtConfirmPassword) {
        if (passwordFromEdTxtConfirmPassword.equals(passwordFromEdTxtNewPassword)) {
            updatePasswordOfCurrentAppuser(passwordFromEdTxtNewPassword);
        } else {
            Toast.makeText(getContext(), "Confirmation doesn't match new password", Toast.LENGTH_SHORT).show();
        }
    }

    private void updatePasswordOfCurrentAppuser(String passwordFromEdTxtNewPassword) {
        String newPasswordOfAppUser = Base64.encodeToString(passwordFromEdTxtNewPassword.getBytes(), Base64.NO_WRAP);
        ApplicationGlobals.appUser.setPassword(newPasswordOfAppUser);
        appUserRepository.updateAppUser(ApplicationGlobals.appUser);
        //MainActivity.appUserDatabase.appUserDAO().updateAppUser(ApplicationGlobals.appUser);
        Toast.makeText(getContext(), "AppUser's password updated", Toast.LENGTH_SHORT).show();
    }

    private String getStringOfOldPassword() {
        String passwordOfAppUserFromApplicationGlobalsString = "";
        try {
            byte[] passwordOfAppUserFromApplicationGlobals = (Base64.decode(ApplicationGlobals.appUser.getPassword(), Base64.NO_WRAP));
            try {
                passwordOfAppUserFromApplicationGlobalsString = new String(passwordOfAppUserFromApplicationGlobals, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } catch (IllegalArgumentException e) {
            passwordOfAppUserFromApplicationGlobalsString = ApplicationGlobals.appUser.getPassword();

        }
        return passwordOfAppUserFromApplicationGlobalsString;
    }
}
