package pl.fitpossible.fitpossibleproject.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeleteUserFragment extends Fragment implements View.OnClickListener {
    private EditText EdTxtPasswordToDeleteAppUser;
    private Button BnDeleteUser;

    public DeleteUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delete_user, container, false);

        EdTxtPasswordToDeleteAppUser = view.findViewById(R.id.edtxt_password_to_delete_appuser);

        BnDeleteUser = view.findViewById(R.id.bn_delete_user);
        BnDeleteUser.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View view) {
        String passwordFromEdTxtPasswordtoDeleteAppUser = EdTxtPasswordToDeleteAppUser.getText().toString();

        String passwordOfAppUserFromApplicationGlobalsString = getPasswordOfLoggedAppUser();


        if (passwordFromEdTxtPasswordtoDeleteAppUser.equals(passwordOfAppUserFromApplicationGlobalsString)) {

            showAlertDialog();
            //getActivity().finishAndRemoveTask();

        } else {
            Toast.makeText(getContext(), "You're password doesn't match " + ApplicationGlobals.loggedAppUserName + "password", Toast.LENGTH_SHORT).show();
        }
    }

    private String getPasswordOfLoggedAppUser() {
        String passwordOfAppUserFromApplicationGlobalsString = "";
        try {
            if (ApplicationGlobals.appUser.getPassword().matches("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$")) {
                passwordOfAppUserFromApplicationGlobalsString = new String(Base64.decode(ApplicationGlobals.appUser.getPassword(), Base64.NO_WRAP), "UTF-8");

            } else {
                throw new IllegalArgumentException();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Something went wrong while decoding you're password", Toast.LENGTH_LONG).show();
        } catch (IllegalArgumentException e) {
            passwordOfAppUserFromApplicationGlobalsString = ApplicationGlobals.appUser.getPassword();
        }
        return passwordOfAppUserFromApplicationGlobalsString;
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.app_name);
        builder.setMessage("Do you really want to delete current user?");
        //   builder.setIcon(R.drawable.ic_launcher);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                try {
                    deleteAppUser();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deleteAppUser() {
        AppUserRepository appUserRepository = new AppUserRepository();
        appUserRepository.deleteAppUser(ApplicationGlobals.appUser);
//        MainActivity.appUserDatabase.appUserDAO().deleteAppUser(ApplicationGlobals.appUser);
        Toast.makeText(getActivity(), "User " + ApplicationGlobals.loggedAppUserName + " deleted", Toast.LENGTH_LONG).show();
        MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new LoggingFragment()).addToBackStack(null).commit();
    }


}
