package pl.fitpossible.fitpossibleproject.fragments;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import java.util.Date;
import java.util.List;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.dao.ActivityHistoryDAO;
import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;
import pl.fitpossible.fitpossibleproject.repository.ActivityHistoryRepository;

public class UserChartsFragmentViewModel extends AndroidViewModel {
    private ActivityHistoryRepository activityHistoryRepository;
    private List<ActivityHistory> allActivityHistory;
    private List<ActivityHistory> allActivityHistoryById;
    private List<ActivityHistory> allActivityHistoryByIdAndDate;
    // private LiveData<List<ActivityHistory>> activityHistoryById;

    Long id = ApplicationGlobals.loggedAppUserId;
    Date date;

    public UserChartsFragmentViewModel(@NonNull Application application) {
        super(application);
        ActivityHistoryDAO activityHistoryDAO = null;
        activityHistoryRepository = new ActivityHistoryRepository();
        allActivityHistory = activityHistoryRepository.getActivityHistories();
        allActivityHistoryById = activityHistoryRepository.getActivityHistoryById(id, application.getApplicationContext());
        allActivityHistoryByIdAndDate = activityHistoryRepository.getActivityHistoryByIdAndDate(id, date, getApplication().getApplicationContext());

        //activityHistoryById = activityHistoryRepository.getActivityHistoryById();
    }


    public void insert(ActivityHistory activityHistory) {
        activityHistoryRepository.addActivityHistory(activityHistory, getApplication().getApplicationContext());
    }

    public void update(ActivityHistory activityHistory) {
        activityHistoryRepository.updateActivityHistory(activityHistory);
    }

    public void delete(ActivityHistory activityHistory) {
        activityHistoryRepository.deleteActivityHistory(activityHistory);
    }

    public List<ActivityHistory> getAllActivityHistory() {
        return allActivityHistory;
    }

    public List<ActivityHistory> getAllActivityHistoryById(Long id) {
        return allActivityHistoryById;
    }

    public List<ActivityHistory> getActivityHistoryByIdAndDate() {
        return allActivityHistoryByIdAndDate;
    }
}
