package pl.fitpossible.fitpossibleproject.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.RecyclerViewAdapterForLogging;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;
import pl.fitpossible.fitpossibleproject.repository.AvatarImageRepository;

import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReadAppUsersFragment extends Fragment {
    private TextView ViewAllAppUsers;
    private RecyclerView recyclerView;
    private RecyclerViewAdapterForLogging recyclerViewAdapterForLogging;
    private AppUserRepository appUserRepository = new AppUserRepository();
    private AvatarImageRepository avatarImageRepository = new AvatarImageRepository();

    ReadAppUsersFragmentViewModel readAppUsersFragmentViewModel;

    public ReadAppUsersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(TAG, "initRecyclerView: init recyclerView");

        View view = inflater.inflate(R.layout.fragment_read_app_users, container, false);
        Context context = getActivity().getApplicationContext();


        //ArrayList<String> appUserLoginList = new ArrayList<>();

        ArrayList<AppUser> appUserList = (ArrayList<AppUser>) appUserRepository.getAppUsers(this.getContext());//(ArrayList) readAppUsersFragmentViewModel.getAllAppUsers();//MainActivity.appUserDatabase.appUserDAO().getAppUsers();

//        if (appUserList.size() != 0) {
//        }//{
//            for (int i = 0; i < appUserList.size(); i++) {
//                appUserLoginList.add(appUserList.get(i).getLogin());
//            }
        ArrayList<String> appUserLoginList = (ArrayList<String>) appUserRepository.getAppUserLoginList();//(ArrayList) MainActivity.appUserDatabase.appUserDAO().getAppUsersLogin();


        String[] avatarsArray = getResources().getStringArray(R.array.drawables);
        ArrayList<String> avatars = new ArrayList<>();
        for (int j = 0; j < avatarsArray.length; j++) {
            if (avatarImageRepository.getOneStringAppUserAvatarImage((long) j, getContext()) != null) {
                avatars.add(avatarImageRepository.getOneStringAppUserAvatarImage((long) j, getContext()));

            } else {
                avatars.add(avatarsArray[j]);
            }

        }


        recyclerView = view.findViewById(R.id.recyclerView_for_read_app_users);
        //  ViewAllAppUsers = view.findViewById(R.id.txt_view_all_appUsers);
        recyclerViewAdapterForLogging = new RecyclerViewAdapterForLogging();

        //   recyclerViewAdapterForLogging.setMAvatars(avatars);
        recyclerViewAdapterForLogging.setMAppUser(appUserList);
        recyclerViewAdapterForLogging.setMContext(context);

        recyclerView.setAdapter(recyclerViewAdapterForLogging);
        //recyclerView.setLayoutManager(new LinearLayoutManager(context));

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        recyclerView.setLayoutManager(mLayoutManager);
//        } else {
        if (appUserList.size() == 0) {
            Toast.makeText(getContext(), "There are no AppUsers yet", Toast.LENGTH_LONG).show();
        }
//        }
        return view;
    }

}

