package pl.fitpossible.fitpossibleproject.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.dao.NutritionHistoryDAO;
import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;
import pl.fitpossible.fitpossibleproject.entity.NutritionHistory;
import pl.fitpossible.fitpossibleproject.repository.NutritionHistoryRepository;

import static java.util.Calendar.DAY_OF_MONTH;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShowMealFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerViewAdapterForShowMeal recyclerViewAdapterForShowMeal;


    Bundle bundle;
    TextView TxtDateOfMeal;
    TextView TxtNameOfMeal;
    TextView TxtCaloriesOfMeal;
    TextView TxtEmptyShowMealDatabase;
    Long appUserId = ApplicationGlobals.loggedAppUserId;
    Long mealDate;
    Date dateTruncateItd;
    List<NutritionHistory> meal;
    NutritionHistoryRepository nutritionHistoryRepository = new NutritionHistoryRepository();
    NutritionHistoryDAO nutritionHistoryDAO;

    public ShowMealFragment() {
        // Required empty public constructor
    }

    Boolean mealDataBaseIsNotEmpty() {
        if (nutritionHistoryRepository.getNutritionHistoryByIdAndDate(appUserId, dateTruncateItd, getContext()).isEmpty() == false) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_meal, container, false);

        Context context = getActivity().getApplicationContext();


        // Inflate the layout for this fragment
        TxtDateOfMeal = view.findViewById(R.id.txt_date_of_meal);
        TxtNameOfMeal = view.findViewById(R.id.txt_name_of_meal);
        TxtCaloriesOfMeal = view.findViewById(R.id.txt_calories_of_meal);
        recyclerView = view.findViewById(R.id.recyclerView_for_show_meal);
        TxtEmptyShowMealDatabase = view.findViewById(R.id.txt_empty_show_meal_database);
        if ((getArguments() != null) && (getArguments().containsKey("MealDate") == true)) {
            bundle = getArguments();
            Long dateLong = bundle.getLong("MealDate");
            Log.d("dateLongL", dateLong.toString());
            String dateString = DateFormat.format("dd/MM/yyyy", new Date(dateLong)).toString();
            Date dateFromCalendar = new Date(dateLong);
            dateTruncateItd = DateUtils.truncate(dateFromCalendar, DAY_OF_MONTH);


            TxtDateOfMeal.setText(dateString);
            mealDate = dateLong;

        }


        if (mealDataBaseIsNotEmpty()) {
            meal = nutritionHistoryRepository.getNutritionHistoryByIdAndDate(appUserId, dateTruncateItd, getContext());//MainActivity.appUserDatabase.nutritionHistoryDAO().getNutritionHistoryByIdAndDate(appUserId, dateTruncateItd);
        }


        if (mealDataBaseIsNotEmpty()) {
            TxtEmptyShowMealDatabase.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerViewAdapterForShowMeal = new RecyclerViewAdapterForShowMeal();
            recyclerViewAdapterForShowMeal.setMNutritionHistories((ArrayList) meal);

            recyclerViewAdapterForShowMeal.setMContext(context);
            recyclerView.setAdapter(recyclerViewAdapterForShowMeal);

            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
        } else {
            TxtEmptyShowMealDatabase.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        return view;
    }


    @Data
    public class RecyclerViewAdapterForShowMeal extends RecyclerView.Adapter<pl.fitpossible.fitpossibleproject.fragments.ShowMealFragment.RecyclerViewAdapterForShowMeal.ViewHolder> {
        private ArrayList<NutritionHistory> mNutritionHistories = new ArrayList<>();
        private Context mContext;

        @NonNull
        @Override
        //Tu robisz view określając layouta którego bedziesz powielać i viewholdera na jego podstawie
        public pl.fitpossible.fitpossibleproject.fragments.ShowMealFragment.RecyclerViewAdapterForShowMeal.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_show_meal_layout, parent, false);
            pl.fitpossible.fitpossibleproject.fragments.ShowMealFragment.RecyclerViewAdapterForShowMeal.ViewHolder holder = new pl.fitpossible.fitpossibleproject.fragments.ShowMealFragment.RecyclerViewAdapterForShowMeal.ViewHolder(view);
            return holder;
        }

        @Override
        //tu wpisujesz co ma sie znaleźć w każdym okienku z layouta i jak ma zadziałać
        public void onBindViewHolder(@NonNull pl.fitpossible.fitpossibleproject.fragments.ShowMealFragment.RecyclerViewAdapterForShowMeal.ViewHolder holder, int position) {
            if ((mNutritionHistories.get(position).getTime()) != null) {
                SimpleDateFormat time_format = new SimpleDateFormat("HH:mm");
                String hour = time_format.format(mNutritionHistories.get(position).getTime());
                holder.hour.setText(hour);
            } else {
                holder.hour.setText("no hour added");
            }
            holder.mealName.setText(mNutritionHistories.get(position).getName());
            String caloriesString = ((Float) (mNutritionHistories.get(position).getNumberOfUnits() * mNutritionHistories.get(position).getCaloriesPerUnit())).toString();
            holder.calories.setText(caloriesString);
            System.out.println(mNutritionHistories.get(position).toString());
        }

        @Override
        public int getItemCount() {
            return mNutritionHistories.size();
        }

        //tu określasz jakie elementy layouta chcesz zawrzeć we Viewholderze
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView hour;
            TextView mealName;
            TextView calories;
            Button bnDeleteMeal;

            ConstraintLayout parentLayoutShowMeal;

            public ViewHolder(View itemView) {

                super(itemView);

                hour = itemView.findViewById(R.id.txt_hour_of_meal);
                mealName = itemView.findViewById(R.id.txt_name_of_meal);
                calories = itemView.findViewById(R.id.txt_calories_of_meal);
                parentLayoutShowMeal = itemView.findViewById(R.id.parent_layout_show_meal);
                bnDeleteMeal = itemView.findViewById(R.id.bn_delete_this_meal);
                bnDeleteMeal.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                final NutritionHistory nutritionHistory = mNutritionHistories.get(this.getAdapterPosition());
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("FitPossible");
                builder.setMessage("Do you really want to delete " + nutritionHistory.getName());

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            NutritionHistory nutritionHistory = mNutritionHistories.get(getAdapterPosition());
                            MainActivity.appUserDatabase.nutritionHistoryDAO().deleteNutritionHistory(nutritionHistory);
                            mNutritionHistories.remove(nutritionHistory);
                            notifyItemRemoved(getLayoutPosition());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Couldn't delete " + nutritionHistory.getName() + " form Nutrition History", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        }
    }

}
