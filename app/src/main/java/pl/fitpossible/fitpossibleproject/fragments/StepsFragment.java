package pl.fitpossible.fitpossibleproject.fragments;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.LifestyleType;
import pl.fitpossible.fitpossibleproject.entity.StepsHistory;

/**
 * A simple {@link Fragment} subclass.
 */
public class StepsFragment extends Fragment implements SensorEventListener {
    TextView TxtAllSteps;
    TextView TxtEachStep;
    TextView TxtStepsFromDatabase;
    SensorManager sensorManager;
    boolean running = false;
    Float stepCounter = 0.0f;

    public StepsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_steps, container, false);
        TxtAllSteps = (TextView) view.findViewById(R.id.tv_steps);
        TxtEachStep = (TextView) view.findViewById(R.id.tv_eachStep);
        TxtStepsFromDatabase = (TextView) view.findViewById(R.id.tv_steps_from_database);
        sensorManager = (SensorManager) getLayoutInflater().getContext().getSystemService(Context.SENSOR_SERVICE);
        return view;
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        tv_steps = (TextView) findViewById(R.id.tv_steps);
//        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
//    }

    @Override
    public void onResume() {
        super.onResume();
        running = true;
        Sensor countAllSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        Sensor countEachSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        if (countAllSensor != null) {
            sensorManager.registerListener(this, countAllSensor, SensorManager.SENSOR_DELAY_UI);
        }
        if (countEachSensor != null) {
            sensorManager.registerListener(this, countEachSensor, SensorManager.SENSOR_DELAY_UI);
        } else {
            Toast.makeText(getLayoutInflater().getContext(), "Sensor not found!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        running = false;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (running) {
//            if(MainActivity.stepsHistoryDatabase.stepsHistoryDAO().getOneStepHistorybyId()!=null){
//                StepsHistory stepsHistory = MainActivity.stepsHistoryDatabase.stepsHistoryDAO().getOneStepHistorybyId();
//            }else{StepsHistory stepsHistory =0;}

            if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
                TxtAllSteps.setText(String.valueOf(event.values[0]));
            }
            if (event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {

                TxtEachStep.setText(String.valueOf(event.values[0]));

                StepsHistory stepsOnDay = new StepsHistory();
                stepsOnDay.setSteps(event.values[0]);
                stepsOnDay.setDateOfSteps(new Date(event.timestamp));
                MainActivity.stepsHistoryDatabase.stepsHistoryDAO().addStepHistory(stepsOnDay);
                //                if (!stepsOnDay.getDateOfSteps().equals(new Date())) {
//                    MainActivity.stepsHistoryDatabase.stepsHistoryDAO().addStepHistory(stepsOnDay);
//                } //else {
                //MainActivity.stepsHistoryDatabase.stepsHistoryDAO().updateStepsHistory(stepsOnDay);
                //}
//                StepsHistory stepsHistory2 = MainActivity.stepsHistoryDatabase.stepsHistoryDAO().getOneStepHistorybyId();
//                if (stepsHistory2 != null) {
//                    String s = stepsHistory2.getSteps().toString();
//                    TxtStepsFromDatabase.setText(s);
//                }
                Integer recentSteps = MainActivity.stepsHistoryDatabase.stepsHistoryDAO().getstepsHistory().size();
//                if (recentSteps!=null){
//                    if(recentSteps <2500){MainActivity.appUserDatabase.appUserDAO().updateAppUser(ApplicationGlobals.appUser.setLifestyle(LifestyleType.INACTIVE));
//                }

                if (recentSteps != null) {
                    String s = recentSteps.toString();
                    TxtStepsFromDatabase.setText(s);
                }
            }
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
