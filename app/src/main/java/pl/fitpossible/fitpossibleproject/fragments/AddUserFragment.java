package pl.fitpossible.fitpossibleproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pl.fitpossible.fitpossibleproject.LoggingActivity;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddUserFragment extends Fragment implements View.OnClickListener {
    //private EditText UserId;
    private EditText UserName;
    private EditText AppUserPassword;
    private Button BnSaveAppUser;
    private AppUserRepository appUserRepository = new AppUserRepository();

    public AddUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_user, container, false);


        // UserId = view.findViewById(R.id.txt_user_id);
        UserName = view.findViewById(R.id.txt_show_login);
        AppUserPassword = view.findViewById(R.id.txt_appUser_password);

        BnSaveAppUser = view.findViewById(R.id.bn_add_appUser);
        BnSaveAppUser.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        //przypisanie wpisanego tekstu z okienek do zmiennych
        // int userId = Integer.parseInt(UserId.getText().toString());
        String appUserName = UserName.getText().toString();
        String appUserPassword = AppUserPassword.getText().toString();
        String encodedAppUserName = new String(Base64.encode(appUserName.getBytes(), Base64.NO_WRAP));
        String encodedappUserPassword = new String(Base64.encode(appUserPassword.getBytes(), Base64.NO_WRAP));

        //stworzenie obiektu
        addAppUserToDatabase(appUserName, appUserPassword, encodedAppUserName, encodedappUserPassword);


        //zresetowanie tekstu w okienkach
        // UserId.setText("");
        resetEdTxt();

    }

    private void addAppUserToDatabase(String appUserName, String appUserPassword, String encodedAppUserName, String encodedappUserPassword) {
        AppUser appUser = new AppUser();
        //appUser.setId(userId);
        appUser.setLogin(encodedAppUserName);
        appUser.setPassword(encodedappUserPassword);

        //dodanie usera
        Integer massage = appUserRepository.addAppUser(appUser, getContext());//MainActivity.appUserDatabase.appUserDAO().addAppUser(appUser);
        //wyświetlenie komunikatu
        switch (massage) {
            case -1:
                Toast.makeText(getActivity(), "Something went wrong while adding new profile \nPlease check you're profile.", Toast.LENGTH_SHORT).show();
                break;
            case 0:
                Toast.makeText(getActivity(), "User " + appUserName + " already added!", Toast.LENGTH_LONG).show();
                break;
            case 1:
                Toast.makeText(getActivity(), "User Added!\n" + "name: " + appUserName, Toast.LENGTH_SHORT).show();
                goToLoggingFragment(appUserName, appUserPassword);
                break;
        }
    }


    private void goToLoggingFragment(String appUserName, String appUserPassword) {
        Bundle bundle = new Bundle();
        bundle.putString("CurrentAppUserName", appUserName);
        bundle.putString("CurrentAppUserPassword", appUserPassword);
        LoggingFragment loggingFragment = new LoggingFragment();
        loggingFragment.setArguments(bundle);
        LoggingActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, loggingFragment).
                addToBackStack(null).commit();
    }

    private void resetEdTxt() {
        UserName.setText("");
        AppUserPassword.setText("");
    }
}
