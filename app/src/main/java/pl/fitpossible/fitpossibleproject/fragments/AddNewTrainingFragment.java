package pl.fitpossible.fitpossibleproject.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.lang3.time.DateUtils;

import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;
import pl.fitpossible.fitpossibleproject.entity.ActivityType;
import pl.fitpossible.fitpossibleproject.entity.MovementActivity;
import pl.fitpossible.fitpossibleproject.repository.ActivityHistoryRepository;
import pl.fitpossible.fitpossibleproject.repository.MovementActivityRepository;

import static android.R.layout.simple_list_item_1;
import static java.util.Calendar.getInstance;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNewTrainingFragment extends Fragment implements View.OnClickListener {

    EditText EdTxtDateOfTraining;
    EditText EdTxtTimeOfTrianing;
    AutoCompleteTextView EdTxtNameOfTraining;
    EditText EdTxtCaloriesPerUnit;
    EditText EdTxtUnitType;
    EditText EdTxtNumberOfReps;
    Button BnSubmitNewTraining;

    Bundle bundle;
    Long trainingDate;
    Date dateTruncateItd;
    Time timeOfTraining;
    ActivityHistoryRepository activityHistoryRepository = new ActivityHistoryRepository();
    MovementActivityRepository movementActivityRepository = new MovementActivityRepository();

    public AddNewTrainingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_new_training, container, false);

        EdTxtDateOfTraining = view.findViewById(R.id.edTxt_date_of_training);
        EdTxtNameOfTraining = view.findViewById(R.id.edTxt_name_of_activity);
        EdTxtCaloriesPerUnit = view.findViewById(R.id.edTxt_calories_per_unit);
        EdTxtUnitType = view.findViewById(R.id.edTxt_unit_type);
        EdTxtNumberOfReps = view.findViewById(R.id.edTxt_reps_done);
        EdTxtTimeOfTrianing = view.findViewById(R.id.edTxt_time_of_activity);
        BnSubmitNewTraining = view.findViewById(R.id.bn_submit_new_training);
        BnSubmitNewTraining.setOnClickListener(this);

        getDateOfTrainingFromBundleString();
        //Date dateFromCalendar = new Date(dateLong);
        //dateTruncateItd = DateUtils.truncate(dateFromCalendar, DAY_OF_MONTH);
        EdTxtDateOfTraining.setText(getDateOfTrainingFromBundleString());
        EdTxtTimeOfTrianing.setText(getCurrentTimeOfTrainingString());
        //  EdTxtTimeOfTrianing.setText(timeOfTraining.toString());
        //trainingDate = dateLong;
        autocompliteTraining();
        return view;
    }

    private void autocompliteTraining() {
        EdTxtNameOfTraining.setAdapter(getStringArrayAdapterOfTraining());

        EdTxtNameOfTraining.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    String nameOfTraining = EdTxtNameOfTraining.getText().toString();
                    MovementActivity movementActivity = movementActivityRepository.getMovementActivityByName(nameOfTraining, getContext());//MainActivity.movementActivityDatabase.movementActivityDAO().getMovementActivity(nameOfTraining);
                    EdTxtCaloriesPerUnit.setText(((Integer) movementActivity.getCaloriesPerUnit()).toString());
                    EdTxtUnitType.setText((movementActivity.getUnitType()));
                } catch (Exception e) {
                    Log.d("You've got: ", e.getCause() + " You'd better change you're code here!" );
                }
            }
        });
    }

    @NonNull
    private ArrayAdapter<String> getStringArrayAdapterOfTraining() {
        List<MovementActivity> movementActivityList = movementActivityRepository.getAllMovementActivity(getContext());//MainActivity.movementActivityDatabase.movementActivityDAO().getMovementActivity();
        TreeSet<String> trainingNameSet = new TreeSet<String>();
        for (int i = 0; i < movementActivityList.size(); i++) {
            trainingNameSet.add(movementActivityList.get(i).getActivityName());
        }
        ArrayList<String> trainingNameArrayList = new ArrayList<String>();

        for (String s : trainingNameSet) {
            trainingNameArrayList.add(s);
        }
        // final String[] foodNameArray = foodNameArrayList.toArray(new String[foodNameArrayList.size()]);

        // final String[]foodProductName = new String[]{"Ananas","Arbuz","Banan","Baklazan"};

        return new ArrayAdapter<String>(this.getContext(), simple_list_item_1, trainingNameArrayList);
    }

    private String getDateOfTrainingFromBundleString() {
        if ((getArguments() != null) && (getArguments().containsKey("TrainingDate" ) == true)) {
            bundle = getArguments();
            Long dateLong = bundle.getLong("TrainingDate" );
            Log.d("dateLongL", dateLong.toString());
            String dateString = DateFormat.format("dd/MM/yyyy", new Date(dateLong)).toString();
            return dateString;
        }
        return DateFormat.format("dd/MM/yyyy", new Date()).toString();
    }

    @NonNull
    private String getCurrentTimeOfTrainingString() {
        Calendar c = getInstance();
        Time timeOfTraining = new Time(0);
        timeOfTraining.setHours(c.getTime().getHours());
        timeOfTraining.setMinutes(c.getTime().getMinutes());


        Long timeOfTrainingLong = timeOfTraining.getTime();
        return DateFormat.format("HH:mm", new Time(timeOfTrainingLong)).toString();
    }

    @Override
    public void onClick(View v) {

        Date dateOfTraining = getDateFromEdTxt();
        Integer numberOfReps = 0;
        Integer caloriesPerHour = 0;
        if (dateOfTraining == null) return;

        timeOfTraining = getTimeFromEdTxt();

        String nameOfTraining = EdTxtNameOfTraining.getText().toString();
        try{
            caloriesPerHour = Integer.parseInt(EdTxtCaloriesPerUnit.getText().toString());
        }catch (NumberFormatException e){
            e.printStackTrace();
            Toast.makeText(getContext(), "Value must not be empty", Toast.LENGTH_LONG).show();
        }
        try {
            numberOfReps = Integer.parseInt(EdTxtNumberOfReps.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Value must not be empty", Toast.LENGTH_LONG).show();
        }
        String nameOfUnits = EdTxtUnitType.getText().toString();
        try {
            if (!(numberOfReps <= 0)) {
                addTrainingToDatabase(dateOfTraining, timeOfTraining, nameOfTraining, caloriesPerHour, numberOfReps, nameOfUnits);
                resetEdTextViews();
            } else {
                Toast.makeText(getContext(), "Training not added\nNumber of Units ", Toast.LENGTH_LONG).show();
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Training not added\nWrong value", Toast.LENGTH_LONG).show();

        }

    }

    @NonNull
    private Time getTimeFromEdTxt() {
        Time timeOfTraining = new Time(0);
        String timeOfTrainingString = "";
        try {
            timeOfTrainingString = EdTxtTimeOfTrianing.getText().toString();
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Toast.makeText(this.getContext(), "Wrong  time format", Toast.LENGTH_LONG).show();
        }
        try {
            Long timeLong = DateUtils.parseDate(timeOfTrainingString, "HH:mm" ).getTime();
            timeOfTraining = new Time(timeLong);
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(this.getContext(), "Wrong time format", Toast.LENGTH_LONG).show();
        }
        return timeOfTraining;
    }

    @Nullable
    private Date getDateFromEdTxt() {
        Date dateOfTraining;
        String dateOfTrainingString = "";
        try {
            dateOfTrainingString = EdTxtDateOfTraining.getText().toString();
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Toast.makeText(this.getContext(), "Wrong date format", Toast.LENGTH_LONG).show();
        }
        try {
            dateOfTraining = DateUtils.parseDate(dateOfTrainingString, "dd/MM/yyyy" );
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(this.getContext(), "Wrong date format", Toast.LENGTH_LONG).show();
            return null;
        }
        return dateOfTraining;
    }

    private void addTrainingToDatabase(Date dateOfTraining, Date timeOfTraining, final String
            nameOfTraining, final Integer caloriesPerUnit, Integer numberOfReps, final String nameOfUnits) {
        ActivityHistory activityHistory = new ActivityHistory();
        activityHistory.setAppUserId(ApplicationGlobals.loggedAppUserId);
        activityHistory.setDate(dateOfTraining);
        activityHistory.setActivityType(nameOfTraining);
        activityHistory.setCaloriesPerUnit(caloriesPerUnit);
        activityHistory.setReps(numberOfReps);
        activityHistory.setTime(timeOfTraining);
        activityHistory.setUnitType(nameOfUnits);
        activityHistoryRepository.addActivityHistory(activityHistory, getContext());
//        MainActivity.appUserDatabase.activityHistoryDAO().addActivityHistory(activityHistory);
        if (movementActivityRepository.getMovementActivityByName(nameOfTraining,getContext()) == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("FitPossible" );
            builder.setMessage("There is no " + nameOfTraining + " in your Activity Type Database\nDo you want to add it?" );
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        MovementActivity movementActivity = new MovementActivity();
                        movementActivity.setActivityName(nameOfTraining);
                        movementActivity.setCaloriesPerUnit(caloriesPerUnit);
                        movementActivity.setUnitType(nameOfUnits);
                        movementActivityRepository.addMovementActivity(movementActivity,getContext());
                        Toast.makeText(getContext(), nameOfTraining + " added to Movement Activity Database", Toast.LENGTH_SHORT).show();
                        autocompliteTraining();
                        dialog.dismiss();
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Couldn't add " + nameOfTraining + " to Food Database", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        Toast.makeText(getActivity(), "New training added", Toast.LENGTH_SHORT).show();
    }

    private void resetEdTextViews() {

        EdTxtNameOfTraining.setText("" );
        EdTxtCaloriesPerUnit.setText("" );
        EdTxtUnitType.setText("" );
        EdTxtNumberOfReps.setText("" );
        EdTxtUnitType.setText("" );

        EdTxtDateOfTraining.setHint("Date in format: dd/MM/yyy" );
        EdTxtNameOfTraining.setHint("Name of training" );
        EdTxtCaloriesPerUnit.setHint("Calories per hour" );
        EdTxtUnitType.setHint("Calories per rep" );
        EdTxtNumberOfReps.setHint("Number of reps" );
        EdTxtUnitType.setHint("Name of units" );
    }
}
