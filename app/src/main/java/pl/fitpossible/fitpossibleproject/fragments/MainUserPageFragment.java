package pl.fitpossible.fitpossibleproject.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

import pl.fitpossible.fitpossibleproject.ActivityCaptureRep;
import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.LoggingActivity;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.entity.AvatarImage;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;
import pl.fitpossible.fitpossibleproject.repository.AvatarImageRepository;
import pl.fitpossible.fitpossibleproject.repository.WeightRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainUserPageFragment extends Fragment implements View.OnClickListener {
    Button BnBalance;
    Button BnFoodDiary;
    Button BnTrainings;
    Button BnAddMealNow;
    Button BnLoggOut;
    Button BnGoToUpdateAppUser;
    Button BnGoToStepsFragment;
    Button BnGoToCaptureReps;

    TextView TxtShowLogin;
    ImageView ImgAvatar;
    AppUserRepository appUserRepository = new AppUserRepository();
    AvatarImageRepository avatarImageRepository = new AvatarImageRepository();
    WeightRepository weightRepository = new WeightRepository();
    private static final int PICK_IMAGE = 100;
    Uri imageUri;

    public MainUserPageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_user_page, container, false);
        AppUser currentAppUser;

        ImgAvatar = view.findViewById(R.id.avatar);


//        Bundle bundle = getActivity().getIntent().getExtras();//getArguments();
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("ID", Context.MODE_PRIVATE);

        Long currentAppUsersId = sharedPreferences.getLong("LoggedAppUserID", -1);//bundle.getLong("CurrentAppUserId");

        currentAppUser = appUserRepository.getAppUserById(currentAppUsersId, getContext());//MainActivity.appUserDatabase.appUserDAO().getAppUserById(currentAppUsersId);


        setApplicationGlobals(currentAppUser, currentAppUsersId);

        setApplicationGlobalsAvatar(currentAppUsersId);

        Double appUserWeight = new Double(0);
        try {
            appUserWeight = (weightRepository.getLastAppUsersWeight(ApplicationGlobals.loggedAppUserId, getContext()).getWeight());
        } catch (NullPointerException e) {
            Toast.makeText(getActivity(), "There is no weight set!", Toast.LENGTH_SHORT).show();
        }
        TxtShowLogin = view.findViewById(R.id.txt_show_login);
        TxtShowLogin.setText(ApplicationGlobals.loggedAppUserName);

        BnBalance = view.findViewById(R.id.bn_balance);
        BnBalance.setOnClickListener(this);

        BnFoodDiary = view.findViewById(R.id.bn_food_diary);
        BnFoodDiary.setOnClickListener(this);

        BnTrainings = view.findViewById(R.id.bn_trainings);
        BnTrainings.setOnClickListener(this);

        BnAddMealNow = view.findViewById(R.id.bn_go_to_fragment_food_product);
        BnAddMealNow.setOnClickListener(this);

        BnLoggOut = view.findViewById(R.id.bn_log_out);
        BnLoggOut.setOnClickListener(this);

        BnGoToUpdateAppUser = view.findViewById(R.id.bn_go_to_update_appUser);
        BnGoToUpdateAppUser.setOnClickListener(this);

        BnGoToStepsFragment = view.findViewById(R.id.bn_go_to_steps_fragment);
        BnGoToStepsFragment.setOnClickListener(this);

        BnGoToCaptureReps = view.findViewById(R.id.bn_rep_capture);
        BnGoToCaptureReps.setOnClickListener(this);

        ImgAvatar.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bn_balance:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new UsersChartsFragment()).
                        addToBackStack(null).commit();
                break;

            case R.id.bn_food_diary:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new FoodDiaryFragment()).
                        addToBackStack(null).commit();
                break;

            case R.id.bn_trainings:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new TrainingDiaryFragment()).
                        addToBackStack(null).commit();
                break;

            case R.id.bn_go_to_fragment_food_product:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new MainFoodProductFragment()).
                        addToBackStack(null).commit();
                break;

            case R.id.bn_go_to_update_appUser:
                MainActivity.fragmentManager.beginTransaction().

                        replace(R.id.fragment_container, new UpdateAppUserFragment()).

                        addToBackStack(null).

                        commit();
                break;

            case R.id.bn_go_to_steps_fragment:
                MainActivity.fragmentManager.beginTransaction().

                        replace(R.id.fragment_container, new StepsFragment()).

                        addToBackStack(null).

                        commit();
                break;

            case R.id.bn_log_out:
                Intent intent = new Intent();
                startActivity(new Intent(getActivity(), LoggingActivity.class));
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("ID", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("LoggedAppUserID", -1);
                editor.commit();
                getActivity().finish();
                break;

            case R.id.bn_rep_capture:
                startActivity(new Intent(getActivity(), ActivityCaptureRep.class));
                break;

        }
    }

    private void setApplicationGlobalsAvatar(Long currentAppUsersId) {
        if (ApplicationGlobals.loggedAppUserAvatar != null) {
            ImgAvatar.setImageBitmap(ApplicationGlobals.loggedAppUserAvatar);
        } else {
            try {
                AvatarImage avatar = avatarImageRepository.getOneAvatarImageById(currentAppUsersId, getContext());//MainActivity.appUserDatabase.avatarImageDAO().getOneAvatarImageById(currentAppUsersId);
                if (avatar != null) {
                    String encodedImageOfAvatar = avatar.getStringWithImage();
                    //byte[] encodedImageOfAvatarByteArray = encodedImageOfAvatar.getBytes();
                    byte data[] = Base64.decode(encodedImageOfAvatar, Base64.DEFAULT);
                    BitmapFactory.Options bitmapFactoryOptions = new BitmapFactory.Options();
                    bitmapFactoryOptions.inSampleSize = 4;
                    Bitmap avatarBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    ApplicationGlobals.loggedAppUserAvatar = avatarBitmap;
                    ImgAvatar.setImageBitmap(ApplicationGlobals.loggedAppUserAvatar);


                }

            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
    }

    private void setApplicationGlobals(AppUser currentAppUser, Long currentAppUsersId) {

        try {
            if (ApplicationGlobals.appUser == null && currentAppUser.getLogin().matches("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$")) {
                ApplicationGlobals.loggedAppUserName = new String(Base64.decode(currentAppUser.getLogin(), Base64.NO_WRAP), "UTF-8");
            } else {
                throw new IllegalArgumentException();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            ApplicationGlobals.loggedAppUserName = currentAppUser.getLogin();
        }
        if (ApplicationGlobals.loggedAppUserId != currentAppUser.getId()) {
            ApplicationGlobals.loggedAppUserAvatar = null;
        }

        ApplicationGlobals.loggedAppUserId = currentAppUser.getId();
        ApplicationGlobals.appUser = currentAppUser;
        if (ApplicationGlobals.loggedAppUserWeight == null) {
            try {
                if (weightRepository.getLastAppUsersWeight(currentAppUsersId, getContext()).getWeight() != null) {
                    ApplicationGlobals.loggedAppUserWeight = weightRepository.getLastAppUsersWeight(currentAppUsersId, getContext()).getWeight();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }


}
