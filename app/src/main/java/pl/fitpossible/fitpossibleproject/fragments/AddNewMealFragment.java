package pl.fitpossible.fitpossibleproject.fragments;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.threeten.bp.format.DateTimeFormatter;

import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.ViewModel.AddNewMealFragmentViewModel;
import pl.fitpossible.fitpossibleproject.entity.Food;
import pl.fitpossible.fitpossibleproject.entity.NutritionHistory;
import pl.fitpossible.fitpossibleproject.repository.FoodRepository;
import pl.fitpossible.fitpossibleproject.repository.NutritionHistoryRepository;

import static android.R.layout.simple_list_item_1;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.getInstance;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNewMealFragment extends Fragment implements View.OnClickListener {

    EditText EdTxtDateOfMeal;
    EditText EdTxtTimeOfMeal;
    AutoCompleteTextView EdTxtNameOfMeal;
    EditText EdTxtCaloriesPerUnit;
    EditText EdTxtNumberOfUnits;
    AutoCompleteTextView AutoCompleteTxtNameUnits;
    Button BnSubmitNewMeal;

    NutritionHistoryRepository nutritionHistoryRepository = new NutritionHistoryRepository();
    FoodRepository foodRepository = new FoodRepository();
    Bundle bundle;
    public Time timeOfMeal;
    Long mealDate;
    Date dateTruncateItd;
    String dateString;
    AndroidViewModel mViewModel;


    public AddNewMealFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_new_meal, container, false);
        mViewModel = ViewModelProviders.of(this).get(AddNewMealFragmentViewModel.class);


        EdTxtDateOfMeal = view.findViewById(R.id.EdTxt_date_of_meal);
        EdTxtTimeOfMeal = view.findViewById(R.id.EdTxt_time_of_meal);
        EdTxtNameOfMeal = view.findViewById(R.id.EdTxt_name_of_meal);
        EdTxtCaloriesPerUnit = view.findViewById(R.id.EdTxt_calories_per_unit);
        EdTxtNumberOfUnits = view.findViewById(R.id.EdTxt_number_of_units);
        AutoCompleteTxtNameUnits = view.findViewById(R.id.EdTxt_name_of_units);
        BnSubmitNewMeal = view.findViewById(R.id.bn_submit_new_meal);
        BnSubmitNewMeal.setOnClickListener(this);

        getDateFromBundle();
        EdTxtDateOfMeal.setText(getDateFromBundle());
        EdTxtTimeOfMeal.setText(getCurrentTime());

        String[] unitArray = getResources().getStringArray(R.array.units);
        ArrayAdapter<String> arrayAdapterForUnit = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, unitArray);
        AutoCompleteTxtNameUnits.setAdapter(arrayAdapterForUnit);
        AutoCompleteTxtNameUnits.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String unit = AutoCompleteTxtNameUnits.getText().toString();
            }
        });

        autocompleteMeal();

        return view;
    }


    private String getCurrentTime() {
        timeOfMeal = new Time(0);
        Calendar c = getInstance();
        timeOfMeal.setHours(c.getTime().getHours());
        timeOfMeal.setMinutes(c.getTime().getMinutes());
        Long timeOfMealLong = timeOfMeal.getTime();
        String timeString = DateFormat.format("HH:mm", new Time(timeOfMealLong)).toString();
        return timeString;
    }

    private String getDateFromBundle() {
        if ((getArguments() != null) && (getArguments().containsKey("MealDate"))) {
            bundle = getArguments();
            Long dateLong = bundle.getLong("MealDate");
            dateString = DateFormat.format("dd/MM/yyyy", new Date(dateLong)).toString();
            return dateString;
        }
//            Date dateFromCalendar = new Date(dateLong);
//            dateTruncateItd = DateUtils.truncate(dateFromCalendar, DAY_OF_MONTH);
//            mealDate = dateLong;
        return dateString;
    }

    private void autocompleteMeal() {
        ArrayAdapter<String> mealNameAdapter = getArrayAdapterOfFoodProducts();
        EdTxtNameOfMeal.setAdapter(mealNameAdapter);
        EdTxtNameOfMeal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    String nameOfMeal = EdTxtNameOfMeal.getText().toString();
                    Food food = foodRepository.showFoodProductByName(nameOfMeal, getContext());
                    EdTxtCaloriesPerUnit.setText(food.getCaloriesPerUnit().toString());
                    AutoCompleteTxtNameUnits.setText(food.getUnit().toString());
                } catch (NullPointerException e) {
                    Log.e("FitPossible", "exception", e);
                } catch (ArrayIndexOutOfBoundsException e) {
                    Log.e("FitPossible", "exception", e);
                } catch (Exception e) {
                    Log.e("FitPossible", "exception", e);
                    //Toast.makeText(getContext(), "Something went wrong during setting you're food product", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @NonNull
    ArrayAdapter<String> getArrayAdapterOfFoodProducts() throws NullPointerException {
        List<Food> foodList = foodRepository.showAllFoodProducts(getContext());
        TreeSet<String> foodNameSet = new TreeSet<String>();
        for (int i = 0; i < foodList.size(); i++) {
            foodNameSet.add(foodList.get(i).getName());
        }
        ArrayList<String> foodNameArrayList = new ArrayList<String>();

        for (String s : foodNameSet) {
            foodNameArrayList.add(s);
        }

        return new ArrayAdapter<String>(this.getContext(), simple_list_item_1, foodNameArrayList);
    }

    @Override
    public void onClick(View v) {
        Date dateOfMeal = getDateAndTimeOfMeal();
        if (dateOfMeal == null) return;
        if ((EdTxtNameOfMeal.getText().toString() != null && !EdTxtNameOfMeal.getText().toString().trim().isEmpty()) &&
                (EdTxtCaloriesPerUnit.getText().toString() != null && !EdTxtCaloriesPerUnit.getText().toString().trim().isEmpty()) &&
                (EdTxtNumberOfUnits.getText().toString() != null && !EdTxtNumberOfUnits.getText().toString().trim().isEmpty()) &&
                (AutoCompleteTxtNameUnits.getText().toString() != null) && !AutoCompleteTxtNameUnits.getText().toString().trim().isEmpty()
        ) {
            String nameOfMeal = EdTxtNameOfMeal.getText().toString();
            Integer caloriesPerUnit = Integer.parseInt(EdTxtCaloriesPerUnit.getText().toString());
            Float numberOfUnits = Float.parseFloat(EdTxtNumberOfUnits.getText().toString());
            String nameOfUnits = AutoCompleteTxtNameUnits.getText().toString();

            addToNutritionHistory(dateOfMeal, nameOfMeal, caloriesPerUnit, numberOfUnits, nameOfUnits);
        } else {
            Toast.makeText(getContext(), "Please fill each line with valid format", Toast.LENGTH_LONG).show();
            return;
        }
        resetEdTextViews();
        autocompleteMeal();
    }

    @Nullable
    private Date getDateAndTimeOfMeal() {
        String dateOfMealString = new String();
        String timeOfMealString = new String();
        try {
            dateOfMealString = EdTxtDateOfMeal.getText().toString();
            timeOfMealString = EdTxtTimeOfMeal.getText().toString();
        } catch (NumberFormatException e) {
            e.printStackTrace();
            Toast.makeText(this.getContext(), "Wrong date or time format", Toast.LENGTH_LONG).show();
        }
        Date dateOfMeal = new Date();
        // timeOfMeal = new Time(0);

        try {
            dateOfMeal = DateUtils.parseDate(dateOfMealString, "dd/MM/yyyy");
            Long timeLong = DateUtils.parseDate(timeOfMealString, "HH:mm").getTime();
            timeOfMeal = new Time(timeLong);
        } catch (ParseException e) {
            e.printStackTrace();
            Toast.makeText(this.getContext(), "Wrong date or time format", Toast.LENGTH_LONG).show();
            return null;
        }
        return dateOfMeal;
    }

    private void addToNutritionHistory(Date dateOfMeal, final String nameOfMeal,
                                       final Integer caloriesPerUnit, Float numberOfUnits, final String nameOfUnits) {
        try {
            NutritionHistory nutritionHistory = new NutritionHistory();
            nutritionHistory.setAppUserId(ApplicationGlobals.loggedAppUserId);
            nutritionHistory.setDate(dateOfMeal);
            nutritionHistory.setTime(timeOfMeal);
            nutritionHistory.setName(nameOfMeal);
            nutritionHistory.setCaloriesPerUnit(caloriesPerUnit);
            nutritionHistory.setNumberOfUnits(numberOfUnits);
            nutritionHistory.setUnit(nameOfUnits);

//            MainActivity.appUserDatabase.nutritionHistoryDAO().addNutritionHistory(nutritionHistory);
            nutritionHistoryRepository.addNutritionHistory(nutritionHistory, getContext());

            Toast.makeText(getActivity(), "New meal added", Toast.LENGTH_SHORT).show();

            if (foodRepository.showFoodProductByName(nameOfMeal, getContext()) == null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("FitPossibleProject");
                builder.setMessage("There is no " + nameOfMeal + " in your Food Database\nDo you want to add it?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Food food = new Food();
                            food.setName(nameOfMeal);
                            food.setCaloriesPerUnit(caloriesPerUnit);
                            food.setUnit(nameOfUnits);
                            foodRepository.addNewFoodProduct(food, getContext());
                            Toast.makeText(getContext(), nameOfMeal + " added to Food database", Toast.LENGTH_SHORT).show();
                            autocompleteMeal();
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Couldn't add " + nameOfMeal + " to Food Database", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }


        } catch (Exception e) {
            Log.e("FitPossible", "exception", e);
            Toast.makeText(getContext(), "Something went wrong during this action", Toast.LENGTH_LONG).show();
        }
    }

    private void resetEdTextViews() {
//        EdTxtDateOfMeal.setText("");
//        EdTxtTimeOfMeal.setText("");
        EdTxtNameOfMeal.setText("");
        EdTxtCaloriesPerUnit.setText("");
        EdTxtNumberOfUnits.setText("");
        AutoCompleteTxtNameUnits.setText("");

        EdTxtDateOfMeal.setHint("Date in format: dd/MM/yyy");
        EdTxtTimeOfMeal.setHint("Time in format HH:mm");
        EdTxtNameOfMeal.setHint("Name of meal");
        EdTxtCaloriesPerUnit.setHint("Calories per unit");
        EdTxtNumberOfUnits.setHint("Number of units");
        AutoCompleteTxtNameUnits.setHint("Name of units");
    }
}
