package pl.fitpossible.fitpossibleproject.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.Food;
import pl.fitpossible.fitpossibleproject.repository.FoodRepository;

import static android.R.layout.simple_list_item_1;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeleteFoodProductFragment extends Fragment implements View.OnClickListener {
    private AutoCompleteTextView TxtDeleteFoodById;
    private Button BnDeleteFood;
    private FoodRepository foodRepository = new FoodRepository();

    public DeleteFoodProductFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_delete_food_product, container, false);
        TxtDeleteFoodById = view.findViewById(R.id.txt_delete_food_product_by_id);
        BnDeleteFood = view.findViewById(R.id.bn_delete_food_product);
        BnDeleteFood.setOnClickListener(this);

        ArrayAdapter<String> foodNameArrayAdapter = getArrayAdapterOfFoodProducts();
        TxtDeleteFoodById.setAdapter(foodNameArrayAdapter);
        TxtDeleteFoodById.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    String nameOfMeal = TxtDeleteFoodById.getText().toString();
                    Food food = foodRepository.showFoodProductByName(nameOfMeal, getContext());//MainActivity.foodDatabase.foodDAO().getOneFood(nameOfMeal);
                    String name = food.getName();
                } catch (Exception e) {
                    Log.d("error", e.getMessage());
                }

            }
        });


        return view;
    }

    @Override
    public void onClick(View view) {
        String name = TxtDeleteFoodById.getText().toString();
        Food food = foodRepository.showFoodProductByName(name, getContext());
        food.setName(name);
        foodRepository.deleteFoodProduct(food, getContext());//MainActivity.foodDatabase.foodDAO().deleteFood(food);

        Toast.makeText(getActivity(), "Food deleted", Toast.LENGTH_SHORT).show();
        TxtDeleteFoodById.setText("");
        ArrayAdapter<String> foodNameArrayAdapter = getArrayAdapterOfFoodProducts();
        TxtDeleteFoodById.setAdapter(foodNameArrayAdapter);
    }

    @NonNull
    ArrayAdapter<String> getArrayAdapterOfFoodProducts() throws NullPointerException {
        List<Food> foodList = foodRepository.showAllFoodProducts(getContext());//MainActivity.foodDatabase.foodDAO().getFood();
        TreeSet<String> foodNameSet = new TreeSet<String>();
        for (int i = 0; i < foodList.size(); i++) {
            foodNameSet.add(foodList.get(i).getName());
        }
        ArrayList<String> foodNameArrayList = new ArrayList<String>();

        for (String s : foodNameSet) {
            foodNameArrayList.add(s);
        }

        return new ArrayAdapter<String>(this.getContext(), simple_list_item_1, foodNameArrayList);
    }
}
