package pl.fitpossible.fitpossibleproject.fragments;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.commons.lang3.time.DateUtils;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.entity.AvatarImage;
import pl.fitpossible.fitpossibleproject.entity.Gender;
import pl.fitpossible.fitpossibleproject.entity.LifestyleType;
import pl.fitpossible.fitpossibleproject.entity.Weight;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;
import pl.fitpossible.fitpossibleproject.repository.AvatarImageRepository;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateAppUserFragment extends Fragment implements View.OnClickListener {

    EditText TxtUpdateAppUserName;
    EditText TxtUpdateAppUserEmail;
    EditText TxtUpdateAppUserHeight;
    EditText TxtUpdateAppUserWeight;
    EditText TxtUpdateAppUserDateOfBirth;

    Spinner SpUpdateAppUserGender;
    Spinner SpUpdateAppUserLifestyle;

    ImageView ImgUpdateAppUserAvatar;
    ImageView ImgAvatar;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    AppUserRepository appUserRepository = new AppUserRepository();
    AvatarImageRepository avatarImageRepository = new AvatarImageRepository();

    Button BnUpdateAppUser;
    Button BnGoToUpdateAppUserPassword;
    Button BnGoToDeleteUser;

    public UpdateAppUserFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_update_app_user, container, false);


        TxtUpdateAppUserName = view.findViewById(R.id.txt_update_appUser_name);
        TxtUpdateAppUserEmail = view.findViewById(R.id.txt_update_appUser_email);
        TxtUpdateAppUserHeight = view.findViewById(R.id.txt_update_appUser_height);
        TxtUpdateAppUserWeight = view.findViewById(R.id.txt_update_appUser_weight);
        TxtUpdateAppUserDateOfBirth = view.findViewById(R.id.txt_update_appUser_dateOfBirth);
        SpUpdateAppUserGender = view.findViewById(R.id.txt_update_appUser_gender);
        SpUpdateAppUserLifestyle = view.findViewById(R.id.txt_update_appUser_lifestyle);
        ImgAvatar = view.findViewById(R.id.img_app_user_avatar);
        ImgAvatar.setOnClickListener(this);

        TxtUpdateAppUserName.setHint(ApplicationGlobals.loggedAppUserName);

        if (ApplicationGlobals.appUser.getEmail() != null) {
            TxtUpdateAppUserEmail.setHint(ApplicationGlobals.appUser.getEmail());
        }

        if (ApplicationGlobals.loggedAppUserWeight != null) {
            TxtUpdateAppUserWeight.setHint(ApplicationGlobals.loggedAppUserWeight.toString());
        }

        if (ApplicationGlobals.appUser.getHeight() != null) {
            TxtUpdateAppUserHeight.setHint(ApplicationGlobals.appUser.getHeight().toString());
        }

        if (ApplicationGlobals.appUser.getDateOfBirth() != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String dateOfBirth = dateFormat.format(ApplicationGlobals.appUser.getDateOfBirth());
            TxtUpdateAppUserDateOfBirth.setHint(dateOfBirth);
        }
        // TxtUpdateAppUserDateOfBirth.setHint(ApplicationGlobals.appUser.getDateOfBirth().toString());}


        List<Gender> genderTypeList = Arrays.asList(Gender.values());
        Spinner genderSpinner = (Spinner) view.findViewById(R.id.txt_update_appUser_gender);
        ArrayAdapter<Gender> genderTypeArrayAdapter = new ArrayAdapter<Gender>(this.getContext(), android.R.layout.simple_spinner_item, genderTypeList);
        genderTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(genderTypeArrayAdapter);
        if (ApplicationGlobals.appUser.getGender() != null) {
            genderSpinner.setSelection(genderTypeArrayAdapter.getPosition(ApplicationGlobals.appUser.getGender()));
        } else {
            genderSpinner.setSelection(genderTypeArrayAdapter.getPosition(Gender.FEMALE));
        }

        List<LifestyleType> lifestyleTypeList = Arrays.asList(LifestyleType.values());
        Spinner lifestyleSpinner = (Spinner) view.findViewById(R.id.txt_update_appUser_lifestyle);
        ArrayAdapter<LifestyleType> lifestyleTypeArrayAdapter = new ArrayAdapter<LifestyleType>(this.getContext(), android.R.layout.simple_spinner_item, lifestyleTypeList);
        lifestyleTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lifestyleSpinner.setAdapter(lifestyleTypeArrayAdapter);
        if (ApplicationGlobals.appUser.getLifestyle() != null) {
            lifestyleSpinner.setSelection(lifestyleTypeArrayAdapter.getPosition(ApplicationGlobals.appUser.getLifestyle()));
        } else {
            lifestyleSpinner.setSelection(lifestyleTypeArrayAdapter.getPosition(LifestyleType.INACTIVE));
        }


        if (ApplicationGlobals.loggedAppUserAvatar != null) {
            ImgAvatar.setImageBitmap(ApplicationGlobals.loggedAppUserAvatar);
        }

        BnGoToUpdateAppUserPassword = view.findViewById(R.id.bn_update_appUser_password);
        BnGoToUpdateAppUserPassword.setOnClickListener(this);

        BnUpdateAppUser = view.findViewById(R.id.bn_submit_appUser_update);
        BnUpdateAppUser.setOnClickListener(this);

        BnGoToDeleteUser = view.findViewById(R.id.bn_go_to_delete_appuser);
        BnGoToDeleteUser.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.bn_submit_appUser_update:
                AppUser appUser = ApplicationGlobals.appUser;
                String name = appUser.getLogin();
                String email = appUser.getEmail();
                Integer height = appUser.getHeight();
                Double weight = ApplicationGlobals.loggedAppUserWeight;
                Gender gender = appUser.getGender();
                Date dateOfBirth = appUser.getDateOfBirth();
                LifestyleType lifestyleType = appUser.getLifestyle();

                if (TxtUpdateAppUserName.getText() != null && !TxtUpdateAppUserName.getText().toString().isEmpty()) {
                    name = TxtUpdateAppUserName.getText().toString();
                }
                if (TxtUpdateAppUserEmail.getText() != null && !TxtUpdateAppUserEmail.getText().toString().isEmpty()) {
                    email = TxtUpdateAppUserEmail.getText().toString();
                }
                if (appUser.getHeight() != null) {
                    height = appUser.getHeight();
                }
                if (TxtUpdateAppUserHeight.getText() != null && !TxtUpdateAppUserHeight.getText().toString().isEmpty()) {
                    height = Integer.parseInt(TxtUpdateAppUserHeight.getText().toString());
                }

                if (TxtUpdateAppUserWeight.getText() != null && !TxtUpdateAppUserWeight.getText().toString().isEmpty()) {
                    weight = Double.parseDouble(TxtUpdateAppUserWeight.getText().toString());
                }

                if (TxtUpdateAppUserDateOfBirth.getText() != null && !TxtUpdateAppUserDateOfBirth.getText().toString().isEmpty()) {
                    String dateOfBirthString = TxtUpdateAppUserDateOfBirth.getText().toString();
                    try {
                        dateOfBirth = DateUtils.parseDate(dateOfBirthString, "DD-MM-YYYY");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }

                gender = (Gender) SpUpdateAppUserGender.getSelectedItem();
                lifestyleType = (LifestyleType) SpUpdateAppUserLifestyle.getSelectedItem();

                appUser.setId(ApplicationGlobals.loggedAppUserId);

                appUser.setLogin(name);
                appUser.setEmail(email);
                appUser.setHeight(height);
                appUser.setDateOfBirth(dateOfBirth);
                appUser.setGender(gender);
                appUser.setLifestyle(lifestyleType);
                Weight weight1 = new Weight();
                weight1.setWeight(weight);
                weight1.setAppUserId(appUser.getId());

                ApplicationGlobals.appUser = appUser;
                ApplicationGlobals.loggedAppUserName = appUser.getLogin();

                Date date = new Date();
                date = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
                weight1.setDate(date);
                ApplicationGlobals.loggedAppUserWeight = weight1.getWeight();


                appUserRepository.updateAppUser(appUser);// MainActivity.appUserDatabase.appUserDAO().updateAppUser(appUser);
                Toast.makeText(getActivity(), "AppUser Updated!", Toast.LENGTH_SHORT).show();

                break;

            case R.id.bn_update_appUser_password:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new ChangePasswordFragment())
                        .addToBackStack(null).commit();
                break;

            case R.id.bn_go_to_delete_appuser:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new DeleteUserFragment())
                        .addToBackStack(null).commit();
                break;

            case R.id.img_app_user_avatar:
                openGallery();
        }
    }

    private void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            // Bitmap bitmap = null;
            imageUri = data.getData();
            ImgAvatar.setImageURI(imageUri);
            InputStream inputStream = null;
            try {
                final BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inSampleSize = 4;
                inputStream = getActivity().getContentResolver().openInputStream(imageUri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, opts);
                // Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                ApplicationGlobals.loggedAppUserAvatar = bitmap;
                ImgAvatar.setImageBitmap(bitmap);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] imageToDatabase = byteArrayOutputStream.toByteArray();
                String imageToDatabaseEncoded = Base64.encodeToString(imageToDatabase, Base64.DEFAULT);

                AvatarImage avatarImage = new AvatarImage();
                avatarImage.setId(ApplicationGlobals.loggedAppUserId);
                avatarImage.setAppUserId(ApplicationGlobals.loggedAppUserId);
                avatarImage.setAvatarName("Avatar");
                avatarImage.setStringWithImage(imageToDatabaseEncoded);

                if (avatarImageRepository.getOneAvatarImageById(ApplicationGlobals.loggedAppUserId, getContext()) == null) {
                    avatarImageRepository.addAvatarImage(avatarImage, getContext());//MainActivity.appUserDatabase.avatarImageDAO().addAvatarImage(avatarImage);
                } else {
                    avatarImageRepository.updateAvatarImage(avatarImage, getContext());//MainActivity.appUserDatabase.avatarImageDAO().updateAvatarImage(avatarImage);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }

}
