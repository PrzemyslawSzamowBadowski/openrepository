package pl.fitpossible.fitpossibleproject.fragments;


import android.os.BaseBundle;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;

import static java.util.Calendar.getInstance;


/**
 * A simple {@link Fragment} subclass.
 */
public class FoodDiaryFragment extends Fragment implements View.OnClickListener {

    Button BnShowMealOnDate;
    Button BnAddMealOnDate;
    TextView TxtLoggedAs;
    TextView TxtShowLogin;
    TextView TxtShowCalendarDate;
    CalendarView calendarView;
    long dateFromCalendar;
    Bundle bundle = new Bundle();
    ImageView loggedUserAvatar;

    public FoodDiaryFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_food_diary, container, false);

        calendarView = view.findViewById(R.id.calendarView);
        TxtShowCalendarDate = view.findViewById(R.id.txt_show_calendar_date);


        Long dateLong = getInstance().getTimeInMillis();
        String dateString = DateFormat.format("dd/MM/yyyy", new java.sql.Date(dateLong)).toString();
        TxtShowCalendarDate.setText(dateString);
        bundle.putLong("MealDate", dateLong);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView thisCalendarView, int year, int month, int dayOfMonth) {

                setDateFromCalendar(year, month, dayOfMonth);
            }
        });

        BnAddMealOnDate = view.findViewById(R.id.bn_add_meal_on_date);

        BnAddMealOnDate.setOnClickListener(this);

        BnShowMealOnDate = view.findViewById(R.id.bn_show_meal_on_date);
        BnShowMealOnDate.setOnClickListener(this);

        TxtShowLogin = view.findViewById(R.id.txt_show_login);
        TxtShowLogin.setText(ApplicationGlobals.loggedAppUserName);

        loggedUserAvatar = view.findViewById(R.id.avatar);
        loggedUserAvatar.setImageBitmap(ApplicationGlobals.loggedAppUserAvatar);
        return view;
    }

    private void setDateFromCalendar(int year, int month, int dayOfMonth) {
        Calendar c = getInstance();
        c.set(year, month, dayOfMonth);
        String dateString = DateFormat.format("dd/MM/yyyy", c).toString();
        TxtShowCalendarDate.setText(dateString);
        dateFromCalendar = c.getTimeInMillis();
//                Long dateLong = dateFromCalendar;
//                Log.d("date in milis: ", dateLong.toString());
        bundle.putLong("MealDate", dateFromCalendar);

        Long l = bundle.getLong("MealDate");
//                String s = l.toString();
//                Log.d("in bundle", s);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.bn_add_meal_on_date:

                AddNewMealFragment addNewMealFragment = new AddNewMealFragment();
                addNewMealFragment.setArguments(bundle);
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, addNewMealFragment)
                        .addToBackStack(null).commit();
                break;

            case R.id.bn_show_meal_on_date:

                ShowMealFragment showMealFragment = new ShowMealFragment();
                showMealFragment.setArguments(bundle);
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, showMealFragment)
                        .addToBackStack(null).commit();
                break;


        }
    }
}
