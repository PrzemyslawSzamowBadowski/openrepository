package pl.fitpossible.fitpossibleproject.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.Food;
import pl.fitpossible.fitpossibleproject.repository.FoodRepository;

import static android.R.layout.simple_list_item_1;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateFoodProductFragment extends Fragment implements View.OnClickListener {


    AutoCompleteTextView TxtUpdateFoodName;
    EditText TxtUpdateFoodCaloriesPerUnit;
    AutoCompleteTextView AutoCompleteTxtUpdateUnits;
    Button BnSubmitFoodProduct;
    FoodRepository foodRepository = new FoodRepository();

    public UpdateFoodProductFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_food, container, false);

        TxtUpdateFoodName = view.findViewById(R.id.txt_update_food_product_name);
        TxtUpdateFoodCaloriesPerUnit = view.findViewById(R.id.txt_update_food_calories_per_unit);
        AutoCompleteTxtUpdateUnits = view.findViewById(R.id.txt_update_food_unit);

        BnSubmitFoodProduct = view.findViewById(R.id.bn_submit_food_update);
        BnSubmitFoodProduct.setOnClickListener(this);

        autocompleteMeal();
        autocompleteUnit();

        return view;
    }

    @Override
    public void onClick(View v) {

        String name = TxtUpdateFoodName.getText().toString();
        Integer caloriesPerUnit = Integer.parseInt(TxtUpdateFoodCaloriesPerUnit.getText().toString());
        String unit = AutoCompleteTxtUpdateUnits.getText().toString();

        updateFoodProduct(name, caloriesPerUnit, unit);

        //  resetEdTxt();


    }

    private void updateFoodProduct(String name, Integer caloriesPerUnit, String unit) {
        try {
            final Food food = foodRepository.showFoodProductByName(name,getContext());//MainActivity.foodDatabase.foodDAO().getOneFood(name);
            food.setName(name);
            food.setCaloriesPerUnit(caloriesPerUnit);
            food.setUnit(unit);

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("FitPossibleProject");
            builder.setMessage("Do you really want to update this product?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    try {
                       foodRepository.updateNewFoodProduct(food,getContext());// MainActivity.foodDatabase.foodDAO().updateFood(food);
                        Toast.makeText(getActivity(), "Food product updated", Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();

        } catch (NullPointerException e) {
            Toast.makeText(getContext(), "There is no food product named " + name, Toast.LENGTH_LONG).show();
        } catch (
                Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Something went wrong during updating.\nPlease try again", Toast.LENGTH_LONG).show();
        }

    }

    private void resetEdTxt() {
        TxtUpdateFoodName.setText("");
        TxtUpdateFoodCaloriesPerUnit.setText("");
        AutoCompleteTxtUpdateUnits.setText("");
    }

    private void autocompleteUnit() {
        String[] unitArray = getResources().getStringArray(R.array.units);
        ArrayAdapter<String> arrayAdapterForUnit = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, unitArray);
        AutoCompleteTxtUpdateUnits.setAdapter(arrayAdapterForUnit);
        AutoCompleteTxtUpdateUnits.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String unit = AutoCompleteTxtUpdateUnits.getText().toString();
            }
        });
    }

    private void autocompleteMeal() {
        ArrayAdapter<String> mealNameAdapter = getArrayAdapterOfFoodProducts();
        TxtUpdateFoodName.setAdapter(mealNameAdapter);
        TxtUpdateFoodName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    String nameOfMeal = TxtUpdateFoodName.getText().toString();
                    if (foodRepository.showFoodProductByName(nameOfMeal,getContext())!= null) {
                        Food food = foodRepository.showFoodProductByName(nameOfMeal,getContext());
                        TxtUpdateFoodCaloriesPerUnit.setText(food.getCaloriesPerUnit().toString());
                        AutoCompleteTxtUpdateUnits.setText(food.getUnit().toString());
                    }
                } catch (NullPointerException e) {
                    Log.e("FitPossible", "exception", e);
                } catch (ArrayIndexOutOfBoundsException e) {
                    Log.e("FitPossible", "exception", e);
                } catch (Exception e) {
                    Log.e("FitPossible", "exception", e);
                    //Toast.makeText(getContext(), "Something went wrong during setting you're food product", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @NonNull
    ArrayAdapter<String> getArrayAdapterOfFoodProducts() throws NullPointerException {
        List<Food> foodList = foodRepository.showAllFoodProducts(getContext());//MainActivity.foodDatabase.foodDAO().getFood();
        TreeSet<String> foodNameSet = new TreeSet<String>();
        for (int i = 0; i < foodList.size(); i++) {
            foodNameSet.add(foodList.get(i).getName());
        }
        ArrayList<String> foodNameArrayList = new ArrayList<String>();

        for (String s : foodNameSet) {
            foodNameArrayList.add(s);
        }

        return new ArrayAdapter<String>(this.getContext(), simple_list_item_1, foodNameArrayList);
    }
}
