package pl.fitpossible.fitpossibleproject.fragments;


import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.LoggingActivity;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.databases.AppUserDatabase;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoggingFragment extends Fragment implements View.OnClickListener {

    Bundle bundle;

    private Button BnSubmit;
    private Button BnAddUser;
    private Button BnShowUsers;

    EditText EdTxtUserName;
    EditText EdTxtPassword;
    Long currentAppUserId;

    AppUserRepository appUserRepository = new AppUserRepository();
    LoggingFragmentViewModel loggingFragmentViewModel;

    public LoggingFragment() {
        // Required empty public constructor

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    public void resetGlobals() {

        ApplicationGlobals.loggedAppUserWeight = null;
        ApplicationGlobals.loggedAppUserAvatar = null;
        ApplicationGlobals.loggedAppUserId = null;
        ApplicationGlobals.loggedAppUserName = null;
    }

    @Override
    //  @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        resetGlobals();
        View view = inflater.inflate(R.layout.fragment_logging, container, false);
        EdTxtUserName = view.findViewById(R.id.edtxt_enter_login);
        EdTxtPassword = view.findViewById(R.id.edtxt_password);
        updateTextView(view);

        BnSubmit = view.findViewById(R.id.bn_submit);
        BnSubmit.setOnClickListener(this);

        BnAddUser = view.findViewById(R.id.bn_add_appUser);
        BnAddUser.setOnClickListener(this);

        BnShowUsers = view.findViewById(R.id.bn_view_appUsers);
        BnShowUsers.setOnClickListener(this);


        return view;
    }


    // @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    public void updateTextView(View view) {

        if ((getArguments() != null) && (getArguments().containsKey("CurrentAppUserName") == true)) {
            bundle = getArguments();
            String currentAppUsersLogin = bundle.getString("CurrentAppUserName");
            EdTxtUserName.setText(currentAppUsersLogin);
        }

        if ((getArguments() != null) && (getArguments().containsKey("CurrentAppUserPassword") == true)) {
            bundle = getArguments();
            String currentAppUsersPassword = bundle.getString("CurrentAppUserPassword");
            EdTxtPassword.setText(currentAppUsersPassword);
        }
    }

//    @Override
//    public void onBackPressed() {
//        // Do Here what ever you want do on back press;
//    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bn_submit:
                try {
                    AppUser appUser;
                    TempAppUser tempAppUserFromEdTxt = new TempAppUser().invoke();
                    String password = tempAppUserFromEdTxt.getPassword();
                    String login = tempAppUserFromEdTxt.getLogin();
                    String encodedLogin = tempAppUserFromEdTxt.getEncodedLogin();


                    try {
                        appUser = appUserRepository.getAppUserByLogin(tempAppUserFromEdTxt.encodedLogin, getContext());//loggingFragmentViewModel.getAppUserByLogin(tempAppUserFromEdTxt.encodedLogin);//appUserRepository.getAppUserByLogin(tempAppUserFromEdTxt.encodedLogin);//AppUserDatabase.getInstance(this.getContext()).appUserDAO().getAppUserByLogin(tempAppUserFromEdTxt.encodedLogin);

                        Log.d("appUser: ", appUser.toString());
                        getDecodedPasswordFromDatabase(appUser);
                    } catch (NullPointerException e) {
                        appUser = appUserRepository.getAppUserByLogin(tempAppUserFromEdTxt.login, getContext());//loggingFragmentViewModel.getAppUserByLogin(tempAppUserFromEdTxt.login);//AppUserDatabase.getInstance(this.getContext()).appUserDAO().getAppUserByLogin(tempAppUserFromEdTxt.login);
                        //    String passwordFromDatabaseString = appUser.getPassword();

                    }
//                    String loginFromDatabase = Base64.decode(appUser.getLogin(),Base64.NO_WRAP).toString();
//                    Log.d("login from database: ", loginFromDatabase);
                    if (appUser.getPassword().equals(tempAppUserFromEdTxt.getPassword()) || appUser.getPassword().equals(Base64.encodeToString(tempAppUserFromEdTxt.getPassword().getBytes(), Base64.NO_WRAP))) {
                        goToMainUserPageFragment(appUser);
                    } else {
                        Toast.makeText(getActivity(), "These credentials don't match any user!", Toast.LENGTH_LONG).show();
                    }
                    break;
                } catch (NullPointerException e) {
                    Toast.makeText(getActivity(), "These credentials don't match any user!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    Log.d("Exception: ", e.toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(getActivity(), "You probably typed in some unexpected signs!", Toast.LENGTH_LONG).show();
                    Log.d("Exception: ", e.toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.bn_add_appUser:
                LoggingActivity.fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, new AddUserFragment())
                        .addToBackStack(null)
                        .commit();
                break;

            case R.id.bn_view_appUsers:
                LoggingActivity.fragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, new ReadAppUsersFragment())
                        .addToBackStack(null)
                        .commit();
                break;
        }
    }

    private void getDecodedPasswordFromDatabase(AppUser appUser) throws UnsupportedEncodingException {
        byte[] passwordFromDatabase = (Base64.decode(appUser.getPassword(), Base64.NO_WRAP));
        String passwordFromDatabaseString = new String(passwordFromDatabase, "UTF-8");
        Log.d("password from database", passwordFromDatabaseString);
    }

    private void goToMainUserPageFragment(AppUser appUser) {
        currentAppUserId = appUser.getId();
//        Bundle bundle = new Bundle();
//        bundle.putLong("CurrentAppUserId", currentAppUserId);
        MainUserPageFragment mainUserPageFragment = new MainUserPageFragment();
        mainUserPageFragment.setArguments(bundle);
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtra("CurrentAppUserId", currentAppUserId);
        startActivity(intent);
        putIdToSharedPreferences(currentAppUserId);
        getActivity().finish();
//        MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, mainUserPageFragment).
//                addToBackStack(null).commit();
    }


    private class TempAppUser {
        private String password;
        private String login;
        private String encodedLogin;

        public String getPassword() {
            return password;
        }

        public String getLogin() {
            return login;
        }

        public String getEncodedLogin() {
            return encodedLogin;
        }

        public TempAppUser invoke() throws UnsupportedEncodingException {
            String passwordFromDatabaseString = "";
            AppUser appUser = null;
            password = EdTxtPassword.getText().toString();
            Log.d("Zwykły password z edittext: ", password);
            String encodedPassword = Base64.encodeToString(password.getBytes("UTF-8"), Base64.NO_WRAP);
            Log.d("Encoded password: ", encodedPassword);
            login = EdTxtUserName.getText().toString();
            encodedLogin = Base64.encodeToString(login.getBytes("UTF-8"), Base64.NO_WRAP);
            return this;
        }
    }

    private void putIdToSharedPreferences(long id) {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("ID", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("LoggedAppUserID", id);
        editor.commit();
    }


}
