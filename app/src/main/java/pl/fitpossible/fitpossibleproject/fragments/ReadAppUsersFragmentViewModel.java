package pl.fitpossible.fitpossibleproject.fragments;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import java.util.List;

import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.repository.AppUserRepository;

public class ReadAppUsersFragmentViewModel extends AndroidViewModel {
    private AppUserRepository appUserRepository;
    private List<AppUser> allAppUsers;

    public ReadAppUsersFragmentViewModel(@NonNull Application application) {
        super(application);
        appUserRepository = new AppUserRepository();
        allAppUsers = appUserRepository.getAppUsers(application.getApplicationContext());
    }

    public List<AppUser> getAllAppUsers() {
        return allAppUsers;
    }
}
