package pl.fitpossible.fitpossibleproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;

import static java.util.Calendar.getInstance;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrainingDiaryFragment extends Fragment implements View.OnClickListener  {

    Button BnShowTrainingOnDate;
    Button BnAddTrainingOnDate;
    TextView TxtLoggedAs;
    TextView TxtShowLogin;
    TextView TxtShowCalendarDate;
    ImageView ImgAvatar;
    CalendarView calendarView;
    long dateFromCalendar;
    Bundle bundle = new Bundle();


    public TrainingDiaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_training_diary, container, false);

        ImgAvatar = view.findViewById(R.id.avatar);
        if (ApplicationGlobals.loggedAppUserAvatar != null) {
            ImgAvatar.setImageBitmap(ApplicationGlobals.loggedAppUserAvatar);
        }


        calendarView = view.findViewById(R.id.calendarView);
        TxtShowCalendarDate = view.findViewById(R.id.txt_show_calendar_date);


        Long dateLong = getInstance().getTimeInMillis();
        String dateString = DateFormat.format("dd/MM/yyyy", new java.sql.Date(dateLong)).toString();
        TxtShowCalendarDate.setText(dateString);
        bundle.putLong("TrainingDate", dateLong);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView thisCalendarView, int year, int month, int dayOfMonth) {

//
                Calendar c = getInstance();
                c.set(year, month, dayOfMonth);
                String dateString = DateFormat.format("dd/MM/yyyy", c).toString();
                TxtShowCalendarDate.setText(dateString);
                dateFromCalendar = c.getTimeInMillis();
//                Long dateLong = dateFromCalendar;
//                Log.d("date in milis: ", dateLong.toString());
                bundle.putLong("TrainingDate", dateFromCalendar);

                Long l = bundle.getLong("TrainingDate");
//                String s = l.toString();
//                Log.d("in bundle", s);
            }
        });

        BnAddTrainingOnDate = view.findViewById(R.id.bn_add_training_on_date);

        BnAddTrainingOnDate.setOnClickListener(this);

        BnShowTrainingOnDate = view.findViewById(R.id.bn_show_training_on_date);
        BnShowTrainingOnDate.setOnClickListener(this);

        TxtShowLogin = view.findViewById(R.id.txt_show_login);
        TxtShowLogin.setText(ApplicationGlobals.loggedAppUserName);

        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.bn_add_training_on_date:

                AddNewTrainingFragment addNewTrainingFragment = new AddNewTrainingFragment();
                addNewTrainingFragment.setArguments(bundle);
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, addNewTrainingFragment)
                        .addToBackStack(null).commit();
                break;

            case R.id.bn_show_training_on_date:

                ShowTrainingFragment showTrainingFragment = new ShowTrainingFragment();
                showTrainingFragment.setArguments(bundle);
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, showTrainingFragment)
                        .addToBackStack(null).commit();
                break;


        }
    }

}
