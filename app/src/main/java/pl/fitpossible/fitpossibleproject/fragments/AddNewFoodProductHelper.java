package pl.fitpossible.fitpossibleproject.fragments;

import android.database.sqlite.SQLiteConstraintException;
import android.util.Log;
import android.widget.Toast;

import pl.fitpossible.fitpossibleproject.entity.Food;
import pl.fitpossible.fitpossibleproject.repository.FoodRepository;

public class AddNewFoodProductHelper {
    private final AddNewFoodProductFragment addNewFoodProductFragment;

    public AddNewFoodProductHelper(AddNewFoodProductFragment addNewFoodProductFragment) {
        this.addNewFoodProductFragment = addNewFoodProductFragment;
    }

    void addNewFoodProduct(String name, Integer caloriesPerUnit, String unit) {
        if (name.trim().equals("") || name.isEmpty()) {
            name = null;
        }
        if (unit.trim().equals("") || unit.isEmpty()) {
            unit = null;
        }
        try {
            Food food = new Food();
            food.setName(name);
            food.setCaloriesPerUnit(caloriesPerUnit);
            food.setUnit(unit);
            FoodRepository foodRepository = new FoodRepository();
            int isOK = foodRepository.addNewFoodProduct(food, addNewFoodProductFragment.getContext());

            if (isOK == 1) {
                Toast.makeText(addNewFoodProductFragment.getActivity(), "New food product added", Toast.LENGTH_SHORT).show();

                addNewFoodProductFragment.getTxtAddNewFoodProductName().setText("");
                addNewFoodProductFragment.getTxtAddNewFoodProductCaloriesPerUnit().setText("");
                addNewFoodProductFragment.getAutoCompleteTxtAddNewFoodProductUnit().setText("");
            } else if (isOK == -1) {
                Toast.makeText(addNewFoodProductFragment.getContext(), "There is already food named " + name + " on a list", Toast.LENGTH_LONG).show();
            } else if (isOK == 0) {
                Toast.makeText(addNewFoodProductFragment.getContext(), "Something went wrong, please try again", Toast.LENGTH_LONG).show();
            }
        } catch (SQLiteConstraintException e) {
            Log.e("FitPossible", "exception constraint and so on", e);
            Toast.makeText(addNewFoodProductFragment.getContext(), "There is already food named " + name + " on a list", Toast.LENGTH_LONG).show();
        } catch (NullPointerException e) {
            e.printStackTrace();
            Toast.makeText(addNewFoodProductFragment.getContext(), "Make sure that you filled every line", Toast.LENGTH_LONG).show();
        }
    }
}