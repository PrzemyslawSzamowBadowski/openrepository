package pl.fitpossible.fitpossibleproject.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.Formulas;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.databases.AppUserDatabase;
import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;
import pl.fitpossible.fitpossibleproject.entity.NutritionHistory;
import pl.fitpossible.fitpossibleproject.repository.ActivityHistoryRepository;
import pl.fitpossible.fitpossibleproject.repository.NutritionHistoryRepository;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsersChartsFragment extends Fragment {

    TextView currentAppUserLogin;
    ImageView ImgAvatar;
    Spinner spinnerOfDays;

    UserChartsFragmentViewModel viewModel;
    String login = ApplicationGlobals.loggedAppUserName;
    Long id = ApplicationGlobals.loggedAppUserId;
    List<NutritionHistory> nutritionHistoryList;
    List<ActivityHistory> activityHistoryList;
    int baseCaloricExpenditure = getBaseCaloricExpenditure();
    NutritionHistoryRepository nutritionHistoryRepository = new NutritionHistoryRepository();
    ActivityHistoryRepository activityHistoryRepository = new ActivityHistoryRepository();

    TreeMap<Date, Float> mapOfCaloriesInMealByDate;
    TreeMap<Date, Float> mapOfCaloricExpenditure = new TreeMap<>();


    PointsGraphSeries<DataPoint> seriesCaloriesADay = new PointsGraphSeries<>();
    PointsGraphSeries<DataPoint> seriesNutrition = new PointsGraphSeries<>();
    PointsGraphSeries<DataPoint> seriesActivity = new PointsGraphSeries<>();
    PointsGraphSeries<DataPoint> seriesCaloriesADayPlusActivity = new PointsGraphSeries<>();


    public UsersChartsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_users_charts, container, false);

//        viewModel = ViewModelProviders.of(this).get(UserChartsFragmentViewModel.class);
//        viewModel.getAllActivityHistoryById(id).observe(this, new Observer<List<ActivityHistory>>() {
//            @Override
//            public void onChanged(@Nullable List<ActivityHistory> activityHistories) {
//              //activityHistoryList = activityHistories;
//            }
//        });

        currentAppUserLogin = view.findViewById(R.id.txt_current_user_login);
        currentAppUserLogin.setText(login);
        GraphView graph = view.findViewById(R.id.graph);

        ImgAvatar = view.findViewById(R.id.avatar);
        if (ApplicationGlobals.loggedAppUserAvatar != null) {
            ImgAvatar.setImageBitmap(ApplicationGlobals.loggedAppUserAvatar);
        }

        spinnerOfDays = view.findViewById(R.id.spinner_days);
        setAdapterForSpinnerOfDays();

        final TextView txtBalanceOfCalories = view.findViewById(R.id.txt_balance_of_calories);
        try {
            nutritionHistoryList = nutritionHistoryRepository.getNutritionHistoryById(id, getContext());//MainActivity.appUserDatabase.nutritionHistoryDAO().getNutritionHistoryById(id);
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "You have no meals saved", Toast.LENGTH_LONG).show();
        }

        activityHistoryList = activityHistoryRepository.getActivityHistoryById(id, getContext());//.AppUserDatabase.getInstance(getContext()).activityHistoryDAO().getActivityHistoryById(id);
        Collections.sort(nutritionHistoryList);
        Collections.sort(activityHistoryList);

        Set datesSetForNutrition = getSetOfDatesFromNutrition();
        mapOfCaloriesInMealByDate = (TreeMap<Date, Float>) getMapOfDatesNutrition(datesSetForNutrition);
        setDataForSeriesCaloriesADayNutrition(mapOfCaloriesInMealByDate);

        Set datesSetForActivity = getDatesFromActivityHistory();
        Map<Date, Float> mapOfCaloriesInActivityByDate = getMapOfCaloriesInActivityByDate(datesSetForActivity);

        setDataForSeriesActivity(mapOfCaloriesInActivityByDate);
        setDataForSeriesCaloriesADayPlusActivity(baseCaloricExpenditure, mapOfCaloriesInActivityByDate);
        setDataForSeriesCaloriesADay(baseCaloricExpenditure);

        graph.addSeries(setAppearanceForGraph(seriesCaloriesADay, Color.GREEN, "Base caloric expenditure", 5));
        graph.addSeries(setAppearanceForGraph(seriesActivity, Color.BLUE, "Caloric expenditure from excercises", 5));
        graph.addSeries(setAppearanceForGraph(seriesNutrition, Color.RED, "Caloric intake", 5));
        graph.addSeries(setAppearanceForGraph(seriesCaloriesADayPlusActivity, Color.MAGENTA, "Basic caloric and excercises caloric expenditure combined", 5));
        setGraphViewAppearance(graph);


        final ArrayList<Date> arrayListOfDatesFromMapOfCaloricExpenditure = new ArrayList<>();
        final ArrayList<Date> arrayListOfMapOfCaloriesInMealByDate = new ArrayList<>();
        for (Date d : mapOfCaloricExpenditure.keySet()
        ) {
            arrayListOfDatesFromMapOfCaloricExpenditure.add(d);
        }
        for (Date d : mapOfCaloriesInMealByDate.keySet()
        ) {
            arrayListOfMapOfCaloriesInMealByDate.add(d);
        }

        Collections.sort(arrayListOfDatesFromMapOfCaloricExpenditure);
        Collections.sort(arrayListOfMapOfCaloriesInMealByDate);

        spinnerOfDays.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                float sum = getSumOfCaloricIntakeAndExpenditure(arrayListOfDatesFromMapOfCaloricExpenditure, arrayListOfMapOfCaloriesInMealByDate);
                txtBalanceOfCalories.setText(((Float) sum).toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return view;
    }

    private float getSumOfCaloricIntakeAndExpenditure(ArrayList<Date> arrayListOfDatesFromMapOfCaloricExpenditure, ArrayList<Date> arrayListOfMapOfCaloriesInMealByDate) {
        float expanditure = 0;
        float intake = 0;
        float sum = 0;
        Integer b = Integer.parseInt((String) spinnerOfDays.getSelectedItem());
        for (int i = arrayListOfDatesFromMapOfCaloricExpenditure.size() - 1; i >= arrayListOfDatesFromMapOfCaloricExpenditure.size() - b && i > -1; i--) {
            if (mapOfCaloricExpenditure.get(arrayListOfDatesFromMapOfCaloricExpenditure.get(i)) != null) {
                expanditure = expanditure + mapOfCaloricExpenditure.get(arrayListOfDatesFromMapOfCaloricExpenditure.get(i));
            }
        }
        for (int i = arrayListOfMapOfCaloriesInMealByDate.size() - 1; i >= arrayListOfMapOfCaloriesInMealByDate.size() - b && i > -1; i--) {
            if (mapOfCaloriesInMealByDate.get(arrayListOfMapOfCaloriesInMealByDate.get(i)) != null) {
                intake = intake + mapOfCaloriesInMealByDate.get(arrayListOfMapOfCaloriesInMealByDate.get(i));
            }
        }


        sum = intake - expanditure;
        return sum;
    }

    private void setAdapterForSpinnerOfDays() {
        ArrayList<String> list = new ArrayList<>(Arrays.asList("1", "3", "5", "7", "10", "14", "21", "28"));
        ArrayAdapter<String> arrayAdapterForSpinnerOfDays = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        arrayAdapterForSpinnerOfDays.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinnerOfDays.setAdapter(arrayAdapterForSpinnerOfDays);
    }

    private void setDataForSeriesCaloriesADayNutrition(Map<Date, Float> mapOfCaloriesInMealByDate) {
        Date xNutrition;
        float yNutrition;
        for (Map.Entry<Date, Float> set : mapOfCaloriesInMealByDate.entrySet()) {
            xNutrition = set.getKey();
            yNutrition = set.getValue();
            try {
                seriesNutrition.appendData(new DataPoint(xNutrition, yNutrition), true, 9999);
                //seriesCaloiresADayNutrition.appendData(new DataPoint(xNutrition, y1Nutrition), true, 9999);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @NonNull
    private Map<Date, Float> getMapOfDatesNutrition(Set datesSetForNutrition) {
        Map<Date, Float> mapOfCaloriesInMealByDate = new TreeMap<>();
        Date[] dates = new Date[datesSetForNutrition.size()];
        dates = (Date[]) datesSetForNutrition.toArray(dates);
        datesSetForNutrition.toArray();
        for (int i = 0; i < dates.length; i++) {
            ArrayList<NutritionHistory> mealsInThisDay = (ArrayList<NutritionHistory>) nutritionHistoryRepository.getNutritionHistoryByIdAndDate(ApplicationGlobals.loggedAppUserId, dates[i], getContext());//MainActivity.appUserDatabase.nutritionHistoryDAO().getNutritionHistoryByIdAndDate(ApplicationGlobals.loggedAppUserId, dates[i]);
            float caloriesInThisDate = 0;
            for (int j = 0; j < mealsInThisDay.size(); j++) {
                caloriesInThisDate = caloriesInThisDate + (mealsInThisDay.get(j).getNumberOfUnits() * mealsInThisDay.get(j).getCaloriesPerUnit());
            }
            mapOfCaloriesInMealByDate.put(mealsInThisDay.get(0).getDate(), caloriesInThisDate);

        }
        return mapOfCaloriesInMealByDate;
    }

    @NonNull
    private Set getSetOfDatesFromNutrition() {
        Set datesSetForNutrition = new TreeSet<Date>();
        for (NutritionHistory nutritionHistory : nutritionHistoryList
        ) {
            datesSetForNutrition.add(nutritionHistory.getDate());
        }
        return datesSetForNutrition;
    }

    private int getBaseCaloricExpenditure() {
        int formula;
        if (ApplicationGlobals.loggedAppUserWeight != null) {
            formula = Formulas.BMRbyHarrisa_BenedictaMethod(ApplicationGlobals.appUser, getContext());
        } else {
            formula = 0;
        }
        return formula;
    }

    private void setGraphViewAppearance(GraphView graph) {
        graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
        graph.getGridLabelRenderer().setNumHorizontalLabels(5);
        graph.getGridLabelRenderer().setHumanRounding(false, true);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScrollable(true);
        graph.getGridLabelRenderer().setHorizontalLabelsAngle(135);
        graph.getGridLabelRenderer().setPadding(32);
        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        graph.getLegendRenderer().setTextSize(12);
    }

    private void setDataForSeriesCaloriesADay(int formula) {
        Date xCalories;
        try {
            Date first = getFirstDate();
            Date last = getLastDate();

            for (Date date = first; date.before(new Date(last.getTime() + 86400000)); date = new Date(date.getTime() + 86400000)) {
                xCalories = date;
                seriesCaloriesADay.appendData(new DataPoint(xCalories, formula), true, 9999);

            }
        } catch (IndexOutOfBoundsException e) {
            Toast.makeText(getContext(), "There is no history in your database", Toast.LENGTH_LONG).show();
        }
    }

    private Date getLastDate() {
        Date last;
        if (nutritionHistoryList.get(nutritionHistoryList.size() - 1).getDate().after(activityHistoryList.get(activityHistoryList.size() - 1).getDate())) {
            last = nutritionHistoryList.get(nutritionHistoryList.size() - 1).getDate();
        } else {
            last = activityHistoryList.get(activityHistoryList.size() - 1).getDate();
        }
        return last;
    }

    private Date getFirstDate() {
        Date first;
        if (nutritionHistoryList.get(0).getDate().before(activityHistoryList.get(0).getDate())) {
            first = nutritionHistoryList.get(0).getDate();
        } else {
            first = activityHistoryList.get(0).getDate();
        }
        return first;
    }

    @NonNull
    private Map<Date, Float> getMapOfCaloriesInActivityByDate(Set datesSetForActivity) {
        Map<Date, Float> mapOfCaloriesInActivityByDate = new TreeMap<>();

        Date[] datesInActivity = new Date[datesSetForActivity.size()];

        datesInActivity = (Date[]) datesSetForActivity.toArray(datesInActivity);
        datesSetForActivity.toArray();
        for (int i = 0; i < datesInActivity.length; i++) {
            ArrayList<ActivityHistory> activitiesInThisDay = (ArrayList<ActivityHistory>) (List) activityHistoryRepository.getActivityHistoryByIdAndDate(ApplicationGlobals.loggedAppUserId, datesInActivity[i], getContext());//MainActivity.appUserDatabase.activityHistoryDAO().getActivityHistoryByIdAndDate(ApplicationGlobals.loggedAppUserId, datesInActivity[i]);
            float caloriesInThisDate = 0;
            for (int j = 0; j < activitiesInThisDay.size(); j++) {
                caloriesInThisDate = caloriesInThisDate + (activitiesInThisDay.get(j).getReps() * activitiesInThisDay.get(j).getCaloriesPerUnit());
            }
            mapOfCaloriesInActivityByDate.put(activitiesInThisDay.get(0).getDate(), caloriesInThisDate);

        }
        return mapOfCaloriesInActivityByDate;
    }

    @NonNull
    private Set getDatesFromActivityHistory() {
        Set datesSetForActivity = new TreeSet<Date>();
        for (ActivityHistory activityHistory : activityHistoryList
        ) {
            datesSetForActivity.add(activityHistory.getDate());
        }
        return datesSetForActivity;
    }

    private void setDataForSeriesCaloriesADayPlusActivity(int formula, Map<Date, Float> mapOfCaloriesInActivityByDate) {
        Date xActivity;
        float yActivity;
        Date xCaloriesPlusActivity;
        float yCaloriesPlusActivity;
        if (ApplicationGlobals.loggedAppUserWeight != null) {
            for (Map.Entry<Date, Float> set : mapOfCaloriesInActivityByDate.entrySet()) {
                xActivity = set.getKey();
                yActivity = set.getValue();
                xCaloriesPlusActivity = xActivity;
                yCaloriesPlusActivity = yActivity + formula;
                try {
                    mapOfCaloricExpenditure.put(xCaloriesPlusActivity, yCaloriesPlusActivity);
                    seriesCaloriesADayPlusActivity.appendData(new DataPoint(xCaloriesPlusActivity, yCaloriesPlusActivity), true, 9999);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setDataForSeriesActivity(Map<Date, Float> mapOfCaloriesInActivityByDate) {
        Date xActivity;
        float yActivity;
        if (ApplicationGlobals.loggedAppUserWeight != null) {
            for (Map.Entry<Date, Float> set : mapOfCaloriesInActivityByDate.entrySet()) {
                xActivity = set.getKey();
                yActivity = set.getValue();
                try {
                    seriesActivity.appendData(new DataPoint(xActivity, yActivity), true, 9999);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private PointsGraphSeries setAppearanceForGraph(PointsGraphSeries pointsGraphSeries, int color, String title, int size) {
        pointsGraphSeries.setColor(color);
        pointsGraphSeries.setTitle(title);
        pointsGraphSeries.setSize(size);
        return pointsGraphSeries;
    }


}

