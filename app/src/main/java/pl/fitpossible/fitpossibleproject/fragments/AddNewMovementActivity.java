package pl.fitpossible.fitpossibleproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import pl.fitpossible.fitpossibleproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNewMovementActivity extends Fragment {

    EditText EdTxtNameOfActivity;
    EditText EdTxtNumberOfCalories;
    EditText EdTxtUnitName;

    public AddNewMovementActivity() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_new_movement, container, false);
        EdTxtNameOfActivity = view.findViewById(R.id.edTxt_name_of_new_activity);
        String name = EdTxtNameOfActivity.getText().toString();

        EdTxtNumberOfCalories = view.findViewById(R.id.edTxt_calories_per_unit_of_new_activity);
        int calories = Integer.parseInt(EdTxtNumberOfCalories.getText().toString());

        EdTxtUnitName = view.findViewById(R.id.edTxt_unit_type_of_new_activity);
        String unit = EdTxtUnitName.getText().toString();
        // Inflate the layout for this fragment

        return view;
    }

}
