package pl.fitpossible.fitpossibleproject.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFoodProductFragment extends Fragment implements View.OnClickListener {
    Button BnHintFood;
    Button BnGoToDeleteFood;
    Button BnGoToUpdateFoodProduct;
    Button BnGoToAddNewFoodProduct;

    public MainFoodProductFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main_food_product_page, container, false);

        BnHintFood = view.findViewById(R.id.bn_hint_food);
        BnHintFood.setOnClickListener(this);

        BnGoToDeleteFood = view.findViewById(R.id.bn_go_to_delete_food_product);
        BnGoToDeleteFood.setOnClickListener(this);

        BnGoToUpdateFoodProduct = view.findViewById(R.id.bn_go_to_update_food_product);
        BnGoToUpdateFoodProduct.setOnClickListener(this);

        BnGoToAddNewFoodProduct = view.findViewById(R.id.bn_go_to_add_new_food_product);
        BnGoToAddNewFoodProduct.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.bn_hint_food:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new ReadFoodFragment())
                        .addToBackStack(null).commit();
                break;

            case R.id.bn_go_to_delete_food_product:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new DeleteFoodProductFragment())
                        .addToBackStack(null).commit();
                break;

            case R.id.bn_go_to_update_food_product:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new UpdateFoodProductFragment())
                        .addToBackStack(null).commit();
                break;

            case R.id.bn_go_to_add_new_food_product:
                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, new AddNewFoodProductFragment())
                        .addToBackStack(null).commit();
                break;
        }
    }
}
