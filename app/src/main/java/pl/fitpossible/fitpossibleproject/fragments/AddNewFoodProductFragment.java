package pl.fitpossible.fitpossibleproject.fragments;


import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lombok.Getter;
import lombok.Setter;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.ViewModel.AddNewFoodProductViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
@Getter
@Setter

public class AddNewFoodProductFragment extends Fragment implements View.OnClickListener {
    private final AddNewFoodProductHelper addNewFoodProductHelper = new AddNewFoodProductHelper(this);
    EditText TxtAddNewFoodProductName;
    EditText TxtAddNewFoodProductCaloriesPerUnit;
    AutoCompleteTextView AutoCompleteTxtAddNewFoodProductUnit;
    Button BnSubmitAddNewFoodProduct;
    AndroidViewModel addNewFoodProductViewModel;

    public AddNewFoodProductFragment() {
    }


//    public EditText getTxtAddNewFoodProductName() {
//        return TxtAddNewFoodProductName;
//    }
//
//    public void setTxtAddNewFoodProductName(EditText txtAddNewFoodProductName) {
//        TxtAddNewFoodProductName = txtAddNewFoodProductName;
//    }
//
//    public EditText getTxtAddNewFoodProductCaloriesPerUnit() {
//        return TxtAddNewFoodProductCaloriesPerUnit;
//    }
//
//    public void setTxtAddNewFoodProductCaloriesPerUnit(EditText txtAddNewFoodProductCaloriesPerUnit) {
//        TxtAddNewFoodProductCaloriesPerUnit = txtAddNewFoodProductCaloriesPerUnit;
//    }
//
//    public AutoCompleteTextView getAutoCompleteTxtAddNewFoodProductUnit() {
//        return AutoCompleteTxtAddNewFoodProductUnit;
//    }
//
//    public void setAutoCompleteTxtAddNewFoodProductUnit(AutoCompleteTextView autoCompleteTxtAddNewFoodProductUnit) {
//        AutoCompleteTxtAddNewFoodProductUnit = autoCompleteTxtAddNewFoodProductUnit;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_new_food_product, container, false);
        addNewFoodProductViewModel = ViewModelProviders.of(this).get(AddNewFoodProductViewModel.class);


        TxtAddNewFoodProductName = view.findViewById(R.id.edTxt_add_new_food_product_name);
        TxtAddNewFoodProductCaloriesPerUnit = view.findViewById(R.id.edTxt_add_new_food_product_calories_per_unit);
        AutoCompleteTxtAddNewFoodProductUnit = view.findViewById(R.id.edTxt_add_new_food_product_unit);

        BnSubmitAddNewFoodProduct = view.findViewById(R.id.bn_submit_add_new_food_product);
        BnSubmitAddNewFoodProduct.setOnClickListener(this);

        String[] unitArray = getResources().getStringArray(R.array.units);
        ArrayAdapter<String> arrayAdapterForUnit = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, unitArray);
        AutoCompleteTxtAddNewFoodProductUnit.setAdapter(arrayAdapterForUnit);
        AutoCompleteTxtAddNewFoodProductUnit.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String unit = AutoCompleteTxtAddNewFoodProductUnit.getText().toString();
            }
        });
        return view;
    }

    @Override
    public void onClick(View v) {
        Integer caloriesPerUnit = 0;
        String unit = null;
        String name = null;
        if (TxtAddNewFoodProductName.getText().toString() != "" && AutoCompleteTxtAddNewFoodProductUnit.getText().toString() != "") {
            {
                name = TxtAddNewFoodProductName.getText().toString();
                try {
                    caloriesPerUnit = Integer.parseInt(TxtAddNewFoodProductCaloriesPerUnit.getText().toString());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Calories per unit should be written in digits only", Toast.LENGTH_LONG).show();
                }
            }
            unit = AutoCompleteTxtAddNewFoodProductUnit.getText().toString();
        } else {
            Toast.makeText(getContext(), "Food name and units should not be empty", Toast.LENGTH_LONG).show();
        }
        addNewFoodProductHelper.addNewFoodProduct(name, caloriesPerUnit, unit);
//        TxtAddNewFoodProductName.setText("");
//        TxtAddNewFoodProductCaloriesPerUnit.setText("");
//        AutoCompleteTxtAddNewFoodProductUnit.setText("");
    }

    private void addNewFoodProduct(String name, Integer caloriesPerUnit, String unit) {
        addNewFoodProductHelper.addNewFoodProduct(name, caloriesPerUnit, unit);
    }
}
