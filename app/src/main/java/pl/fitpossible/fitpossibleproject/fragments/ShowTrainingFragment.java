package pl.fitpossible.fitpossibleproject.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.ApplicationGlobals;
import pl.fitpossible.fitpossibleproject.MainActivity;
import pl.fitpossible.fitpossibleproject.R;
import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;
import pl.fitpossible.fitpossibleproject.entity.NutritionHistory;
import pl.fitpossible.fitpossibleproject.repository.ActivityHistoryRepository;

import static java.util.Calendar.DAY_OF_MONTH;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShowTrainingFragment extends Fragment {
    RecyclerView recyclerView;
    ShowTrainingFragment.RecyclerViewAdapterForShowTraining recyclerViewAdapterForShowTraining;
    ActivityHistoryRepository activityHistoryRepository = new ActivityHistoryRepository();

    Bundle bundle;
    TextView TxtDateOfTraining;
    TextView TxtNameOfTraining;
    TextView TxtCaloriesOfTraining;
    TextView TxtEmptyShowTrainingDatabase;
    Long appUserId = ApplicationGlobals.loggedAppUserId;
    Long trainingDate;
    Date dateTruncateItd;
    List<ActivityHistory> training;

    public ShowTrainingFragment() {
        // Required empty public constructor
    }

    Boolean trainingDataBaseIsNotEmpty() {
        if ((activityHistoryRepository.getActivityHistoryByIdAndDate(appUserId, dateTruncateItd, getContext()).isEmpty()) == false) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_show_training, container, false);

        Context context = getActivity().getApplicationContext();


        // Inflate the layout for this fragment
        TxtDateOfTraining = view.findViewById(R.id.txt_date_of_training);
        TxtNameOfTraining = view.findViewById(R.id.txt_name_of_training);
        TxtCaloriesOfTraining = view.findViewById(R.id.txt_calories_of_training);
        recyclerView = view.findViewById(R.id.recyclerView_for_show_training);
        TxtEmptyShowTrainingDatabase = view.findViewById(R.id.txt_empty_show_training_database);
        if ((getArguments() != null) && (getArguments().containsKey("TrainingDate") == true)) {
            bundle = getArguments();
            Long dateLong = bundle.getLong("TrainingDate");
            Log.d("dateLongL", dateLong.toString());
            String dateString = DateFormat.format("dd/MM/yyyy", new Date(dateLong)).toString();
            Date dateFromCalendar = new Date(dateLong);
            dateTruncateItd = DateUtils.truncate(dateFromCalendar, DAY_OF_MONTH);


            TxtDateOfTraining.setText(dateString);
            trainingDate = dateLong;

        }


        if (trainingDataBaseIsNotEmpty()) {

            training = (List) activityHistoryRepository.getActivityHistoryByIdAndDate(appUserId, dateTruncateItd, getContext());//MainActivity.appUserDatabase.activityHistoryDAO().getActivityHistoryByIdAndDate(appUserId, dateTruncateItd);

        }


        if (trainingDataBaseIsNotEmpty()) {
            TxtEmptyShowTrainingDatabase.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            recyclerViewAdapterForShowTraining = new ShowTrainingFragment.RecyclerViewAdapterForShowTraining();
            recyclerViewAdapterForShowTraining.setMTrainingHistories((ArrayList) training);

            recyclerViewAdapterForShowTraining.setMContext(context);
            recyclerView.setAdapter(recyclerViewAdapterForShowTraining);

            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
        }
//        else {
//            TxtEmptyShowMealDatabase.setVisibility(View.VISIBLE);
//            recyclerView.setVisibility(View.GONE);
//        }

        return view;
    }

    @Data
    public class RecyclerViewAdapterForShowTraining extends RecyclerView.Adapter<pl.fitpossible.fitpossibleproject.fragments.ShowTrainingFragment.RecyclerViewAdapterForShowTraining.ViewHolder> {
        private ArrayList<ActivityHistory> mTrainingHistories = new ArrayList<>();
        private Context mContext;

        @NonNull
        @Override
        //Tu robisz view określając layouta którego bedziesz powielać i viewholdera na jego podstawie
        public pl.fitpossible.fitpossibleproject.fragments.ShowTrainingFragment.RecyclerViewAdapterForShowTraining.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_show_training_layout, parent, false);
            pl.fitpossible.fitpossibleproject.fragments.ShowTrainingFragment.RecyclerViewAdapterForShowTraining.ViewHolder holder = new pl.fitpossible.fitpossibleproject.fragments.ShowTrainingFragment.RecyclerViewAdapterForShowTraining.ViewHolder(view);
            return holder;
        }

        @Override
        //tu wpisujesz co ma sie znaleźć w każdym okienku z layouta i jak ma zadziałać
        public void onBindViewHolder(@NonNull pl.fitpossible.fitpossibleproject.fragments.ShowTrainingFragment.RecyclerViewAdapterForShowTraining.ViewHolder holder, int position) {
            if ((mTrainingHistories.get(position).getTime()) != null) {
                SimpleDateFormat time_format = new SimpleDateFormat("HH:mm");
                String hour = time_format.format(mTrainingHistories.get(position).getTime());
                holder.hour.setText(hour);
            } else {
                holder.hour.setText("no hour added");
            }
            holder.trainingName.setText(mTrainingHistories.get(position).getActivityType());
            String caloriesString = ((Integer) (mTrainingHistories.get(position).getReps() * mTrainingHistories.get(position).getCaloriesPerUnit())).toString();
            holder.calories.setText(caloriesString);
            Log.d("activity", mTrainingHistories.get(position).toString());

        }

        @Override
        public int getItemCount() {
            return mTrainingHistories.size();
        }

        //tu określasz jakie elementy layouta chcesz zawrzeć we Viewholderze
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView hour;
            TextView trainingName;
            TextView calories;
            Button deleteThisTraining;

            ConstraintLayout parentLayoutShowTraining;

            public ViewHolder(View itemView) {

                super(itemView);

                hour = itemView.findViewById(R.id.txt_hour_of_training);
                trainingName = itemView.findViewById(R.id.txt_name_of_training);
                calories = itemView.findViewById(R.id.txt_calories_of_training);
                parentLayoutShowTraining = itemView.findViewById(R.id.parent_layout_show_training);
                deleteThisTraining = itemView.findViewById(R.id.bn_delete_this_training);
                deleteThisTraining.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                final ActivityHistory training = mTrainingHistories.get(this.getAdapterPosition());
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("FitPossible");
                builder.setMessage("Do you really want to delete " + training.getActivityType());

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            ActivityHistory training = mTrainingHistories.get(getAdapterPosition());
                            MainActivity.appUserDatabase.activityHistoryDAO().deleteActivityHistory(training);
                            mTrainingHistories.remove(training);
                            notifyItemRemoved(getLayoutPosition());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Couldn't delete " + training.getActivityType() + " form Activity History", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        }
    }


}
