package pl.fitpossible.fitpossibleproject.Rest.Service;

import android.util.Log;

import java.util.List;

import pl.fitpossible.fitpossibleproject.Rest.Dto.AppUserDto;
import pl.fitpossible.fitpossibleproject.Rest.RetrofitApi.RetrofitAppUserApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitAppUserService {
    /////////////  GET  /////////////////////////
    public static void getAppUserDtoWithRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RetrofitAppUserApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitAppUserApi retrofitAppUserApi = retrofit.create(RetrofitAppUserApi.class);

        Call<List<AppUserDto>> call = retrofitAppUserApi.getListOfAppUsers();

        call.enqueue(new Callback<List<AppUserDto>>() {
            @Override
            public void onResponse(Call<List<AppUserDto>> call, Response<List<AppUserDto>> response) {
                List<AppUserDto> appUserDtoList = response.body();
                Log.d("DANE SCIAGNIETE!", "DANE SCIAGNIETE!");
//                //  na potrzeby wyświetlenia w logu
                for (AppUserDto appUserDto : appUserDtoList) {
                    Log.d("Id: ", appUserDto.getId().toString());
                    Log.d("Login: ", appUserDto.getLogin().toString());
                    Log.d("Lifestyle: ", String.valueOf(appUserDto.getLifestyle()));
                }

            }

            @Override
            public void onFailure(Call<List<AppUserDto>> call, Throwable t) {
                //Toast.makeText(MainActivity.getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("Text", "NO TO COS NIE POSZLO", t);
            }
        });
    }
//I niech sie zapisuje pojedynczy user w bazie danych
//        //////////////////
}
