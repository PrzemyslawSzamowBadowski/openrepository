package pl.fitpossible.fitpossibleproject.Rest.RetrofitApi;

import java.util.List;

import pl.fitpossible.fitpossibleproject.Rest.Dto.AppUserDto;
import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitAppUserApi {
    String BASE_URL = "http://10.20.20.173:8080";

     @GET("/users/all")
    Call<List<AppUserDto>> getListOfAppUsers();

//    @POST("user")
//    Call<List<User>> createAccount(@Body User user);

//    ////////wysyłanie większych plików częściami
//    @Multipart
//    @POST
//    Call<ResponseBody> uploadPhoto(
//            @Part("description") RequestBody description,
//            @Part MultipartBody.Part photo
//    )

}
