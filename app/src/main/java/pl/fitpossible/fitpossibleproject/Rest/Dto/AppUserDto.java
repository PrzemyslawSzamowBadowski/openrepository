package pl.fitpossible.fitpossibleproject.Rest.Dto;

import android.arch.persistence.room.TypeConverters;

import java.util.Date;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.converter.AppUserConverters;
import pl.fitpossible.fitpossibleproject.entity.Gender;
import pl.fitpossible.fitpossibleproject.entity.LifestyleType;

@Data
@TypeConverters(AppUserConverters.class)
public class AppUserDto {


    private Long id;
    // @NotBlank

    private String login;
    //@NotBlank
    private String password;
    // @Email
    private String email;

    private Gender gender;

    private Integer height;

    private Date dateOfBirth;
@TypeConverters(AppUserConverters.class)
    private LifestyleType lifestyle;
}
