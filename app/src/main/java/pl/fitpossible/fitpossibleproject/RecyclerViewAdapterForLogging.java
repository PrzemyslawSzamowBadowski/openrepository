package pl.fitpossible.fitpossibleproject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import lombok.Data;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.entity.AvatarImage;
import pl.fitpossible.fitpossibleproject.fragments.LoggingFragment;
import pl.fitpossible.fitpossibleproject.repository.AvatarImageRepository;

@Data
public class RecyclerViewAdapterForLogging extends RecyclerView.Adapter<RecyclerViewAdapterForLogging.ViewHolder> {
    private final String TAG = "RVAdapterForLogging";

    //private ArrayList<String> mAvatars = new ArrayList<>();
    private ArrayList<AppUser> mAppUser = new ArrayList<>();
    private Context mContext;
    private AvatarImageRepository avatarImageRepository = new AvatarImageRepository();


    @Override
    //Tu robisz view określając layouta którego bedziesz powielać i viewholdera na jego podstawie
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_app_user_layout, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    //tu wpisujesz co ma sie znaleźć w każdym okienku z layouta i jak ma zadziałać
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "OnBindViewHolder called");
        final String[] decodedName = {mAppUser.get(position).getLogin()};
//        Glide.with(mContext)
//                .asBitmap()
//                .load(mAvatars.get(position))
//                .into(holder.avatar);


        try {
            if (mAppUser.get(position).getLogin().matches("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$")) {
                decodedName[0] = new String(Base64.decode(mAppUser.get(position).getLogin(), Base64.NO_WRAP), "UTF-8");
                holder.appUserName.setText(decodedName[0]);
            } else {
                throw new IllegalArgumentException();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            decodedName[0] = mAppUser.get(position).getLogin();
            holder.appUserName.setText(decodedName[0]);
        }
        try {
            AvatarImage avatar = avatarImageRepository.getOneAvatarImageById(mAppUser.get(position).getId(), getMContext());//MainActivity.appUserDatabase.avatarImageDAO().getOneAvatarImageById(mAppUser.get(position).getId());
            if (avatar != null) {
                String encodedImageOfAvatar = avatar.getStringWithImage();
                //byte[] encodedImageOfAvatarByteArray = encodedImageOfAvatar.getBytes();
                byte data[] = Base64.decode(encodedImageOfAvatar, Base64.DEFAULT);

                Bitmap avatarBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                // ApplicationGlobals.loggedAppUserAvatar = avatarBitmap;
                holder.avatar.setImageBitmap(avatarBitmap);


            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        holder.parentLayoutShowAppusers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (mAppUser.get(position).getLogin().matches("^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$")) {
                        decodedName[0] = new String(Base64.decode(mAppUser.get(position).getLogin(), Base64.NO_WRAP), "UTF-8");
                    } else {
                        throw new IllegalArgumentException();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    decodedName[0] = mAppUser.get(position).getLogin();
                }

                Log.d(TAG, "onClick: clicked on: " + mAppUser.get(position).getLogin());

                Toast.makeText(mContext, decodedName[0], Toast.LENGTH_SHORT).show();

                Bundle bundle = new Bundle();
                bundle.putString("CurrentAppUserName", decodedName[0]);

                if (bundle.containsKey("CurrentAppUserName")) {
                    LoggingFragment loggingFragment = new LoggingFragment();
                    loggingFragment.setArguments(bundle);

                    //  loggingFragment.updateTextView("zmiana z recyclerview");
                    LoggingActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container, loggingFragment).
                            addToBackStack(null).commit();
                } else {
                    Log.d("RVAdapterForLogging:", "CurrentAppUserName in RecyclerViewAdapterForLogging IS NOT added");
                }

//                MainActivity.fragmentManager.beginTransaction().replace(R.id.fragment_container,new LoggingFragment()).
//                        addToBackStack(null).commit();
            }
        });
    }

    @Override
    //tu określasz ilość okienek które będą sie powtarzać
    public int getItemCount() {

        return mAppUser.size();
    }

    //tu określasz jakie elementy layouta chcesz zawrzeć we Viewholderze
    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView avatar;
        TextView appUserName;
        ConstraintLayout parentLayoutShowAppusers;

        public ViewHolder(View itemView) {

            super(itemView);

            avatar = itemView.findViewById(R.id.img_circle_appUser_image);
            appUserName = itemView.findViewById(R.id.txt_appuser_profile);
            parentLayoutShowAppusers = itemView.findViewById(R.id.parent_layout_show_appusers);
        }

    }
}
