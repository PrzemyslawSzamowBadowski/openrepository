package pl.fitpossible.fitpossibleproject.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import java.util.List;

import pl.fitpossible.fitpossibleproject.converter.AppUserConverters;
import pl.fitpossible.fitpossibleproject.converter.DateConverter;
import pl.fitpossible.fitpossibleproject.converter.GenderConverter;
import pl.fitpossible.fitpossibleproject.entity.AppUser;

@Dao
@TypeConverters(AppUserConverters.class)
public interface AppUserDAO {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    public void addAppUser(AppUser appUuser);

    @Query("select * from AppUser")
    public List<AppUser> getAppUsers();

    @Query("select login from AppUser")
    public List<String> getAppUsersLogin();

    @Query("select * from AppUser where Appuser.id = :id")
    public AppUser getAppUserById(Long id);

    @Query("select * from AppUser where Appuser.login = :login")
    public AppUser getAppUserByLogin(String login);

    @Query("select * from AppUser where Appuser.password = :password")
    public AppUser getAppUserByPassword(String password);

    @Delete
    public void deleteAppUser(AppUser appUser);

    @Update
    public void updateAppUser(AppUser appUser);
}
