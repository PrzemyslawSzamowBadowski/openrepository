package pl.fitpossible.fitpossibleproject.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import java.util.Date;
import java.util.List;

import pl.fitpossible.fitpossibleproject.converter.DateConverter;
import pl.fitpossible.fitpossibleproject.entity.MovementActivity;
import pl.fitpossible.fitpossibleproject.entity.NutritionHistory;

@Dao
@TypeConverters(DateConverter.class)
public interface NutritionHistoryDAO {
    @Insert
    public void addNutritionHistory(NutritionHistory nutritionHistory);

    @Query("select * from NutritionHistory")
    public List<NutritionHistory> getNutritionHistory();

    @Query("select * from NutritionHistory where appUserId = :appUserId and date = :date")
    public List<NutritionHistory> getNutritionHistoryByIdAndDate(Long appUserId, Date date);

    @Query("select * from NutritionHistory where appUserId = :appUserId order by date")
    public List<NutritionHistory> getNutritionHistoryById(Long appUserId);

    @Query("select * from NutritionHistory where date = :date")
    public List<NutritionHistory> getNutritionHistoryByDate(Date date);

    @Delete
    public void deleteNutritionHistory(NutritionHistory nutritionHistory);

    @Update
    public void updateNutritionHistory(NutritionHistory nutritionHistory);
}
