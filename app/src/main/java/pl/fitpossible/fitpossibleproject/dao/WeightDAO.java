package pl.fitpossible.fitpossibleproject.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pl.fitpossible.fitpossibleproject.entity.Weight;

@Dao
public interface WeightDAO {

    @Insert
    public void addWeight(Weight weight);

    @Query("select * from Weight")
    public List<Weight> getWeight();

    @Query("select * from Weight where appUserId = :appUserId")
    public List<Weight> getWeightByAppUserId(Long appUserId);

    @Query("Select * from Weight where appUserId = :appUserId order by id DESC LIMIT 1 ")
    public Weight getLastWeightByAppUserId(Long appUserId);

    @Delete
    public void deleteWeight(Weight weight);

    @Update
    public void updateWeight(Weight weight);

}
