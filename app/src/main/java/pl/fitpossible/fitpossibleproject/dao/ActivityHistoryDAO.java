package pl.fitpossible.fitpossibleproject.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.Date;
import java.util.List;

import pl.fitpossible.fitpossibleproject.entity.ActivityHistory;

@Dao
public interface ActivityHistoryDAO {

    @Insert
    public void addActivityHistory(ActivityHistory activityHistory);

    @Query("select * from ActivityHistory")
    public List<ActivityHistory> getActivityHistories();

    @Query("select * from ActivityHistory where appUserId = :appUserId order by date")
    public List<ActivityHistory> getActivityHistoryById(Long appUserId);

    @Query("select * from ActivityHistory where appUserId = :appUserId and date = :date")
    public List<ActivityHistory> getActivityHistoryByIdAndDate(Long appUserId, Date date);

    @Delete
    public void deleteActivityHistory(ActivityHistory activityHistory);

    @Update
    public void updateActivityHistory(ActivityHistory activityHistory);
}