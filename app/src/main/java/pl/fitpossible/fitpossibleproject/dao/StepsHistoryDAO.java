package pl.fitpossible.fitpossibleproject.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import java.util.Date;
import java.util.List;

import pl.fitpossible.fitpossibleproject.converter.StepsHistoryConverter;
import pl.fitpossible.fitpossibleproject.entity.StepsHistory;
import pl.fitpossible.fitpossibleproject.entity.Weight;

@Dao
@TypeConverters(StepsHistoryConverter.class)
public interface StepsHistoryDAO {

    @Insert
    public void addStepHistory(StepsHistory stepsHistory);

    @Query("select * from StepsHistory")
    public List<StepsHistory> getstepsHistory();

    @Query("select * from StepsHistory where id = (SELECT max(id) FROM StepsHistory); ")
    public StepsHistory getOneStepHistorybyId();

   @Query("select * from StepsHistory where dateOfSteps = :date")
   public StepsHistory getStepsHistoryByDate(Date date);

    @Delete
    public void deleteStepsHistory(StepsHistory stepsHistory);

    @Update
    public void updateStepsHistory(StepsHistory stepsHistory);
}
