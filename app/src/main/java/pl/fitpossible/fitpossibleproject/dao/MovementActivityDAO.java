package pl.fitpossible.fitpossibleproject.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pl.fitpossible.fitpossibleproject.entity.MovementActivity;


@Dao
public interface MovementActivityDAO {

    @Insert
    public void addActivity(MovementActivity movementActivity);

    @Query("select * from MovementActivity")
    public List<MovementActivity> getMovementActivity();

    @Query("select * from MovementActivity where activityName = :activityName")
    public MovementActivity getMovementActivity(String activityName);

    @Delete
    public void deleteActivity(MovementActivity movementActivity);

    @Update
    public void updateActivity(MovementActivity movementActivity);
}
