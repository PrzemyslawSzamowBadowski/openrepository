package pl.fitpossible.fitpossibleproject.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pl.fitpossible.fitpossibleproject.entity.Food;

@Dao
public interface FoodDAO {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    public void addFood(Food food);

    @Query("select * from Food ")
    public List<Food> getFood();

    @Query("select * from Food where name = :foodName LIMIT 1")
    public Food getOneFood(String foodName);

    @Delete
    public void deleteFood(Food food);

    @Update
    public void updateFood(Food food);
}
