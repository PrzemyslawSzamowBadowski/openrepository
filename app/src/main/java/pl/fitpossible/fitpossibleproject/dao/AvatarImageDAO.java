package pl.fitpossible.fitpossibleproject.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pl.fitpossible.fitpossibleproject.entity.AvatarImage;

@Dao
public interface AvatarImageDAO {

    @Insert
    public void addAvatarImage(AvatarImage avatarImage);

    @Query("Select * from AvatarImage")
    public List<AvatarImage> getAllAvatarImages();

//    @Query("Select * from AvatarImage where appUserId = :appUserId")
//    public List<AvatarImage> getAllAvatarImagesById(Long appUserId);

    @Query("Select * from AvatarImage where id =:id")
    public AvatarImage getOneAvatarImageById(Long id);

    @Query("Select StringWithImage from AvatarImage where appUserId = :appUserId Limit 1")
    public String getOneStringAvatarImageByAppUserId(Long appUserId);

    @Delete
    public void deleteAvatarImage(AvatarImage avatarImage);

    @Update
    public void updateAvatarImage(AvatarImage avatarImage);


}
