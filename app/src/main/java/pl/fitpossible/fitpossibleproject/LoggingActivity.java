package pl.fitpossible.fitpossibleproject;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.fitpossible.fitpossibleproject.fragments.LoggingFragment;

public class LoggingActivity extends AppCompatActivity {
    public static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logging);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, new LoggingFragment()).commit();

    }

    @Override
    public void onBackPressed(){

    }
}
