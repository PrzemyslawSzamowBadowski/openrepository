package pl.fitpossible.fitpossibleproject.dto;

import lombok.Data;

@Data
public class AuthorizationUserData {

    private String login;
    private String password;
}
