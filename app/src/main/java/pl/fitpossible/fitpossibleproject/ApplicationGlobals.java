package pl.fitpossible.fitpossibleproject;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.widget.ImageView;

import org.threeten.bp.Duration;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.temporal.Temporal;
import org.threeten.bp.temporal.TemporalAmount;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.databases.AppUserDatabase;
import pl.fitpossible.fitpossibleproject.entity.AppUser;
import pl.fitpossible.fitpossibleproject.entity.Weight;

@Data
public class ApplicationGlobals {

    public static String loggedAppUserName;
    public static Long loggedAppUserId;
    public static AppUser appUser;
    public static Bitmap loggedAppUserAvatar;
    public static Double loggedAppUserWeight;

    public AppUser appUser(Context context, Long id) {

        return AppUserDatabase.getInstance(context).appUserDAO().getAppUserById(loggedAppUserId);
    }
    // public static double appUserWeight = (MainActivity.appUserDatabase.weightDAO().getLastWeightByAppUserId(loggedAppUserId)).getWeight();
//    private Duration appUserAge = new Duration(Calendar.getInstance().getTime().getTime() - appUser.getDateOfBirth().getTime());
}
