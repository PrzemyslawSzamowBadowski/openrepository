package pl.fitpossible.fitpossibleproject.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import lombok.Data;
import lombok.NonNull;

@Data
@Entity(indices = {@Index(value = "name", unique = true)})
public class Food {
    @PrimaryKey(autoGenerate = true)
    private Long id;

    @NonNull 
    private String name;

    @NonNull
    private Integer caloriesPerUnit;

    @NonNull
    private String unit;

    public Food() {
    }

//    public Food(@NonNull String name, @NonNull Integer caloriesPerUnit, @NonNull String unit) {
//        this.name = name;
//        this.caloriesPerUnit = caloriesPerUnit;
//        this.unit = unit;
//    }

}
