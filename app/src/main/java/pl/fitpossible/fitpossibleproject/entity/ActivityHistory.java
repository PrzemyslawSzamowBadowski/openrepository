package pl.fitpossible.fitpossibleproject.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = AppUser.class,
        parentColumns = "id",
        childColumns = "appUserId",
        onUpdate = CASCADE,
        onDelete = CASCADE),
        indices = {@Index("appUserId")})
@Data
public class ActivityHistory implements Comparable<ActivityHistory> {

    @PrimaryKey(autoGenerate = true)
    private Long id;
    private Long appUserId;
    private String activityType;
    private int caloriesPerUnit;
    private String unitType;
    private int reps;
    private Date date;
    private Date time;

    @Override
    public int compareTo(ActivityHistory activityHistory) {
        return (int)(this.date.compareTo(activityHistory.date));
    }


    }
