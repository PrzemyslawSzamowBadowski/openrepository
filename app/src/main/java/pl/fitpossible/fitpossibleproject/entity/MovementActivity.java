package pl.fitpossible.fitpossibleproject.entity;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import lombok.Data;

@Data
@Entity(indices ={@Index(value = "activityName",unique = true)})

public class MovementActivity {
    @PrimaryKey(autoGenerate = true)

    private Long id;
    private String activityName;
    private int caloriesPerUnit;
    private String unitType;


}


