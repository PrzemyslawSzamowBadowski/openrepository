package pl.fitpossible.fitpossibleproject.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.sql.Time;
import java.util.Date;

import lombok.Data;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Data
@Entity(foreignKeys = @ForeignKey(entity = AppUser.class,
        parentColumns = "id",
        childColumns = "appUserId",
        onUpdate = CASCADE,
        onDelete = CASCADE),
        indices = {@Index("appUserId")})
public class NutritionHistory implements Comparable<NutritionHistory> {
    @PrimaryKey(autoGenerate = true)
    private Long id;
    private Long appUserId;
    private String name;
    private Integer caloriesPerUnit;
    private Float numberOfUnits;
    private String unit;
    private Date date;
    private Time time;

    @Override
    public int compareTo(NutritionHistory nutritionHistory) {
        return (int)(this.date.compareTo(nutritionHistory.date));
    }
}
