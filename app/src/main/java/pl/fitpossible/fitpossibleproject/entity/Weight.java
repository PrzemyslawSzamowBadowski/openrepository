package pl.fitpossible.fitpossibleproject.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.converter.WeightConverter;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = AppUser.class,
        parentColumns = "id",
        childColumns = "appUserId",
        onUpdate = CASCADE,
        onDelete = CASCADE),
        indices = {@Index("appUserId")}
)

@Data
@TypeConverters(WeightConverter.class)
public class Weight {
    @PrimaryKey(autoGenerate = true)
    private Long id;

    private Date date;
    private Double weight;
    private Long appUserId;

    // @ManyToOne
    // private AppUser user;

}
