package pl.fitpossible.fitpossibleproject.entity;

public enum Gender {
    FEMALE,
    MALE;

    public static Gender toGender(Integer numeral) {
        if (numeral != null) {
            for (Gender s : Gender.values()) {
                if (s.ordinal() == numeral) {
                    return s;
                }
            }
        }
        return null;
    }

    public static Integer toInteger(Gender gender) {
        if (gender != null) {
            return gender.ordinal();
        } else {
            return null;
        }
    }

    public static Gender StringToGender(String gender) {
        if (gender != null) {
            return Gender.valueOf(gender.toUpperCase());
        } else {
            return null;
        }
    }
}