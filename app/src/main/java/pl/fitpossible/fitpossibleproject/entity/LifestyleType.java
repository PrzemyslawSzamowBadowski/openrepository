package pl.fitpossible.fitpossibleproject.entity;

public enum LifestyleType {
    INACTIVE,
    LOW_ACTIVE,
    MODERATELY_ACTIVE,
    ACTIVE,
    VERY_ACTIVE;

    public static LifestyleType stringToLifestyleType(String stringLifestyleType) {
        if (stringLifestyleType != null) {
            return LifestyleType.valueOf(stringLifestyleType.toUpperCase());
        } else {
            return null;
        }
    }
}
