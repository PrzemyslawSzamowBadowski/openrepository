package pl.fitpossible.fitpossibleproject.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;

import pl.fitpossible.fitpossibleproject.converter.AppUserConverters;
import pl.fitpossible.fitpossibleproject.converter.DateConverter;
import pl.fitpossible.fitpossibleproject.converter.GenderConverter;

import lombok.Data;

@Entity(tableName = "AppUser", indices = {@Index(value = "login", unique = true)})
@Data

public class AppUser {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo//(name = "user_name")

    private String login;

    private String password;

    @ColumnInfo//(name = "user_email")
    private String email;

    private Integer height;

    @TypeConverters(AppUserConverters.class)
    private Gender gender;

    @TypeConverters(AppUserConverters.class)
    private Date dateOfBirth;

    @TypeConverters(AppUserConverters.class)
    private LifestyleType lifestyle;


}
