package pl.fitpossible.fitpossibleproject.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import lombok.Data;
import pl.fitpossible.fitpossibleproject.converter.StepsHistoryConverter;
import pl.fitpossible.fitpossibleproject.fragments.StepsFragment;

@Entity
@Data
@TypeConverters(StepsHistoryConverter.class)
public class StepsHistory {
    @PrimaryKey(autoGenerate = true)
    private Long id;

    private Float steps;

    private Date dateOfSteps;


}
