package pl.fitpossible.fitpossibleproject.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import lombok.Data;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(foreignKeys = @ForeignKey(entity = AppUser.class,
        parentColumns = "id",
        childColumns = "appUserId",
        onUpdate = CASCADE,
        onDelete = CASCADE),
        indices = {@Index("appUserId")})
@Data
public class AvatarImage {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    private Long appUserId;

    private String avatarName;

    private String StringWithImage;

}
