package pl.fitpossible.fitpossibleproject.entity;

public enum ActivityType {
    RUNNING,
    WALKING,
    PUSHUPS,
    SQUATS
}
